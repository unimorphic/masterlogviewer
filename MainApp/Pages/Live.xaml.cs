﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Common.UserInterface;
using MasterLogViewer.Properties;
using Plugins.Interface;
using MasterLogViewer.ViewModels;

namespace MasterLogViewer.Pages
{
    public partial class Live : UserControl, ITabAdvContent
    {
        private readonly LiveViewModel viewModel;


        public event Action<string, string, FrameworkElement> AddNewTab;


        public Live( ILogViewer plugin, string location )
        {
            InitializeComponent();

            filter.Setup( plugin );
            locationSelector.Setup( plugin, location );
            fileViewer.Plugin = plugin;

            DataContext = viewModel = new LiveViewModel( Dispatcher, plugin );
            viewModel.Items.CollectionChanged += ItemsOnCollectionChanged;
            viewModel.PropertyChanged += ViewModelOnPropertyChanged;
            viewModel.Start();

            UserSettings.Default.PropertyChanged += UserSettingsOnPropertyChanged;
        }


        private void UserSettingsOnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            locationSelector.UserSettingsOnPropertyChanged( sender, e );
            filter.UserSettingsOnPropertyChanged( sender, e );
            fileViewer.UserSettingsOnPropertyChanged( sender, e );
        }

        private void ViewModelOnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if( e.PropertyName == "IsAutoScrolling" && viewModel.IsAutoScrolling )
                fileViewer.ScrollGridToEnd();
        }

        private void ItemsOnCollectionChanged( object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs )
        {
			if( viewModel.IsAutoScrolling )
				fileViewer.ScrollGridToEnd();
        }

        private void FileViewer_SelectedCellsChanged()
		{
			viewModel.IsAutoScrolling = false;
		}

        private void FileViewer_VerticalScrollChanged()
        {
            viewModel.IsAutoScrolling = false;
        }


        public void OnSwitchToTab() { }

        public void OnSwitchAwayFromTab( SwitchAwayFromTabEventArgs e ) { }

        public void OnClosingTab( SwitchAwayFromTabEventArgs e )
        {
            UserSettings.Default.PropertyChanged -= UserSettingsOnPropertyChanged;
            viewModel.Stop();
        }

        public void OnTabClosed() { }
    }
}
