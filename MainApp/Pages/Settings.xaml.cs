﻿using System;
using System.Windows.Controls;
using Common.Plugins;
using FirstFloor.ModernUI.Presentation;
using Plugins.Interface;

namespace MasterLogViewer.Pages
{
    public partial class Settings : UserControl
    {
        public Settings()
        {
            InitializeComponent();

            foreach( var plugin in PluginUtility.FilterPlugins<ILogViewer>() )
            {
                tabs.Links.Add( new Link { DisplayName = plugin.DisplayName, IsHeading = true, Source = new Uri( "/Controls/Settings/PluginSettings.xaml?plugin=" + plugin.FullTypeName, UriKind.Relative ) } );
                tabs.Links.Add( new Link { DisplayName = "log locations", Source = new Uri( "/Controls/Settings/FileLocationsSettings.xaml?plugin=" + plugin.FullTypeName, UriKind.Relative ) } );
                tabs.Links.Add( new Link { DisplayName = "filters", Source = new Uri( "/Controls/Settings/FilterSettings.xaml?plugin=" + plugin.FullTypeName, UriKind.Relative ) } );
                tabs.Links.Add( new Link { DisplayName = "grid color rules", Source = new Uri( "/Controls/Settings/GridColorsSettings.xaml?plugin=" + plugin.FullTypeName, UriKind.Relative ) } );
                tabs.Links.Add( new Link { DisplayName = "grid columns", Source = new Uri( "/Controls/Settings/GridColumnsSettings.xaml?plugin=" + plugin.FullTypeName, UriKind.Relative ) } );
            }
        }
    }
}
