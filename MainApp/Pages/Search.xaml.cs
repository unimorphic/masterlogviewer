﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Common.UserInterface;
using MasterLogViewer.Properties;
using Plugins.Interface;
using MasterLogViewer.ViewModels;

namespace MasterLogViewer.Pages
{
    public partial class Search : UserControl, ITabAdvContent
    {
        private readonly SearchViewModel viewModel;


        public event Action<string, string, FrameworkElement> AddNewTab;


        public Search( ILogViewer plugin )
        {
            InitializeComponent();

            filter.Setup( plugin );
            locationSelector.Setup( plugin, string.Empty );
            fileViewer.Plugin = plugin;
            DataContext = viewModel = new SearchViewModel( Dispatcher, plugin );
            viewModel.SearchCompleted += ViewModelOnSearchCompleted;
            UserSettings.Default.PropertyChanged += UserSettingsOnPropertyChanged;
        }

        private void OnLoaded( object sender, RoutedEventArgs e )
        {
            var copyText = Clipboard.GetText( TextDataFormat.Text );
            if( !string.IsNullOrEmpty( copyText ) && !viewModel.IsSearching )
            {
                if( fileViewer.Plugin.IsTextAutoSearchable( copyText ) )
                    viewModel.ClipboardSearch( true );
            }
        }

        private void OnKeyUp( object sender, KeyEventArgs keyEventArgs )
        {
            if( keyEventArgs.Key == Key.F5 && viewModel.RefreshCmd.CanExecute( null ) )
                viewModel.RefreshCmd.Execute( null );
        }


        private void ViewModelOnSearchCompleted()
        {
            Dispatcher.BeginInvoke( DispatcherPriority.Background,
                new Action( () => filter.MoveFocus( new TraversalRequest( FocusNavigationDirection.First ) ) ) );
        }

        private void UserSettingsOnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            locationSelector.UserSettingsOnPropertyChanged( sender, e );
            filter.UserSettingsOnPropertyChanged( sender, e );
            fileViewer.UserSettingsOnPropertyChanged( sender, e );
        }

        private void Search_KeyUp( object sender, KeyEventArgs e )
        {
            if( e.Key == Key.Enter && viewModel.RefreshCmd.CanExecute( null ) )
                viewModel.RefreshCmd.Execute( null );
        }


        public void OnSwitchToTab() { }

        public void OnSwitchAwayFromTab( SwitchAwayFromTabEventArgs e ) { }

        public void OnClosingTab( SwitchAwayFromTabEventArgs e )
        {
            UserSettings.Default.PropertyChanged -= UserSettingsOnPropertyChanged;
        }

        public void OnTabClosed() { }
    }
}
