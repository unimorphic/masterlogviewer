﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Common.UserInterface;
using MasterLogViewer.Properties;
using MasterLogViewer.ViewModels;
using Plugins.Interface;

namespace MasterLogViewer.Pages
{
    public partial class BrowseFile : UserControl, ITabAdvContent
    {
        private readonly BrowseFileViewModel viewModel;


        public event Action<string, string, FrameworkElement> AddNewTab;


        public BrowseFile( ILogViewer plugin, string file )
        {
            InitializeComponent();
            
            filter.Setup( plugin );
            fileViewer.Plugin = plugin;

            DataContext = viewModel = new BrowseFileViewModel( plugin );

            if( !string.IsNullOrEmpty( file ) )
                viewModel.LoadLogFileAsync( file );

            UserSettings.Default.PropertyChanged += UserSettingsOnPropertyChanged;
        }


        private void UserSettingsOnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            filter.UserSettingsOnPropertyChanged( sender, e );
            fileViewer.UserSettingsOnPropertyChanged( sender, e );
        }

        private void OnKeyUp( object sender, KeyEventArgs keyEventArgs )
        {
            if( keyEventArgs.Key == Key.F5 && viewModel.RefreshCmd.CanExecute( null ) )
                viewModel.RefreshCmd.Execute( null );
        }


        public void OnSwitchToTab() { }

        public void OnSwitchAwayFromTab( SwitchAwayFromTabEventArgs e ) { }

        public void OnClosingTab( SwitchAwayFromTabEventArgs e )
        {
            UserSettings.Default.PropertyChanged -= UserSettingsOnPropertyChanged;
        }

        public void OnTabClosed() { }
    }
}
