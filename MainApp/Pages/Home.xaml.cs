﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Common.Plugins;
using Plugins.Interface;
using MasterLogViewer.Controls;
using Common.UserInterface;

namespace MasterLogViewer.Pages
{
    public partial class Home : UserControl, ITabAdvContent
    {
        private readonly List<ButtonLinkGroup> linkGroups = new List<ButtonLinkGroup>();


        public event Action<string, string, FrameworkElement> AddNewTab;


        public Home()
        {
            InitializeComponent();

            foreach( var plugin in PluginUtility.FilterPlugins<ILogViewer>() )
            {
                var linkGroup = new ButtonLinkGroup( plugin.DisplayName.ToUpper(), plugin.IconData );
                linkGroup.Links.Add( new ButtonLink( "Browse", plugin, "M0,21.940001L64,21.940001 58.81641,53.042002 5.4832716,53.042002z M5.4829998,0L26.983213,0 26.983213,6.5154943 58.805,6.5154943 58.805,14.362 5.5063095,14.362 5.5063095,7.1121521 5.4829998,7.1121521z" ) );
                linkGroup.Links.Add( new ButtonLink( "Search", plugin, "F1M-185.925,-2026.96L-203.062,-2048.74C-197.485,-2056.51 -197.433,-2067.31 -203.64,-2075.2 -211.167,-2084.76 -225.019,-2086.42 -234.588,-2078.89 -244.154,-2071.36 -245.808,-2057.51 -238.282,-2047.94 -231.986,-2039.95 -221.274,-2037.5 -212.337,-2041.31L-195.262,-2019.61 -185.925,-2026.96z M-231.201,-2053.51C-235.653,-2059.17 -234.674,-2067.36 -229.02,-2071.81 -223.36,-2076.26 -215.169,-2075.29 -210.721,-2069.63 -206.269,-2063.97 -207.245,-2055.78 -212.902,-2051.33 -218.559,-2046.88 -226.752,-2047.86 -231.201,-2053.51z" ) );
                linkGroup.Links.Add( new ButtonLink( "Live", plugin, "M28.142726,22.328131C28.647455,22.311525,29.188263,22.477322,29.710634,22.845585L41.771735,29.948748C43.257523,30.995686,43.278223,32.740551,41.819834,33.82909L29.951632,41.197762C29.130039,41.808384 28.213346,41.790985 27.474952,41.344269 26.230263,40.595642 26.191262,38.913979 26.179563,37.460724L26.088264,26.318514C26.077864,24.899061 26.024564,23.203699 27.284954,22.550674 27.550039,22.413719 27.839889,22.338093 28.142726,22.328131z M36.769003,0C36.843402,0.0020751697 36.915005,-0.0031738567 36.991705,0.0084228518 52.29283,2.4069891 63.997303,15.636045 64.000004,31.613964 63.997303,47.588585 52.29283,60.821098 36.991705,63.219402 36.915005,63.231201 36.843402,63.226002 36.769003,63.228001L36.769003,52.909592C36.885105,52.86729 37.000906,52.825493 37.122005,52.796387 46.693748,50.483189 53.799353,41.894577 53.820055,31.613964 53.799353,21.333351 46.693748,12.744842 37.122005,10.432339 37.000906,10.403739 36.885105,10.362039 36.769003,10.319139z M27.228002,0L27.228002,10.321038C27.114601,10.362739 26.9988,10.403739 26.8804,10.432339 17.308565,12.744842 10.200539,21.333351 10.180939,31.613964 10.200539,41.896675 17.308565,50.483185 26.8804,52.796391 26.9988,52.825489 27.114601,52.86599 27.228002,52.908188L27.228002,63.228001C27.155002,63.226002 27.083401,63.231201 27.008,63.219402 11.704445,60.821098 0.00390625,47.588585 0,31.613964 0.00390625,15.636044 11.704445,2.4069891 27.008,0.0084228518 27.083401,-0.0031738567 27.155002,0.0020751695 27.228002,0z" ) );
                                
                linkGroups.Add( linkGroup );
            }
            linkGroupsList.ItemsSource = linkGroups;
        }


        private void LinkButton_OnClick( object sender, RoutedEventArgs e )
        {
            var link = (ButtonLink)( (FrameworkElement)sender ).DataContext;

            switch( link.Title )
            {
                case "Browse":
                    AddNewTab( "Browse", link.Plugin.IconData, new Browse( link.Plugin ) );
                    break;
                case "Search":
                    AddNewTab( "Search", link.Plugin.IconData, new Search( link.Plugin ) );
                    break;
                case "Live":
                    AddNewTab( "Live", link.Plugin.IconData, new Live( link.Plugin, string.Empty ) );
                    break;
            }
        }

        private void LinkMiniButton_OnClick( object sender, RoutedEventArgs e )
        {
            var link = (ButtonLink)( (FrameworkElement)sender ).DataContext;

            switch( link.Title )
            {
                case "Live":
                    var locations = LocationSelector.GetSelectedLocations( link.Plugin );
                    foreach( var location in locations )
                        AddNewTab( "Live", link.Plugin.IconData, new Live( link.Plugin, location ) );
                    break;
            }
        }


        public void OnSwitchToTab()
        {
            SetMiniLinkImages();
        }

        public void OnSwitchAwayFromTab( SwitchAwayFromTabEventArgs e ) {}

        public void OnClosingTab( SwitchAwayFromTabEventArgs e ) {}

        public void OnTabClosed() {}


        private void SetMiniLinkImages()
        {
            foreach( var linkGroup in linkGroups )
            {
                foreach( var link in linkGroup.Links.Where( link => link.Title == "Live" ) )
                    link.MiniLinkImage = LocationSelector.GetSelectedLocations( link.Plugin ).Count > 1 ? "M37.634445,2.663L49.985756,2.663C51.928524,2.6629998,53.504002,4.2371601,53.504002,6.1826047L53.504002,9.9770002 34.114998,9.9770002 34.114998,6.1826047C34.114998,4.2371601,35.690475,2.6629998,37.634445,2.663z M4.8268046,0L23.59108,0C26.256365,-6.333994E-08,28.415255,2.1627991,28.415255,4.8268222L28.415255,15.284035 57.100006,15.284035C57.988003,15.284035,58.708,16.005341,58.708,16.893349L58.708,43.695686C58.708,44.586391,57.988003,45.305,57.100006,45.305L1.6086317,45.305C0.72069931,45.305,0,44.586391,0,43.695686L0,9.9193973 0,4.8268222C0,2.1627991,2.1614389,-6.333994E-08,4.8268046,0z" : string.Empty;
            }
        }
    }
}
