﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Common.UserInterface;
using Common.Utility;
using MasterLogViewer.Properties;
using Plugins.Interface;
using MasterLogViewer.ViewModels;
using System.IO;

namespace MasterLogViewer.Pages
{
    public partial class Browse : UserControl, ITabAdvContent
    {
        private readonly ILogViewer plugin;
	    private readonly BrowseViewModel viewModel;


        public event Action<string, string, FrameworkElement> AddNewTab;


        public Browse( ILogViewer plugin )
        {
            InitializeComponent();

            this.plugin = plugin;
            viewModel = new BrowseViewModel( plugin );
            viewModel.UpdateLogLocations();
			DataContext = viewModel;

			UserSettings.Default.PropertyChanged += UserSettingsOnPropertyChanged;
        }


		private void UserSettingsOnPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			if( e.PropertyName == FileLocationsViewModel.GetLogFolderPathSettingName( plugin ) )
				viewModel.UpdateLogLocations();
		}

        private void Folder_ItemDoubleClicked( object item )
        {
            BrowseFile( ( (FileObject)item ).Path );
        }

        private void TopContents_ItemDoubleClicked( object sender, MouseButtonEventArgs e )
        {
            var contents = (ListBox)sender;
            if( contents.SelectedItem != null )
                BrowseFile( contents.SelectedItem.ToString() );
        }

        private void BottomContents_ItemDoubleClicked( object sender, MouseButtonEventArgs e )
        {
            var contents = (TreeView)sender;
            var selectedNode = contents.SelectedItem as LogLocation.ContentInfo;
            if( selectedNode != null && selectedNode.Children.Count <= 0 )
                BrowseFile( selectedNode.FullPath );
        }


        public void OnSwitchToTab() { }

        public void OnSwitchAwayFromTab( SwitchAwayFromTabEventArgs e ) { }

		public void OnClosingTab( SwitchAwayFromTabEventArgs e )
		{
			UserSettings.Default.PropertyChanged -= UserSettingsOnPropertyChanged;
		}

        public void OnTabClosed() { }


        private void BrowseFile( string file )
        {
            var title = File.Exists( file ) ? Path.GetFileName( file ) : file;
            AddNewTab( title, plugin.IconData, new BrowseFile( plugin, file ) );
        }
    }
}
