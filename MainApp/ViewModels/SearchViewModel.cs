﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Common.Utility;
using Plugins.Interface;
using MasterLogViewer.Utilities;

namespace MasterLogViewer.ViewModels
{
    public class SearchViewModel : BindableBase
    {
        private readonly List<LogFileLoader> logFileLoaders = new List<LogFileLoader>();
        private readonly Queue<string> fileQueue = new Queue<string>();
        private readonly ILogViewer plugin;
        private readonly Dispatcher dispatcher;
        private string searchText = string.Empty;
        private bool isSearching;
        private List<string> locations = new List<string>();
        private bool searchQueued = false;
        private bool searchEnabled = true;


        public ObservableCollection<LogRow> Items { get; set; }

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged( "SearchText" );
                Commands.RaiseCanExecuteChanged();
            }
        }

        public DateTime SearchDate { get; set; }

        public bool SearchEnabled
        {
            get { return searchEnabled; }
            set
            {
                searchEnabled = value;
                OnPropertyChanged( "SearchEnabled" );
                Commands.RaiseCanExecuteChanged();
            }
        }

        public bool IsSearching
        {
            get { return isSearching; }
            set
            {
                isSearching = value;
                OnPropertyChanged( "IsSearching" );
                RefreshCmd.RaiseCanExecuteChanged();
                CancelCmd.RaiseCanExecuteChanged();
                ClipboardSearchCmd.RaiseCanExecuteChanged();
                if( !isSearching )
                    SearchCompleted();
            }
        }

        public List<string> LogLocations
        {
            get { return locations; }
            set
            {
                locations = value;
                RefreshCmd.RaiseCanExecuteChanged();
                ClipboardSearchCmd.RaiseCanExecuteChanged();

                if( searchQueued && locations.Count > 0 )
                {
                    searchQueued = false;
                    if( !IsSearching )
                        SearchAsync();
                }
            }
        }

        public RelayCommand RefreshCmd { get { return Commands.AddIfNeeded( ob => SearchAsync(), o => !string.IsNullOrEmpty( SearchText ) && SearchEnabled && !IsSearching && LogLocations.Count > 0 ); } }

        public RelayCommand CancelCmd { get { return Commands.AddIfNeeded( ob => CancelSearch(), o => IsSearching ); } }

        public RelayCommand ClipboardSearchCmd { get { return Commands.AddIfNeeded( ob => ClipboardSearch( false ), o => !IsSearching && LogLocations.Count > 0 ); } }

        public event Action SearchCompleted = delegate {};


        public SearchViewModel( Dispatcher dispatcher, ILogViewer plugin )
        {
            this.dispatcher = dispatcher;
            this.plugin = plugin;
            SearchDate = DateTime.Today;
            Items = new ObservableCollection<LogRow>();

            for( var i = 0; i < 2; i++ )
            {
                var logFileLoader = new LogFileLoader( plugin );
                logFileLoader.FileLoaded += LogFileLoaderOnFileLoaded;
                logFileLoaders.Add( logFileLoader );
            }
        }


        public void ClipboardSearch( bool queue )
        {
            var clipboardText = Clipboard.GetText( TextDataFormat.Text );
            if( !clipboardText.IsNullOrWhiteSpace() )
            {
                SearchText = clipboardText;
                if( LogLocations.Count > 0 )
                    SearchAsync();
                else
                    searchQueued = true;
            }
        }


        private void CancelSearch()
        {
            fileQueue.Clear();
            foreach( var logFileLoader in logFileLoaders )
                logFileLoader.CancelRequested = true;
        }

        private void SearchAsync()
        {
            IsSearching = true;
            searchQueued = false;
            Items.Clear();
            fileQueue.Clear();

            var sources = new List<string>();
            foreach( var location in locations )
            {
                if( Directory.Exists( location ) )
                    sources.AddRange( FileSystemUtility.GetFilesByDate( location, plugin.LogFileExtensions, SearchDate ) );
                else
                    sources.Add( location );
            }
            if( sources.Count > 0 )
            {
                foreach( var s in sources )
                    fileQueue.Enqueue( s );

                plugin.InitReadLogFields();
                DoSearch();
            }
            else
                IsSearching = false;
        }

        private void DoSearch()
        {
            foreach( var logFileLoader in logFileLoaders )
            {
                if( !logFileLoader.IsRunning && fileQueue.Count > 0 )
                {
                    logFileLoader.CancelRequested = false;
                    logFileLoader.SearchLogFileAsync( fileQueue.Dequeue(), SearchText, SearchDate );
                }
            }
        }

        private void LogFileLoaderOnFileLoaded( string filePath, List<LogRow> logRows )
        {
            //Only add chucks at a time to try and avoid UI freezes
            DispatcherUtil.Chunk( logRows, dispatcher, row => Items.Add( row ), () => logFileLoaders.Any( l => l.CancelRequested ) );

            dispatcher.BeginInvoke( DispatcherPriority.Background, new Action( delegate
            {
                if( fileQueue.Count > 0 )
                    DoSearch();
                else if( logFileLoaders.All( l => !l.IsRunning ) )
                {
                    plugin.FinishReadLogFields();
                    IsSearching = false;
                }
            } ) );
        }
    }
}