﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Threading;
using Common.Utility;
using Plugins.Interface;
using MasterLogViewer.Utilities;

namespace MasterLogViewer.ViewModels
{
	public class LiveViewModel : BindableBase
    {
        #region StandardLogLocationMonitor

        private class StandardLogLocationMonitor : ILogLocationMonitor
	    {
	        private readonly Dictionary<string, LogFileMonitor> logFileMonitors = new Dictionary<string, LogFileMonitor>();
	        private readonly FileSystemWatcher systemWatcher;
            private readonly ILogViewer plugin;
            private bool isLoaded;

            public bool IsRunning { get { return systemWatcher.EnableRaisingEvents; } }
            public string Location { get { return systemWatcher.Path; } }

            public event Action<List<Dictionary<string, object>>> FileChanged = delegate { };
            public event Action Loaded = delegate {};


            public StandardLogLocationMonitor( string location, ILogViewer plugin )
            {
                this.plugin = plugin;

                systemWatcher = new FileSystemWatcher( location, plugin.LogFileExtensions );
                systemWatcher.Created += SystemWatcherOnChangedOrCreated;
                systemWatcher.Changed += SystemWatcherOnChangedOrCreated;
            }


            public void StartMonitoring()
            {
                systemWatcher.EnableRaisingEvents = true;

                var file = FileSystemUtility.GetNewestFile( systemWatcher.Path, plugin.LogFileExtensions );

                if( !string.IsNullOrEmpty( file ) )
                    AddFileMonitor( file, false );
                else if( !isLoaded )
                {
                    isLoaded = true;
                    Loaded();
                }
            }

            public void StopMonitoring()
	        {
	            systemWatcher.EnableRaisingEvents = false;

	            foreach( var monitor in logFileMonitors )
	                monitor.Value.StopMonitoring();
	            logFileMonitors.Clear();
	        }


            private void SystemWatcherOnChangedOrCreated( object sender, FileSystemEventArgs e )
            {
                var returnInitialContents = e.ChangeType == WatcherChangeTypes.Created;

                if( !logFileMonitors.ContainsKey( e.FullPath ) )
                {
                    AddFileMonitor( e.FullPath, returnInitialContents );
                }
                else if( !logFileMonitors[e.FullPath].IsRunning )
                {
                    logFileMonitors[e.FullPath].StartMonitoring( returnInitialContents );
                }
            }

            private void MonitorOnFileChanged( List<Dictionary<string, object>> list )
            {
                if( !isLoaded )
                {
                    isLoaded = true;
                    Loaded();
                }

                if( list != null && list.Count > 0 )
                    FileChanged( list );
            }
            

	        private void AddFileMonitor( string file, bool returnInitialContents )
	        {
	            lock( logFileMonitors )
	            {
	                if( !logFileMonitors.ContainsKey( file ) )
	                {
	                    var monitor = new LogFileMonitor( file, plugin );
	                    monitor.FileChanged += MonitorOnFileChanged;
	                    logFileMonitors.Add( file, monitor );

	                    monitor.StartMonitoring( returnInitialContents );
	                }
	            }
	        }
	    }

	    #endregion

        private readonly ILogViewer plugin;
        private readonly List<ILogLocationMonitor> locationMonitors = new List<ILogLocationMonitor>();
        private readonly Dispatcher dispatcher;
        private List<string> locations = new List<string>();
		private bool isAutoScrolling;
		private int numberOfMonitorsLeftToLoad;
        private bool startQueued;


        public ObservableCollection<LogRow> Items { get; set; }

        public List<string> LogLocations
        {
            get { return locations; }
            set
            {
                locations = value;
                UpdateLogLocations();
                
                if( startQueued && locations.Count > 0 )
                {
                    startQueued = false;
                    if( !IsRunning )
                        Start();
                }
            }
        }

        public bool IsRunning { get { return ( locationMonitors.Count > 0 && locationMonitors.Any( w => w.IsRunning ) ); } }

		public bool IsAutoScrolling
		{
			get { return isAutoScrolling; }
			set
			{
				isAutoScrolling = value;
				OnPropertyChanged( "IsAutoScrolling" );
			}
		}

        public bool IsLoading { get { return numberOfMonitorsLeftToLoad > 0 && IsRunning; } }

        private int NumberOfMonitorsLeftToLoad
	    {
            get { return numberOfMonitorsLeftToLoad; }
            set
            {
                numberOfMonitorsLeftToLoad = value;
                OnPropertyChanged( "IsLoading" );
            }
	    }

        public RelayCommand ToggleRunningCmd { get { return Commands.AddIfNeeded( ToggleRunning, o => LogLocations.Count > 0 ); } }

        public RelayCommand ToggleAutoScrollCmd { get { return Commands.AddIfNeeded( o => IsAutoScrolling = !IsAutoScrolling ); } }

        public RelayCommand CleanCmd { get { return Commands.AddIfNeeded( o => Items.Clear() ); } }


        public LiveViewModel( Dispatcher dispatcher, ILogViewer plugin )
		{
            this.plugin = plugin;
            this.dispatcher = dispatcher;

            Items = new ObservableCollection<LogRow>();
            LogLocations = new List<string>();
			IsAutoScrolling = true;
		}


	    public void Start()
		{
            if( !IsRunning )
			{
                if( LogLocations.Count > 0 )
                {
                    startQueued = false;
                    plugin.InitReadLogFields();

                    foreach( var monitor in locationMonitors )
                    {
                        try
                        {
                            monitor.StartMonitoring();
                        }
                        catch( Exception ex )
                        {
                            if( IsLoading )
                                NumberOfMonitorsLeftToLoad--;
                            Error.DisplayAndLogError( Error.ErrorType.ShowAndContinue, "Error occurred while attempting to monitor location " + monitor.Location, ex );
                        }
                    }

                    OnPropertyChanged( "IsRunning", "IsLoading" );
                }
                else
                    startQueued = true;
			}
		}

		public void Stop()
		{
			if( IsRunning )
            {
                foreach( var monitor in locationMonitors )
                    monitor.StopMonitoring();
                plugin.FinishReadLogFields();

                OnPropertyChanged( "IsRunning", "IsLoading" );
			}
		}


        private void MonitorOnLoaded()
        {
            NumberOfMonitorsLeftToLoad--;
        }

        private void MonitorOnFileChanged( List<Dictionary<string, object>> rows )
        {
            //Only add chucks at a time to try and avoid UI freezes
            DispatcherUtil.Chunk( rows.Select( r => new LogRow( r ) ).ToList(), dispatcher, row => Items.Add( row ), () => !IsRunning );
        }


        private void ToggleRunning( object param )
        {
            if( IsRunning )
                Stop();
            else
                Start();
        }

        private void UpdateLogLocations()
        {
            var wasRunning = IsRunning;
            Stop();

            locationMonitors.Clear();

            foreach( var location in LogLocations )
            {
                var monitor = plugin.CreateLocationMonitor( location ) ?? new StandardLogLocationMonitor( location, plugin );
                monitor.Loaded += MonitorOnLoaded;
                monitor.FileChanged += MonitorOnFileChanged;
                locationMonitors.Add( monitor );
            }
            NumberOfMonitorsLeftToLoad = locationMonitors.Count;

            if( wasRunning )
                Start();

            ToggleRunningCmd.RaiseCanExecuteChanged();
        }
    }
}