﻿using System.Collections.ObjectModel;
using Common.Utility;
using Plugins.Interface;
using MasterLogViewer.Properties;

namespace MasterLogViewer.ViewModels
{
    public class BrowseViewModel : BindableBase
    {
        private readonly ILogViewer plugin;

        private LogLocation selectedFileLocation;

        public LogLocation SelectedFileLocation
        {
            get { return selectedFileLocation; }
            set
            {
                if( selectedFileLocation != value && value != null )
                {
                    selectedFileLocation = value;
                    OnPropertyChanged( "SelectedFileLocation" );

                    UserSettings.Default[GetSelectedIndexSettingName( plugin )] = FileLocations.IndexOf( SelectedFileLocation );
                    UserSettings.Default.Save();
                }
            }
        }

        public ObservableCollection<LogLocation> FileLocations { get; private set; }


        public BrowseViewModel( ILogViewer plugin )
        {
            this.plugin = plugin;
            FileLocations = new ObservableCollection<LogLocation>();
        }

        public void UpdateLogLocations()
        {
            FileLocations.Clear();
            if( plugin.BrowseLogLocations )
            {
                foreach( var fileLocation in FileLocationsViewModel.GetLogFileLocations( plugin ) )
                    FileLocations.Add( new LogLocation( plugin, fileLocation, true ) );
            }

            foreach( var browseLocation in plugin.GetBrowseLocations() )
                FileLocations.Add( new LogLocation( plugin, browseLocation, true ) );

            var customPath = UserSettings.Default[LogLocation.GetBrowseFolderPathSettingName( plugin )] as string;
            if( string.IsNullOrEmpty( customPath ) )
                customPath = plugin.GetLocalLogLocation();
            FileLocations.Add( new LogLocation( plugin, customPath, false ) );

            var settingName = GetSelectedIndexSettingName( plugin );
            var selectedIndex = UserSettings.Default[settingName] != null ? (int)UserSettings.Default[settingName] : 0;
            if( selectedIndex < 0 || selectedIndex >= FileLocations.Count )
                selectedIndex = 0;
            if( selectedIndex >= 0 && selectedIndex < FileLocations.Count )
                SelectedFileLocation = FileLocations[selectedIndex];
        }

        public static string GetSelectedIndexSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-BrowseFolderSelectedIndex";
        }
    }
}