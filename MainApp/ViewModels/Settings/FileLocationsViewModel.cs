﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using Common.Utility;
using MasterLogViewer.Properties;
using System.Linq;
using System.Collections.Generic;
using Plugins.Interface;
using System.IO;

namespace MasterLogViewer.ViewModels
{
    public class FileLocationsViewModel : BindableBase
    {
        private readonly ILogViewer plugin;

        public ObservableCollection<LogLocation> LogFileLocations { get; private set; }

        public RelayCommand AddCmd { get { return Commands.AddIfNeeded( AddNew ); } }

        public RelayCommand ReloadCmd { get { return Commands.AddIfNeeded( Reload ); } }


        public FileLocationsViewModel( ILogViewer plugin )
        {
            this.plugin = plugin;
            LogFileLocations = new ObservableCollection<LogLocation>();
            LogFileLocations.CollectionChanged += LogFileLocationsOnCollectionChanged;

            LoadLogLocations();
        }


        public void SaveChanges()
        {
            SaveChanges( plugin, LogFileLocations.Select( l => l.Path ) );
        }


        private void LogFileLocationsOnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            if( e.NewItems != null )
            {
                foreach( LogLocation item in e.NewItems )
                {
                    item.PropertyChanged += ItemOnPropertyChanged;
                    item.Delete += ItemOnDelete;
                }
            }

            if( e.OldItems != null )
            {
                foreach( LogLocation item in e.OldItems )
                {
                    item.PropertyChanged -= ItemOnPropertyChanged;
                    item.Delete -= ItemOnDelete;
                }
            }
        }


        private void ItemOnPropertyChanged( object sender, PropertyChangedEventArgs propertyChangedEventArgs )
        {
            SaveChanges();
        }

        private void ItemOnDelete( LogLocation logFileLocation )
        {
            LogFileLocations.Remove( logFileLocation );
            SaveChanges();
        }


        private void AddNew( object arg )
        {
            LogFileLocations.Add( new LogLocation( plugin, plugin.GetLocalLogLocation(), false ) );
            SaveChanges();
        }

        private void Reload( object arg )
        {
            UserSettings.Default[GetLogFolderPathSettingName( plugin )] = new StringCollection();
            UserSettings.Default.Save();
            LoadLogLocations();
        }

        private void LoadLogLocations()
        {
            LogFileLocations.Clear();
            foreach( var fileLocation in GetLogFileLocations( plugin ) )
                LogFileLocations.Add( new LogLocation( plugin, fileLocation, false ) );
        }


        #region static

        public static List<string> GetLogFileLocations( ILogViewer plugin )
        {
            var logLocations = new List<string>();

            var folderPaths = UserSettings.Default[GetLogFolderPathSettingName( plugin )];
            if( folderPaths != null )
                logLocations.AddRange( ( (StringCollection)folderPaths ).Cast<string>() );

            if( logLocations.Count <= 0 )
            {
                logLocations = plugin.GetDefaultLogFileLocations( ApplicationInfo.GetFullComputerName() );

                var locations = SaveChanges( plugin, logLocations );
                logLocations = new List<string>( locations.Cast<string>() );
            }

            return logLocations.ToList();
        }

        private static StringCollection SaveChanges( ILogViewer plugin, IEnumerable<string> logLocations )
        {
            var saveLocations = new StringCollection();
            foreach( var fileLocation in logLocations )
            {
                var location = Directory.Exists( fileLocation ) ? FileSystemUtility.StandarizePath( fileLocation ) : fileLocation;
                if( !saveLocations.Contains( location ) )
                    saveLocations.Add( location );
            }
            UserSettings.Default[GetLogFolderPathSettingName( plugin )] = saveLocations;
            UserSettings.Default.Save();
            return saveLocations;
        }

        public static string GetLogFolderPathSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-LogFolderPaths";
        }

        #endregion
    }
}