﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using Common.Utility;
using MasterLogViewer.Properties;
using System.Linq;
using System.Collections.Generic;
using Plugins.Interface;

namespace MasterLogViewer.ViewModels
{
    #region Filter

    public class Filter : BindableBase
    {
        private string title = string.Empty;
        private string value = string.Empty;
        private bool selected = false;

        public string Title
        {
            get { return title; }
            set
            {
                if( title != value )
                {
                    title = value;
                    OnPropertyChanged( "Title" );
                }
            }
        }

        public string Value
        {
            get { return value; }
            set
            {
                if( this.value != value )
                {
                    this.value = value;
                    OnPropertyChanged( "Value" );
                }
            }
        }

        public bool IsSelected
        {
            get { return selected; }
            set
            {
                if( selected != value )
                {
                    selected = value;
                    OnPropertyChanged( "IsSelected" );
                }
            }
        }


        public RelayCommand DeleteCmd { get { return Commands.AddIfNeeded( o => Delete( this ) ); } }

        public event Action<Filter> Delete = delegate { };


        public Filter( string title, string value )
        {
            this.title = title;
            this.value = value;
        }
    }

    #endregion


    public class FiltersViewModel : BindableBase
    {
        private const string serializationSeparator = "||";
        private readonly ILogViewer plugin;

        public ObservableCollection<Filter> Filters { get; private set; }

        public RelayCommand AddCmd { get { return Commands.AddIfNeeded( AddNew ); } }

        public RelayCommand ReloadCmd { get { return Commands.AddIfNeeded( Reload ); } }


        public FiltersViewModel( ILogViewer plugin )
        {
            this.plugin = plugin;
            Filters = new ObservableCollection<Filter>();
            Filters.CollectionChanged += FiltersOnCollectionChanged;

            LoadFilters();
        }


        public void SaveChanges()
        {
            SaveChanges( plugin, Filters );
        }


        private void FiltersOnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            if( e.NewItems != null )
            {
                foreach( Filter item in e.NewItems )
                {
                    item.PropertyChanged += ItemOnPropertyChanged;
                    item.Delete += ItemOnDelete;
                }
            }

            if( e.OldItems != null )
            {
                foreach( Filter item in e.OldItems )
                {
                    item.PropertyChanged -= ItemOnPropertyChanged;
                    item.Delete -= ItemOnDelete;
                }
            }
        }


        private void ItemOnPropertyChanged( object sender, PropertyChangedEventArgs propertyChangedEventArgs )
        {
            SaveChanges();
        }

        private void ItemOnDelete( Filter filter )
        {
            Filters.Remove( filter );
            SaveChanges();
        }


        private void AddNew( object arg )
        {
            Filters.Add( new Filter( string.Empty, string.Empty ) );
            SaveChanges();
        }

        private void Reload( object arg )
        {
            UserSettings.Default[GetFiltersSettingName( plugin )] = new StringCollection();
            UserSettings.Default.Save();
            LoadFilters();
        }

        private void LoadFilters()
        {
            Filters.Clear();
            foreach( var filter in GetFilters( plugin ) )
                Filters.Add( filter );
        }


        #region static

        public static List<Filter> GetFilters( ILogViewer plugin )
        {
            var filters = new List<Filter>();

            var filtersSerialized = UserSettings.Default[GetFiltersSettingName( plugin )];
            if( filtersSerialized != null )
            {
                filters.AddRange( ( (StringCollection)filtersSerialized ).Cast<string>().Select(
                    f =>
                    {
                        var index = f.IndexOf( serializationSeparator );
                        return new Filter( f.Substring( 0, index ), f.Substring( index + serializationSeparator.Length ) );
                    } ) );
            }

            if( filters.Count <= 0 )
            {
                filters = plugin.GetDefaultFilters().Select( f => new Filter( f.Key, f.Value ) ).ToList();
                SaveChanges( plugin, filters );
            }

            return filters.ToList();
        }

        private static void SaveChanges( ILogViewer plugin, IEnumerable<Filter> filters )
        {
            var saveFilters = new StringCollection();
            foreach( var filter in filters )
                saveFilters.Add( filter.Title + serializationSeparator + filter.Value );

            UserSettings.Default[GetFiltersSettingName( plugin )] = saveFilters;
            UserSettings.Default.Save();
        }

        public static string GetFiltersSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-Filters";
        }

        #endregion
    }
}