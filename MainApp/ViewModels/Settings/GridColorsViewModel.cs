﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using Common.Utility;
using Plugins.Interface;
using MasterLogViewer.Properties;
using System.Collections.Generic;
using System.Windows.Media;
using System.Text;

namespace MasterLogViewer.ViewModels
{
    #region ColorRule

    public class ColorRule : BindableBase
    {
        private readonly ILogViewer plugin;
        private const string serializeSeperator = "~|~";
        private Color backColor;
        private Color forColor;


        public ObservableCollection<ColorFilter> Filters { get; private set; }

        public Color BackColor
        {
            get { return backColor; }
            set
            {
                if( backColor != value )
                {
                    backColor = value;
                    OnPropertyChanged( "BackColor" );
                }
            }
        }

        public Color ForColor
        {
            get { return forColor; }
            set
            {
                if( forColor != value )
                {
                    forColor = value;
                    OnPropertyChanged( "ForColor" );
                }
            }
        }

        public string DisplayText
        {
            get
            {
                var display = new StringBuilder();

                foreach( var filter in Filters )
                {
                    display.Append( filter.Column );

                    switch( filter.Operation )
                    {
                        case ColorFilterOperations.Equals:
                            display.Append( " == " );
                            break;
                        case ColorFilterOperations.Contains:
                            display.Append( " .Contains " );
                            break;
                    }

                    display.Append( filter.Value );

                    if( Filters.IndexOf( filter ) < Filters.Count - 1 )
                    {
                        switch( filter.Command )
                        {
                            case ColorFilterCommands.And:
                                display.Append( " && " );
                                break;
                            case ColorFilterCommands.Or:
                                display.Append( " || " );
                                break;
                        }
                    }
                }

                return display.ToString();
            }
        }

        public RelayCommand AddCmd { get { return Commands.AddIfNeeded( o => Filters.Add( new ColorFilter( plugin ) ) ); } }

        public RelayCommand DeleteCmd { get { return Commands.AddIfNeeded( o => Delete( this ) ); } }

        public event Action<ColorRule> Delete = delegate { };

        public event Action FilterChanged = delegate { };


        public ColorRule( ILogViewer plugin )
        {
            this.plugin = plugin;
            Filters = new ObservableCollection<ColorFilter>();
            Filters.CollectionChanged += FiltersOnCollectionChanged;
            backColor = Colors.Red;
            forColor = Colors.White;
        }

        public ColorRule( ILogViewer plugin, Color backColor, Color forColor )
            : this( plugin )
        {
            this.backColor = backColor;
            this.forColor = forColor;
        }

        public ColorRule( ILogViewer plugin, string serialized )
            : this( plugin )
        {
            var values = serialized.Split( new[] { serializeSeperator }, StringSplitOptions.RemoveEmptyEntries );
            backColor = (Color)ColorConverter.ConvertFromString( values[0] );
            forColor = (Color)ColorConverter.ConvertFromString( values[1] );
            for( int i = 2; i < values.Length; i++ )
                Filters.Add( new ColorFilter( plugin, values[i] ) );
        }


        public string Serialize()
        {
            var serialized = new StringBuilder();
            serialized.Append( BackColor + serializeSeperator + ForColor + serializeSeperator );
            foreach( var filter in Filters )
                serialized.Append( filter.Serialize() + serializeSeperator );
            return serialized.ToString();
        }


        private void FiltersOnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            if( e.NewItems != null )
            {
                foreach( ColorFilter colorFilter in e.NewItems )
                {
                    colorFilter.PropertyChanged  += ColorFilterOnPropertyChanged;
                    colorFilter.Delete += ColorFilterOnDelete;
                }
            }

            if( e.OldItems != null )
            {
                foreach( ColorFilter colorFilter in e.OldItems )
                {
                    colorFilter.PropertyChanged -= ColorFilterOnPropertyChanged;
                    colorFilter.Delete -= ColorFilterOnDelete;
                }
            }

            OnPropertyChanged( "DisplayText" );
            FilterChanged();
        }

        private void ColorFilterOnPropertyChanged( object sender, PropertyChangedEventArgs propertyChangedEventArgs )
        {
            OnPropertyChanged( "DisplayText" );
            FilterChanged();
        }

        private void ColorFilterOnDelete( ColorFilter colorFilter )
        {
            Filters.Remove( colorFilter );
            OnPropertyChanged( "DisplayText" );
            FilterChanged();
        }
    }

    #endregion

    #region ColorFilter

    public class ColorFilter : BindableBase
    {
        private readonly ILogViewer plugin;
        private const string serializeSeperator = "|";
        private string column;
        private ColorFilterOperations operation;
        private string value;
        private ColorFilterCommands command;


        public string Column
        {
            get { return column; }
            set
            {
                if( column != value )
                {
                    column = value;
                    OnPropertyChanged( "Column" );
                }
            }
        }

        public ColorFilterOperations Operation
        {
            get { return operation; }
            set
            {
                if( operation != value )
                {
                    operation = value;
                    OnPropertyChanged( "Operation" );
                }
            }
        }

        public string Value
        {
            get { return value; }
            set
            {
                if( this.value != value )
                {
                    this.value = value;
                    OnPropertyChanged( "Value" );
                }
            }
        }

        public ColorFilterCommands Command
        {
            get { return command; }
            set
            {
                if( command != value )
                {
                    command = value;
                    OnPropertyChanged( "Command" );
                }
            }
        }

        public IList<string> AvailableColumns { get { return GridColumnsViewModel.GetColumnSettings( plugin ).Select( c => c.Header ).ToList(); } }

        public IList<ColorFilterOperations> AvailableOperations { get { return Enum.GetValues( typeof( ColorFilterOperations ) ).Cast<ColorFilterOperations>().ToList(); } }

        public IList<ColorFilterCommands> AvailableCommands { get { return Enum.GetValues( typeof( ColorFilterCommands ) ).Cast<ColorFilterCommands>().ToList(); } }


        public RelayCommand DeleteCmd { get { return Commands.AddIfNeeded( o => Delete( this ) ); } }

        public event Action<ColorFilter> Delete = delegate { };


        public ColorFilter( ILogViewer plugin )
        {
            this.plugin = plugin;
            column = AvailableColumns[0];
            operation = AvailableOperations[0];
            value = string.Empty;
            command = AvailableCommands[0];
        }

        public ColorFilter( ILogViewer plugin, string column, ColorFilterOperations operation, string value, ColorFilterCommands command )
        {
            this.plugin = plugin;
            this.column = column;
            this.operation = operation;
            this.value = value;
            this.command = command;
        }

        public ColorFilter( ILogViewer plugin, string serialized )
        {
            this.plugin = plugin;
            var values = serialized.Split( new[] { serializeSeperator }, StringSplitOptions.None );
            column = values[0];
            operation = (ColorFilterOperations)Enum.Parse( typeof(ColorFilterOperations), values[1] );
            value = values[2];
            command = (ColorFilterCommands)Enum.Parse( typeof(ColorFilterCommands), values[3] );
        }

        public string Serialize()
        {
            return Column + serializeSeperator + Operation + serializeSeperator + Value + serializeSeperator + Command;
        }
    }

    #endregion


    public class GridColorsViewModel : BindableBase
    {
        private readonly ILogViewer plugin;

        public ObservableCollection<ColorRule> ColorRules { get; private set; }

        public RelayCommand AddCmd { get { return Commands.AddIfNeeded( AddNew ); } }

        public RelayCommand ReloadCmd { get { return Commands.AddIfNeeded( Reload ); } }


        public GridColorsViewModel( ILogViewer plugin )
        {
            this.plugin = plugin;
            ColorRules = new ObservableCollection<ColorRule>();
            ColorRules.CollectionChanged += ColorRulesOnCollectionChanged;

            LoadColorRules();
        }

        private void ColorRulesOnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            if( e.NewItems != null )
            {
                foreach( ColorRule colorRule in e.NewItems )
                {
                    colorRule.PropertyChanged += ColorRuleOnPropertyChanged;
                    colorRule.FilterChanged += ColorRuleOnFilterChanged;
                    colorRule.Delete += ColorRuleOnDelete;
                }
            }

            if( e.OldItems != null )
            {
                foreach( ColorRule colorRule in e.OldItems )
                {
                    colorRule.PropertyChanged -= ColorRuleOnPropertyChanged;
                    colorRule.FilterChanged -= ColorRuleOnFilterChanged;
                    colorRule.Delete -= ColorRuleOnDelete;
                }
            }

            SaveChanges();
        }

        private void ColorRuleOnDelete( ColorRule colorRule )
        {
            ColorRules.Remove( colorRule );
        }

        private void ColorRuleOnPropertyChanged( object sender, PropertyChangedEventArgs propertyChangedEventArgs )
        {
            SaveChanges();
        }

        private void ColorRuleOnFilterChanged()
        {
            SaveChanges();
        }


        private void AddNew( object o )
        {
            var rule = new ColorRule( plugin );
            rule.Filters.Add( new ColorFilter( plugin, "Level", ColorFilterOperations.Equals, "Critical", ColorFilterCommands.Or ) );
            ColorRules.Add( rule );
        }

        private void SaveChanges()
        {
            var serialized = new StringCollection();
            foreach( var rule in ColorRules )
                serialized.Add( rule.Serialize() );
            UserSettings.Default[GetColorRulesSettingName( plugin )] = serialized;
            UserSettings.Default.Save();
        }

        private void Reload( object arg )
        {
            UserSettings.Default[GetColorRulesSettingName( plugin )] = new StringCollection();
            UserSettings.Default.Save();
            LoadColorRules();
        }

        private void LoadColorRules()
        {
            if( ColorRules.Count > 0 )
                ColorRules.Clear();
            foreach( var colorRule in GetColorRuleSettings( plugin ) )
                ColorRules.Add( colorRule );
        }


        #region static

        public static List<ColorRule> GetColorRuleSettings( ILogViewer plugin )
        {
            var rules = new List<ColorRule>();

            var colorRules = UserSettings.Default[GetColorRulesSettingName( plugin )];
            if( colorRules != null )
            {
                try
                {
                    foreach( var color in (StringCollection)colorRules )
                        rules.Add( new ColorRule( plugin, color ) );
                }
                catch( Exception ex )
                {
                    LogUtility.Append( "Failed loading grid color settings" + Environment.NewLine + ex, true );
                }
            }

            if( rules.Count <= 0 )
            {
                foreach( var colorRuleInfo in plugin.GetDefaultColorRules() )
                {
                    var rule = new ColorRule( plugin, colorRuleInfo.BackColor, colorRuleInfo.ForColor );
                    foreach( var filterInfo in colorRuleInfo.Filters )
                        rule.Filters.Add( new ColorFilter( plugin, filterInfo.Column, filterInfo.Operation, filterInfo.Value, filterInfo.Command ) );
                    rules.Add( rule );
                }
            }

            return rules;
        }

        public static string GetColorRulesSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-ColorRules";
        }

        #endregion
    }
}