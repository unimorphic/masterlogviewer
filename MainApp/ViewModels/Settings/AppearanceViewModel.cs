﻿using System;
using System.Windows;
using FirstFloor.ModernUI.Presentation;
using System.ComponentModel;
using System.Linq;
using System.Windows.Media;
using MasterLogViewer.Properties;
using Common.Utility;

namespace MasterLogViewer.ViewModels
{
    /// <summary>
    /// A simple view model for configuring theme, font and accent colors.
    /// </summary>
    public class AppearanceViewModel : BindableBase
    {
        private const string FontSmall = "small";
        private const string FontLarge = "large";

        // 20 accent colors from Windows Phone 8
        private readonly Color[] accentColors = new []{
            Color.FromRgb(0xa4, 0xc4, 0x00),   // lime
            Color.FromRgb(0x60, 0xa9, 0x17),   // green
            Color.FromRgb(0x00, 0x8a, 0x00),   // emerald
            Color.FromRgb(0x00, 0xab, 0xa9),   // teal
            Color.FromRgb(0x1b, 0xa1, 0xe2),   // cyan
            Color.FromRgb(0x00, 0x50, 0xef),   // cobalt
            Color.FromRgb(0x6a, 0x00, 0xff),   // indigo
            Color.FromRgb(0xaa, 0x00, 0xff),   // violet
            Color.FromRgb(0xf4, 0x72, 0xd0),   // pink
            Color.FromRgb(0xd8, 0x00, 0x73),   // magenta
            Color.FromRgb(0xa2, 0x00, 0x25),   // crimson
            Color.FromRgb(0xe5, 0x14, 0x00),   // red
            Color.FromRgb(0xfa, 0x68, 0x00),   // orange
            Color.FromRgb(0xf0, 0xa3, 0x0a),   // amber
            Color.FromRgb(0xe3, 0xc8, 0x00),   // yellow
            Color.FromRgb(0x82, 0x5a, 0x2c),   // brown
            Color.FromRgb(0x6d, 0x87, 0x64),   // olive
            Color.FromRgb(0x64, 0x76, 0x87),   // steel
            Color.FromRgb(0x76, 0x60, 0x8a),   // mauve
            Color.FromRgb(0x87, 0x79, 0x4e),   // taupe
        };

        private Color selectedAccentColor;
        private LinkCollection themes = new LinkCollection();
        private Link selectedTheme;
        private double selectedGridFontSize;
        private double selectedZoom;


        public LinkCollection Themes
        {
            get { return themes; }
        }

        public string[] FontSizes
        {
            get { return new[] { FontSmall, FontLarge }; }
        }

        public Color[] AccentColors
        {
            get { return accentColors; }
        }

        public Link SelectedTheme
        {
            get { return selectedTheme; }
            set
            {
                if( selectedTheme != value )
                {
                    selectedTheme = value;
                    OnPropertyChanged( "SelectedTheme" );

                    // and update the actual theme
                    AppearanceManager.Current.ThemeSource = value.Source;
                    UserSettings.Default.Theme = AppearanceManager.Current.ThemeSource.ToString();
                    UserSettings.Default.Save();
                }
            }
        }

        public double SelectedGridFontSize
        {
            get { return selectedGridFontSize; }
            set
            {
                if( Math.Abs( selectedGridFontSize - value ) > 0.001 )
                {
                    selectedGridFontSize = value;
                    OnPropertyChanged( "SelectedGridFontSize" );

                    UserSettings.Default.GridFontSize = value;
                    UserSettings.Default.Save();
                }
            }
        }

        public Color SelectedAccentColor
        {
            get { return selectedAccentColor; }
            set
            {
                if( selectedAccentColor != value )
                {
                    selectedAccentColor = value;
                    OnPropertyChanged( "SelectedAccentColor" );

                    AppearanceManager.Current.AccentColor = value;
                    UserSettings.Default.AccentColor = AppearanceManager.Current.AccentColor.ToString();
                    UserSettings.Default.Save();
                }
            }
        }

        public double SelectedZoom
        {
            get { return selectedZoom; }
            set
            {
                if( Math.Abs( selectedZoom - value ) > 0.001 )
                {
                    selectedZoom = value;
                    OnPropertyChanged( "SelectedZoom" );

                    if( Application.Current.Resources.Contains( "windowTransform" ) )
                    {
                        var trans = (TransformGroup)Application.Current.Resources["windowTransform"];

                        if( Math.Abs( ( (ScaleTransform)trans.Children[0] ).ScaleX - value ) > 0.001 )
                        {
                            trans = new TransformGroup();
                            trans.Children.Add( new ScaleTransform( value, value ) );
                            Application.Current.Resources["windowTransform"] = trans;
                        }
                    }
                    
                    UserSettings.Default.Zoom = value;
                    UserSettings.Default.Save();
                }
            }
        }

        public Common.Utility.RelayCommand ReloadCmd { get { return Commands.AddIfNeeded( Reload ); } }


        public AppearanceViewModel()
        {
            // add the default themes
            themes.Add( new Link { DisplayName = "dark", Source = AppearanceManager.DarkThemeSource } );
            themes.Add( new Link { DisplayName = "light", Source = AppearanceManager.LightThemeSource } );
            SyncSettings();

            AppearanceManager.Current.PropertyChanged += OnAppearanceManagerPropertyChanged;
        }

        private void SyncSettings()
        {
            // synchronizes the selected viewmodel theme with the actual theme used by the appearance manager.
            SelectedTheme = themes.FirstOrDefault( l => l.Source.Equals( AppearanceManager.Current.ThemeSource ) );

            // and make sure accent color is up-to-date
            SelectedAccentColor = AppearanceManager.Current.AccentColor;

            SelectedZoom = UserSettings.Default.Zoom;
            SelectedGridFontSize = UserSettings.Default.GridFontSize;
        }

        private void OnAppearanceManagerPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if( e.PropertyName == "ThemeSource" || e.PropertyName == "AccentColor" || e.PropertyName == "FontSize" )
                SyncSettings();
        }

        private void Reload( object arg )
        {
            UserSettings.Default.Theme = AppearanceManager.LightThemeSource.ToString();
            UserSettings.Default.GridFontSize = 13;
            UserSettings.Default.AccentColor = AppearanceManager.DefaultAccentColor.ToString();
            UserSettings.Default.Zoom = 1;
            UserSettings.Default.Save();

            ApplySettings();
            SyncSettings();
        }


        #region static

        public static void ApplySettings()
        {
            if( !string.IsNullOrEmpty( UserSettings.Default.Theme ) )
            {
                AppearanceManager.Current.ThemeSource = new Uri( UserSettings.Default.Theme, UriKind.Relative );
                AppearanceManager.Current.AccentColor = (Color)ColorConverter.ConvertFromString( UserSettings.Default.AccentColor );
            }
        }

        #endregion
    }
}
