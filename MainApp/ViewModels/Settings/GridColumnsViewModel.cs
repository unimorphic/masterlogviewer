﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Common.Utility;
using Plugins.Interface;
using MasterLogViewer.Properties;
using System.Collections.Generic;

namespace MasterLogViewer.ViewModels
{
    #region FileColumn

    public class FileColumn : BindableBase
    {
        private bool visible;
        private bool toolTip;


        public string Header { get; set; }

        public string BindingPath { get; set; }

        public bool IsVisible
        {
            get { return visible; }
            set
            {
                if( visible != value )
                {
                    visible = value;
                    OnPropertyChanged( "IsVisible" );
                }
            }
        }

        public bool ShowToolTip
        {
            get { return toolTip; }
            set
            {
                if( toolTip != value )
                {
                    toolTip = value;
                    OnPropertyChanged( "ShowToolTip" );
                }
            }
        }

        public RelayCommand MoveDownCmd { get { return Commands.AddIfNeeded( o => Move( this, false ) ); } }

        public RelayCommand MoveUpCmd { get { return Commands.AddIfNeeded( o => Move( this, true ) ); } }

        public event Action<FileColumn, bool> Move = delegate {};


        public FileColumn( string header, string bindingPath, bool isVisible, bool toolTip )
        {
            Header = header;
            BindingPath = bindingPath;
            visible = isVisible;
            this.toolTip = toolTip;
        }

        public FileColumn( string serialized )
        {
            var values = serialized.Split( new[] {"|"}, StringSplitOptions.RemoveEmptyEntries );
            Header = values[0];
            BindingPath = values[1];
            IsVisible = bool.Parse( values[2] );
            if( values.Length > 3 )
                ShowToolTip = bool.Parse( values[3] );
        }

        public string Serialize()
        {
            return Header + "|" + BindingPath + "|" + IsVisible + "|" + ShowToolTip;
        }

        public override string ToString()
        {
            return Header;
        }
    }

    #endregion

    public class GridColumnsViewModel : BindableBase
    {
        private readonly ILogViewer plugin;
        private FileColumn defaultSortColumn;
        private ListSortDirection defaultSortAscending;
        private bool loading = true;


        public ObservableCollection<FileColumn> Columns { get; private set; }

        public ObservableCollection<FileColumn> ColumnsChoice { get; private set; }

        public FileColumn DefaultSortColumn
        {
            get { return defaultSortColumn; }
            set
            {
                if( value != null )
                {
                    defaultSortColumn = value;
                    OnPropertyChanged( "DefaultSortColumn" );
                    SaveChanges();
                }
            }
        }

        public ListSortDirection DefaultSortAscending
        {
            get { return defaultSortAscending; }
            set
            {
                defaultSortAscending = value;
                OnPropertyChanged( "DefaultSortAscending" );
                SaveChanges();
            }
        }

        public RelayCommand ReloadCmd { get { return Commands.AddIfNeeded( Reload ); } }


        public GridColumnsViewModel( ILogViewer plugin )
        {
            this.plugin = plugin;
            Columns = new ObservableCollection<FileColumn>();
            Columns.CollectionChanged += ColumnsOnCollectionChanged;
            ColumnsChoice = new ObservableCollection<FileColumn>();
        }


        public void LoadColumns()
        {
            try
            {
                loading = true;

                Columns.Clear();
                foreach( var column in GetColumnSettings( plugin ) )
                    Columns.Add( column );

                ColumnsChoice.Clear();
                foreach( var column in Columns )
                    ColumnsChoice.Add( column );
                ColumnsChoice.Insert( 0, new FileColumn( "[None]", string.Empty, false, false ) );

                var columnBindingPath = GetDefaultSortColumnBinding( plugin );
                DefaultSortColumn = ColumnsChoice.FirstOrDefault( c => c.BindingPath == columnBindingPath );

                DefaultSortAscending = GetDefaultSortAscending( plugin ) ? ListSortDirection.Ascending : ListSortDirection.Descending;
            }
            finally
            {
                loading = false;
            }
        }


        private void ColumnsOnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            if( e.NewItems != null )
            {
                foreach( FileColumn column in e.NewItems )
                {
                    column.PropertyChanged += ColumnOnPropertyChanged;
                    column.Move += ColumnOnMove;
                }
            }

            if( e.OldItems != null )
            {
                foreach( FileColumn column in e.OldItems )
                {
                    column.PropertyChanged -= ColumnOnPropertyChanged;
                    column.Move -= ColumnOnMove;
                }
            }
        }

        private void ColumnOnMove( FileColumn fileColumn, bool up )
        {
            var index = Columns.IndexOf( fileColumn );
            var newIndex = up ? index - 1 : index + 1;
            if( newIndex >= 0 && newIndex < Columns.Count )
            {
                Columns.Move( index, newIndex );
                SaveChanges();
            }
        }

        private void ColumnOnPropertyChanged( object sender, PropertyChangedEventArgs propertyChangedEventArgs )
        {
            SaveChanges();
        }

        private void SaveChanges()
        {
            if( !loading )
            {
                var serialized = new StringCollection();
                foreach( var column in Columns )
                    serialized.Add( column.Serialize() );
                UserSettings.Default[GetFileViewColumnsSettingName( plugin )] = serialized;

                UserSettings.Default[GetDefaultSortColumnSettingName( plugin )] = DefaultSortColumn != null ? DefaultSortColumn.BindingPath : null;
                UserSettings.Default[GetDefaultSortAscendingSettingName( plugin )] = DefaultSortAscending == ListSortDirection.Ascending;

                UserSettings.Default.Save();
            }
        }

        private void Reload( object arg )
        {
            UserSettings.Default[GetFileViewColumnsSettingName( plugin )] = new StringCollection();
            UserSettings.Default[GetDefaultSortColumnSettingName( plugin )] = null;
            UserSettings.Default[GetDefaultSortAscendingSettingName( plugin )] = null;
            UserSettings.Default.Save();
            LoadColumns();
        }


        #region static

        public static List<FileColumn> GetColumnSettings( ILogViewer plugin )
        {
            var columns = new List<FileColumn>();

            var viewColumns = UserSettings.Default[GetFileViewColumnsSettingName( plugin )];
            if( viewColumns != null )
            {
                foreach( var column in (StringCollection)viewColumns )
                    columns.Add( new FileColumn( column ) );
            }

            if( columns.Count <= 0 )
            {
                var locations = FileLocationsViewModel.GetLogFileLocations( plugin );
                foreach( var columnInfo in plugin.GetDefaultColumns( locations.Count > 1 ) )
                    columns.Add( new FileColumn( columnInfo.Header, columnInfo.BindingPath, columnInfo.IsVisible, columnInfo.ShowToolTip ) );
            }

            return columns;
        }

        public static string GetDefaultSortColumnBinding( ILogViewer plugin )
        {
            return UserSettings.Default[GetDefaultSortColumnSettingName( plugin )] as string ?? string.Empty;
        }

        public static bool GetDefaultSortAscending( ILogViewer plugin )
        {
            var ascending = UserSettings.Default[GetDefaultSortAscendingSettingName( plugin )];
            return ascending != null && (bool)ascending;
        }

        public static string GetFileViewColumnsSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-FileViewColumns";
        }

        public static string GetDefaultSortColumnSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-DefaultSortColumn";
        }

        public static string GetDefaultSortAscendingSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-DefaultSortAscending";
        }

        #endregion
    }
}