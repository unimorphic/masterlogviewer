﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common.Utility;
using MasterLogViewer.Properties;
using Plugins.Interface;

namespace MasterLogViewer.ViewModels
{
    public class LogLocation : BindableBase
    {
        #region ContentInfo

        public class ContentInfo
        {
            public string Title { get; set; }

            public string FullPath { get; set; }

            public List<ContentInfo> Children { get; set; }

            public ContentInfo( string title, string fullPath, List<ContentInfo> children )
            {
                Title = title;
                Children = children;
                FullPath = fullPath;
            }
        }

        #endregion


        private readonly ILogViewer plugin;
        private string path;
        private bool isReadOnly;

        public string Path
        {
            get { return path; }
            set
            {
                if( path.ToLower() != value.ToLower() && !IsReadOnly )
                {
                    path = value;
                    OnPropertyChanged( "Path", "IsValidDirectory", "IsReadOnlyAndNotValidDirectory" );

                    if( !IsReadOnly )
                    {
                        UserSettings.Default[GetBrowseFolderPathSettingName( plugin )] = Path;
                        UserSettings.Default.Save();
                    }
                }
            }
        }

        public bool IsReadOnly
        {
            get { return isReadOnly; }
            set
            {
                if( isReadOnly != value )
                {
                    isReadOnly = value;
                    OnPropertyChanged( "IsReadOnly", "IsReadOnlyAndNotValidDirectory" );
                }
            }
        }

        public bool IsValidDirectory { get { return Directory.Exists( Path ); } }

        public bool IsReadOnlyAndNotValidDirectory { get { return IsReadOnly && !IsValidDirectory; } }

        public bool HasContents { get { return plugin.GetLocationContents( Path ).Count > 0; } }

        public List<ContentInfo> Contents
        {
            get
            {
                var tree = new List<ContentInfo>();
                foreach( var content in plugin.GetLocationContents( Path ) )
                    AddElementToTree( tree, plugin.GetLocationContentHierarchy( content ), content );
                return tree;
            }
        }

        public List<string> TopContents { get { return FileLocationsViewModel.GetLogFileLocations( plugin ); } }

        public List<ContentInfo> BottomContents
        {
            get
            {
                var tree = new List<ContentInfo>();
                var locations = FileLocationsViewModel.GetLogFileLocations( plugin );
                foreach( var content in plugin.GetLocationContents( Path ).Where( c => !locations.Contains( c ) ) )
                    AddElementToTree( tree, plugin.GetLocationContentHierarchy( content ), content );
                return tree;
            }
        }

        public RelayCommand NavigateUpCmd { get { return Commands.AddIfNeeded( NavigateUp ); } }

        public RelayCommand DeleteCmd { get { return Commands.AddIfNeeded( o => Delete( this ) ); } }

        public event Action<LogLocation> Delete = delegate { };


        public LogLocation( ILogViewer plugin, string path, bool readOnly )
        {
            this.plugin = plugin;
            this.path = path;
            isReadOnly = readOnly;
        }

        private void NavigateUp( object arg )
        {
            var parentDir = FileSystemUtility.GetParentDirectory( Path );
            if( !string.IsNullOrEmpty( parentDir ) )
                Path = parentDir;
        }

        public static string GetBrowseFolderPathSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-BrowseFolderPath";
        }


        void AddElementToTree(List<ContentInfo> currentList, List<string> item, string fullPath)
        {
            foreach( var info in currentList )
            {
                if( item[0] == "All-User-Install-Agent" )
                {
                    int i = 0;
                    i++;
                }

                if( info.Title.ToLower() == item[0].ToLower() )
                {
                    if( item.Count > 1 )
                    {
                        item.RemoveAt( 0 );
                        AddElementToTree( info.Children, item, fullPath );
                    }
                    break;
                }
            }

            if( item.Count > 0 )
            {
                currentList.Add( new ContentInfo( item[0], fullPath, new List<ContentInfo>() ) );
                item.RemoveAt( 0 );
                AddElementToTree( currentList[currentList.Count - 1].Children, item, fullPath );
            }
        }
    }
}
