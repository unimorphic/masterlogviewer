﻿using System;
using System.Collections.Generic;
using Common.Utility;
using MasterLogViewer.Utilities;
using Plugins.Interface;

namespace MasterLogViewer.ViewModels
{
    public class BrowseFileViewModel : BindableBase
    {
        private readonly LogFileLoader logFileLoader;
        private readonly ILogViewer plugin;
        private bool isLoading;
        private string filePath = string.Empty;
        private DateTime? filterDate;


        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                isLoading = value;
                OnPropertyChanged( "IsLoading" );
            }
        }

        public bool FilterByDate { get { return plugin.FilterBrowseByDate; } }

        public DateTime? FilterDate
        {
            get
            {
                return filterDate;
            }
            set
            {
                if( filterDate != value )
                {
                    filterDate = value;
                    LoadLogFileAsync( filePath );
                }
            }
        }

        public List<LogRow> Items { get; set; }


        public RelayCommand RefreshCmd { get { return Commands.AddIfNeeded( ob => LoadLogFileAsync( filePath ), o => !string.IsNullOrEmpty( filePath ) ); } }


        public BrowseFileViewModel( ILogViewer plugin )
        {
            this.plugin = plugin;
            filterDate = plugin.FilterBrowseByDate ? DateTime.Today : (DateTime?)null;
            logFileLoader = new LogFileLoader( plugin );
            Items = new List<LogRow>();
            logFileLoader.FileLoaded += LoaderOnFileLoaded;
        }

        public void LoadLogFileAsync( string path )
        {
            if( !string.IsNullOrEmpty( path ) )
            {
                IsLoading = true;
                filePath = path;
                plugin.InitReadLogFields();
                logFileLoader.SearchLogFileAsync( path, null, FilterDate );
            }
        }


        private void LoaderOnFileLoaded( string path, List<LogRow> logRows )
        {
            if( logRows != null )
            {
                Items = logRows;
                OnPropertyChanged( "Items" );
            }
            plugin.FinishReadLogFields();
            IsLoading = false;
        }
    }
}