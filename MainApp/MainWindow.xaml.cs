﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Common.Plugins;
using Common.UserInterface;
using Common.Utility;
using FirstFloor.ModernUI.Windows.Controls;
using Plugins.Interface;
using MasterLogViewer.Properties;
using MasterLogViewer.ViewModels;
using MasterLogViewer.Controls;

namespace MasterLogViewer
{
    public partial class MainWindow : ModernWindow
    {
        public MainWindow()
        {
            InitApplication();
            PluginUtility.PopulatePlugins( ApplicationInfo.AppPath );
            PluginUtility.PopulateEmbbededPlugins();
            InitSettings();

            InitializeComponent();
            SetResourceReference( StyleProperty, "CondensedModernWindow" );
            
            AppearanceViewModel.ApplySettings();
            SetWindowSize();

            CommandBindings.Add( new CommandBinding( ApplicationCommands.Close, CloseCmdExecuted ) );
            UserSettings.Default.PropertyChanged += DefaultOnPropertyChanged;
        }


        private void OnLoaded( object sender, RoutedEventArgs e )
        {
            SetLayoutTransform();
        }

        private void OnStateChanged( object sender, EventArgs eventArgs )
        {
            UserSettings.Default.WindowMaximized = WindowState == WindowState.Maximized;
            UserSettings.Default.Save();
        }

        private void OnLocationChanged( object sender, EventArgs eventArgs )
        {
            UserSettings.Default.WindowRect = RestoreBounds;
            UserSettings.Default.Save();
        }

        private void OnSizeChanged( object sender, SizeChangedEventArgs e )
        {
            UserSettings.Default.WindowRect = RestoreBounds;
            UserSettings.Default.Save();
        }

        private void DefaultOnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if( e.PropertyName == "Zoom" )
                SetLayoutTransform();
        }

        private void CloseCmdExecuted( object sender, ExecutedRoutedEventArgs e )
        {
            ContentSource = null;

            var tab = tabs.SelectedItem as TabItem;
            if( tab != null )
            {
                var tabContent = tab.Content as ITabAdvContent;
                if( tabContent != null )
                    tabContent.OnSwitchToTab();
            }
        }


        private void SetLayoutTransform()
        {
            var zoom = UserSettings.Default.Zoom;

            var border = GetTemplateChild( "WindowBorder" ) as Border;
            if( border != null && zoom > 0 )
                border.LayoutTransform = new ScaleTransform( zoom, zoom );
        }

        private void SetWindowSize()
        {
            var windowRect = UserSettings.Default.WindowRect;
            if( windowRect.Width > 0 )
            {
                Top = windowRect.Top;
                Left = windowRect.Left;
                Width = windowRect.Width;
                Height = windowRect.Height;
                if( UserSettings.Default.WindowMaximized )
                    WindowState = WindowState.Maximized;
            }
        }

        private void InitSettings()
        {
            foreach( var plugin in PluginUtility.FilterPlugins<ILogViewer>() )
            {
                AddSettingIfNeeded( FileLocationsViewModel.GetLogFolderPathSettingName( plugin ), typeof( StringCollection ) );
                AddSettingIfNeeded( GridColorsViewModel.GetColorRulesSettingName( plugin ), typeof( StringCollection ) );
                AddSettingIfNeeded( GridColumnsViewModel.GetFileViewColumnsSettingName( plugin ), typeof( StringCollection ) );
                AddSettingIfNeeded( GridColumnsViewModel.GetDefaultSortColumnSettingName( plugin ), typeof( string ) );
                AddSettingIfNeeded( GridColumnsViewModel.GetDefaultSortAscendingSettingName( plugin ), typeof( bool ) );
                AddSettingIfNeeded( BrowseViewModel.GetSelectedIndexSettingName( plugin ), typeof( int ) );
                AddSettingIfNeeded( LogFileViewer.GetBottomRowHeightSettingName( plugin ), typeof( double ) );
                AddSettingIfNeeded( LocationSelector.GetSelectedLogFolderPathsSettingName( plugin ), typeof( StringCollection ) );
                AddSettingIfNeeded( LogLocation.GetBrowseFolderPathSettingName( plugin ), typeof( string ) );
                AddSettingIfNeeded( FiltersViewModel.GetFiltersSettingName( plugin ), typeof( StringCollection ) );
                AddSettingIfNeeded( FilterSelector.GetSelectedFiltersSettingName( plugin ), typeof( StringCollection ) );
            }

            UserSettings.Default.Reload();
        }


        private static void InitApplication()
        {
            GC.KeepAlive( typeof( Common.UserInterface.DirectoryBox ) );    //Preload Common.UserInterface dll
            Application.Current.DispatcherUnhandledException += OnDispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

            AddResouceDictionary( "/FirstFloor.ModernUI;component/Themes/Generic.xaml" );
            AddResouceDictionary( "/FirstFloor.ModernUI;component/Assets/ModernUI.xaml" );
            AddResouceDictionary( "/FirstFloor.ModernUI;component/Assets/ModernUI.Light.xaml" );
            AddResouceDictionary( "/Common.UserInterface;component/Themes/Generic.xaml" );
            AddResouceDictionary( "/MasterLogViewer;component/Resources/CustomStyles.xaml" );

;            ApplicationInfo.Init();
        }

        private static void CurrentDomainOnUnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            Error.DisplayAndLogError( Error.ErrorType.Fatal, "Unexpected error occurred", e.ExceptionObject as Exception );
        }

        private static void OnDispatcherUnhandledException( object sender, DispatcherUnhandledExceptionEventArgs e )
        {
            Error.DisplayAndLogError( Error.ErrorType.Fatal, "Unexpected error occurred", e.Exception );
        }

        private static void AddSettingIfNeeded( string settingName, Type settingType )
        {
            SettingsUtility.AddSettingIfNeeded( UserSettings.Default, settingName, settingType );
        }

        private static void AddResouceDictionary( string resourceDictionName )
        {
            Application.Current.Resources.MergedDictionaries.Add( new ResourceDictionary { Source = new Uri( resourceDictionName, UriKind.Relative ) } );
        }
    }
}
