﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Plugins.Interface;
using MasterLogViewer.Properties;
using MasterLogViewer.Utilities;
using MasterLogViewer.ViewModels;
using Common.UserInterface;

namespace MasterLogViewer.Controls
{
    public partial class LogFileViewer : UserControl
    {
        #region ContainsConverter

        private class ContainsConverter : IValueConverter
        {
            private readonly string containsValue;

            public ContainsConverter( string value )
            {
                containsValue = value.ToLower().Trim();
            }

            public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
            {
                return value != null && value.ToString().ToLower().Contains( containsValue );
            }

            public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region EqualsConverter

        private class EqualsConverter : IValueConverter
        {
            private readonly string equalsValue;

            public EqualsConverter( string value )
            {
                equalsValue = value.ToLower().Trim();
            }

            public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
            {
                return value != null && value.ToString().ToLower().Trim() == equalsValue;
            }

            public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region TrimConverter

        private class TrimConverter : IValueConverter
        {
            private readonly int maxLength;

            public TrimConverter( int maxLength )
            {
                this.maxLength = maxLength;
            }

            public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
            {
                return value != null && value.ToString().Length > maxLength ? value.ToString().Substring( 0, maxLength ) + "..." : value;
            }

            public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
            {
                throw new NotImplementedException();
            }
        }

        #endregion


        private const int MaxGridFieldLength = 200;
        private ScrollViewer scrollViewer;
        private ILogViewer plugin;
        private SearchTerms searchTerms;
        private GridViewColumnHeader currentSortHeader;


        public static readonly DependencyProperty FilterProperty =
               DependencyProperty.Register( "Filter", typeof( string ), typeof( LogFileViewer ), new PropertyMetadata( string.Empty, FilterPropertyChangedCallback ) );

        public string Filter
        {
            get { return (string)GetValue( FilterProperty ); }
            set { SetValue( FilterProperty, value ); }
        }

        public ILogViewer Plugin
        {
            get { return plugin; }
            set
            {
                plugin = value;

                var rowHeight = UserSettings.Default[GetBottomRowHeightSettingName( plugin )];
                bottomRow.Height = new GridLength( rowHeight == null ? 50 : (double)rowHeight );

                SetColumns();
                SetFormatting();
            }
        }


        public event Action VerticalScrollChanged = delegate {};

        public event Action SelectedCellsChanged = delegate {};


        public LogFileViewer()
        {
            InitializeComponent();
        }


        private void OnLoaded( object sender, RoutedEventArgs routedEventArgs )
        {
            FilterPropertyChangedCallback( this, new DependencyPropertyChangedEventArgs() );
        }


        public void ScrollGridToEnd()
        {
            Dispatcher.BeginInvoke( DispatcherPriority.Normal, new Action( () =>
            {
                if( scrollViewer != null )
                {
                    scrollViewer.ScrollChanged -= ScrollViewerOnScrollChanged;

                    scrollViewer.ScrollToVerticalOffset( scrollViewer.ScrollableHeight );
                    scrollViewer.UpdateLayout();

                    scrollViewer.ScrollChanged += ScrollViewerOnScrollChanged;
                }
            } ) );
        }

	    public void UserSettingsOnPropertyChanged( object sender, PropertyChangedEventArgs e )
	    {
		    if( e.PropertyName == GridColumnsViewModel.GetFileViewColumnsSettingName( plugin ) )
			    SetColumns();
			else if( e.PropertyName == "GridFontSize" || e.PropertyName == GridColorsViewModel.GetColorRulesSettingName( plugin ) )
			    SetFormatting();
	    }


        private void ScrollViewerOnScrollChanged( object sender, ScrollChangedEventArgs scrollChangedEventArgs )
        {
            if( Math.Abs( scrollChangedEventArgs.VerticalChange ) > 0.01 )
                VerticalScrollChanged();
        }

        private void GridSplitter_DragCompleted( object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e )
        {
            UserSettings.Default[GetBottomRowHeightSettingName( plugin )] = bottomRow.Height.Value;
            UserSettings.Default.Save();
        }

        private void CollectionViewSource_Filter( object sender, FilterEventArgs e )
        {
            var row = e.Item as LogRow;
            if( row != null && searchTerms != null )
                e.Accepted = row.Contains( searchTerms );
        }

        private void Grid_OnLoaded( object sender, RoutedEventArgs e )
        {
            if( VisualTreeHelper.GetChildrenCount( grid ) > 0 )
            {
                var border = VisualTreeHelper.GetChild( grid, 0 ) as Decorator;
                if( border != null && scrollViewer == null )
                {
                    scrollViewer = border.Child as ScrollViewer;
                    if( scrollViewer != null )
                    {
                        scrollViewer.ScrollChanged -= ScrollViewerOnScrollChanged;
                        scrollViewer.ScrollChanged += ScrollViewerOnScrollChanged;
                    }
                }
            }
        }

        private void GridOnSelectionChanged( object sender, SelectionChangedEventArgs selectionChangedEventArgs )
        {
            SelectedCellsChanged();
        }

        private void Grid_RowDoubleClick( object sender, MouseButtonEventArgs e )
        {
            new LogRowViewerDialog( (LogRow)grid.SelectedItem, Plugin ).ShowDialog();
        }


        private void GridViewColumnHeader_Click( object sender, RoutedEventArgs e )
        {
            var header = e.OriginalSource as GridViewColumnHeader;
            if( header != null )
            {
                if( currentSortHeader != null && currentSortHeader != header )
                    GridViewColumnProperties.SetSortDirection( currentSortHeader, null );
                currentSortHeader = header;

                var direction = GridViewColumnProperties.GetSortDirection( header );
                direction = !direction.HasValue || direction.Value != ListSortDirection.Ascending ? ListSortDirection.Ascending : ListSortDirection.Descending;
                GridViewColumnProperties.SetSortDirection( header, direction );

                var lcv = (ListCollectionView)CollectionViewSource.GetDefaultView( grid.ItemsSource );
                lcv.CustomSort = new LogRowSort( direction.Value, GetColumnBindingPath( header.Column ), plugin );
            }
        }

        private static void FilterPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e )
        {
            var source = dependencyObject as LogFileViewer;
            if( source != null && source.grid.ItemsSource != null )
            {
                source.searchTerms = !string.IsNullOrEmpty( source.Filter ) ? new SearchTerms( source.Filter, source.plugin ) : null;
                ( (ListCollectionView)source.grid.ItemsSource ).Refresh();
            }
        }


        #region Context Menu Events

        private void GridContextMenuOnOpened( object sender, RoutedEventArgs e )
        {
            var contextMenu = (ContextMenu)grid.FindResource( "DataRowContextMenu" );
            var filterContextMenu = contextMenu.Items.Cast<MenuItem>().First( i => i.Name == "filterContextMenu" );
            filterContextMenu.DataContext = ( (FrameworkElement)grid.InputHitTest( grid.PointFromScreen( contextMenu.PointToScreen( new Point() ) ) ) ).DataContext;
        }

        private void CopyCellOnClick( object sender, RoutedEventArgs e )
        {
            var contextMenu = (ContextMenu)grid.FindResource( "DataRowContextMenu" );
            var element = grid.InputHitTest( grid.PointFromScreen( contextMenu.PointToScreen( new Point() ) ) );
            var textBlock = VisualTreeUtility.FindVisualChild<TextBlock>( (DependencyObject)element );
            if( textBlock == null )
                textBlock = VisualTreeUtility.FindVisualParent<TextBlock>( (DependencyObject)element );
            if( textBlock != null )
                Clipboard.SetText( textBlock.Text );
        }

        private void CopySelectedRowsOnClick( object sender, RoutedEventArgs e )
        {
            var copyText = new StringBuilder();
            var view = (GridView)grid.View;
            
            foreach( LogRow item in grid.SelectedItems )
            {
                foreach( var column in view.Columns )
                    copyText.Append( item.Fields[GetColumnBindingPath( column )] + "\t" );

                copyText.Append( Environment.NewLine );
            }

            if( copyText.Length > 0 )
                Clipboard.SetText( copyText.ToString().Trim() );
        }

        private void FilterOnClick( object sender, RoutedEventArgs e )
        {
            var menu = (MenuItem)sender;
            Filter += " " + string.Format( menu.HeaderStringFormat, menu.Header );
        }

        #endregion


        private void SetColumns()
        {
            var view = (GridView)grid.View;
            var filterContextMenu = ( (ContextMenu)grid.FindResource( "DataRowContextMenu" ) ).Items.Cast<MenuItem>().First( i => i.Name == "filterContextMenu" );
            
            view.Columns.Clear();
            filterContextMenu.Items.Clear();

            var columns = GridColumnsViewModel.GetColumnSettings( Plugin );
            foreach( var column in columns )
            {
                if( column.IsVisible )
                {
                    var gridColumn = new GridViewColumn { Header = column.Header };
                    if( column.ShowToolTip )
                    {
                        var factory = new FrameworkElementFactory( typeof( TextBlock ) );
                        factory.SetBinding( ToolTipProperty, new Binding( "Fields[" + column.BindingPath + "]" ) );
                        factory.SetBinding( TextBlock.TextProperty, new Binding( "Fields[" + column.BindingPath + "]" ) { Converter = new TrimConverter( MaxGridFieldLength ) } );
                        factory.SetValue( ToolTipService.BetweenShowDelayProperty, 0 );
                        factory.SetValue( ToolTipService.InitialShowDelayProperty, 0 );
                        gridColumn.CellTemplate = new DataTemplate( typeof( TextBlock ) ) { VisualTree = factory };
                        GridViewColumnProperties.SetBindingPath( gridColumn, column.BindingPath );
                    }
                    else
                        gridColumn.DisplayMemberBinding = new Binding( "Fields[" + column.BindingPath + "]" ) { Mode = BindingMode.OneWay, Converter = new TrimConverter( MaxGridFieldLength ) };
                    
                    view.Columns.Add( gridColumn );
                    
                    var menu = new MenuItem {Header = column.Header, HeaderStringFormat = column.BindingPath + ":\"{0}\""};
                    menu.SetBinding( HeaderedItemsControl.HeaderProperty, new Binding( "Fields[" + column.BindingPath + "]" ) );

                    var style = new Style( typeof(MenuItem), (Style)Application.Current.Resources[typeof(MenuItem)] );
                    var trigger = new DataTrigger {Binding = new Binding( "Fields[" + column.BindingPath + "]" ), Value = string.Empty};
                    trigger.Setters.Add( new Setter( VisibilityProperty, Visibility.Collapsed ) );
                    style.Triggers.Add( trigger );
                    menu.Style = style;

                    menu.Click += FilterOnClick;
                    filterContextMenu.Items.Add( menu );
                }
            }

            var binding = new Binding( "SelectedItem.Fields[" + plugin.DetailsFieldName + "]" ) { ElementName = "grid" };
            messageDisplay.SetBinding( TextBox.TextProperty, binding );
        }

        private void SetFormatting()
        {
            var rowStyle = new Style( typeof( ListViewItem ), (Style)Application.Current.Resources[typeof( ListViewItem )] );
            var columns = GridColumnsViewModel.GetColumnSettings( Plugin );

            var colorRules = GridColorsViewModel.GetColorRuleSettings( Plugin );
            foreach( var rule in colorRules )
            {
                var triggers = new List<MultiDataTrigger>();
                var trigger = new MultiDataTrigger();
                triggers.Add( trigger );

                foreach( var filter in rule.Filters )
                {
                    var column = columns.FirstOrDefault( c => c.Header == filter.Column );
                    if( column != null )
                    {
                        var binding = new Binding( "Fields[" + column.BindingPath + "]" );
                        switch( filter.Operation )
                        {
                            case ColorFilterOperations.Equals:
                                binding.Converter = new EqualsConverter( filter.Value );
                                break;
                            case ColorFilterOperations.Contains:
                                binding.Converter = new ContainsConverter( filter.Value );
                                break;
                        }
                        trigger.Conditions.Add( new Condition( binding, true ) );
                        
                        if( filter.Command == ColorFilterCommands.Or )
                        {
                            trigger = new MultiDataTrigger();
                            triggers.Add( trigger );
                        }
                    }
                }

                foreach( var dataTrigger in triggers )
                {
                    if( dataTrigger.Conditions.Count > 0 )
                    {
                        dataTrigger.Setters.Add( new Setter( BackgroundProperty, new SolidColorBrush( rule.BackColor ) ) );
                        dataTrigger.Setters.Add( new Setter( ForegroundProperty, new SolidColorBrush( rule.ForColor ) ) );
                        rowStyle.Triggers.Add( dataTrigger );
                    }
                }
            }

            var mouseTrigger = new Trigger { Property = IsMouseOverProperty, Value = true };
            mouseTrigger.Setters.Add( new Setter( BackgroundProperty, new DynamicResourceExtension( "DataGridCellBackgroundHover" ) ) );
            mouseTrigger.Setters.Add( new Setter( ForegroundProperty, new DynamicResourceExtension( "DataGridCellForegroundHover" ) ) );
            rowStyle.Triggers.Add( mouseTrigger );

            var selectedTrigger = new Trigger { Property = ListBoxItem.IsSelectedProperty, Value = true };
            selectedTrigger.Setters.Add( new Setter( BackgroundProperty, new DynamicResourceExtension( "DataGridCellBackgroundSelected" ) ) );
            selectedTrigger.Setters.Add( new Setter( ForegroundProperty, new DynamicResourceExtension( "DataGridCellForegroundSelected" ) ) );
            rowStyle.Triggers.Add( selectedTrigger );

            rowStyle.Setters.Add( new Setter( ContextMenuProperty, new DynamicResourceExtension( "DataRowContextMenu" ) ) );
            rowStyle.Setters.Add( new EventSetter( RequestBringIntoViewEvent, new RequestBringIntoViewEventHandler( ( sender, e ) => e.Handled = true ) ) );
            rowStyle.Setters.Add( new EventSetter( MouseDoubleClickEvent, new MouseButtonEventHandler( Grid_RowDoubleClick ) ) );
            rowStyle.Setters.Add( new Setter( FontSizeProperty, UserSettings.Default.GridFontSize ) );
            rowHeight.FontSize = UserSettings.Default.GridFontSize;

            rowStyle.Setters.Add( new Setter( HeightProperty, new Binding { ElementName = "rowHeight", Path = new PropertyPath("ActualHeight") } ) );
            rowStyle.Setters.Add( new Setter( VerticalContentAlignmentProperty, VerticalAlignment.Top ) );

            grid.ItemContainerStyle = rowStyle;
        }

        public static string GetBottomRowHeightSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-BrowseFileBottomRowHeight";
        }

        public static string GetColumnBindingPath( GridViewColumn column )
        {
            string path;
            if( column.DisplayMemberBinding != null )
                path = ( (Binding)column.DisplayMemberBinding ).Path.Path.Replace( "Fields[", string.Empty ).TrimEnd( ']' );
            else
                path = GridViewColumnProperties.GetBindingPath( column );
            return path;
        }
    }
}
