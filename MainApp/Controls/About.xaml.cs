﻿using System;
using System.Windows;
using System.Windows.Controls;
using Common.Utility;

namespace MasterLogViewer.Controls
{
    public partial class About : UserControl
    {
        public About()
        {
            InitializeComponent();

            productName.Text = ApplicationInfo.AssemblyInfo.Product;
            version.Text = string.Format( "Version {0}", ApplicationInfo.AssemblyInfo.Version );
            copyrightYear.Text = DateTime.Today.Year.ToString();
            companyName.Text = ApplicationInfo.AssemblyInfo.Company;
            companyLink.NavigateUri = new Uri( "http://" + ApplicationInfo.AssemblyInfo.Company + ".com" );
        }

        private void AppDataFolderOnClick( object sender, RoutedEventArgs e )
        {
            try
            {
                System.Diagnostics.Process.Start( ApplicationInfo.AppDataFolder );
            }
            catch( Exception ex )
            {
                Error.DisplayAndLogError( Error.ErrorType.ShowAndContinue, "Failed to open app data folder", ex );
            }
        }
    }
}
