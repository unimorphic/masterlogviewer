﻿using System.Windows.Controls;
using Common.Plugins;
using FirstFloor.ModernUI.Windows;
using Plugins.Interface;
using MasterLogViewer.Utilities;
using MasterLogViewer.ViewModels;

namespace MasterLogViewer.Controls
{
    public partial class FilterSettings : UserControl, IContent
    {
        public FilterSettings()
        {
            InitializeComponent();
        }


        #region IContent Members

        public void OnNavigatedTo( FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e )
        {
            var pluginName = NavigationUtility.GetParamater( e, "plugin" );
            if( !string.IsNullOrEmpty( pluginName ) )
                DataContext = new FiltersViewModel( PluginUtility.GetPlugin<ILogViewer>( pluginName ) );
        }

        public void OnFragmentNavigation( FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e ) { }

        public void OnNavigatedFrom( FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e ) { }

        public void OnNavigatingFrom( FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e ) { }

        #endregion
    }
}
