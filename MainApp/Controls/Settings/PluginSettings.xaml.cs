﻿using System.Collections.Generic;
using System.Windows.Controls;
using Common.Plugins;
using FirstFloor.ModernUI.Windows;
using Plugins.Interface;
using MasterLogViewer.Utilities;

namespace MasterLogViewer.Controls
{
    public partial class PluginSettings : UserControl, IContent
    {
        public PluginSettings()
        {
            InitializeComponent();
        }


        #region IContent Members

        public void OnNavigatedTo( FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e )
        {
            var pluginName = NavigationUtility.GetParamater( e , "plugin" );
            if( !string.IsNullOrEmpty( pluginName ) )
            {
                var plugin = PluginUtility.GetPlugin<ILogViewer>( pluginName );
				list.ItemsSource = new List<ButtonLinkPath>
                {
                    new ButtonLinkPath( "Log Locations", "/Controls/Settings/FileLocationsSettings.xaml?plugin=" + plugin.FullTypeName, "M0,23.667L64,23.667 64,52.667 0,52.667z M0,0L25.802,0 25.802,6.862999 64,6.862999 64,14.818 0,14.818 0,13.727 0,6.862999z" ),
                    new ButtonLinkPath( "Filters", "/Controls/Settings/FilterSettings.xaml?plugin=" + plugin.FullTypeName, "M2.1299944,9.9798575L55.945994,9.9798575 35.197562,34.081179 35.197562,62.672859 23.428433,55.942383 23.428433,33.52121z M1.3001332,0L56.635813,0C57.355887,0,57.935946,0.5891428,57.935946,1.3080959L57.935946,2.8258877C57.935946,3.5448422,57.355887,4.133985,56.635813,4.133985L1.3001332,4.133985C0.58005941,4.133985,-2.3841858E-07,3.5448422,0,2.8258877L0,1.3080959C-2.3841858E-07,0.5891428,0.58005941,0,1.3001332,0z" ),
					new ButtonLinkPath( "Grid Color Rules", "/Controls/Settings/GridColorsSettings.xaml?plugin=" + plugin.FullTypeName, "F1M65.7861,33.6147C65.7861,33.6147 65.8955,24.9741 52.178,27.8386 52.178,27.8386 44.2256,27.6199 50.7194,17.9038 50.7194,17.9038 61.3564,2.02344 43.8297,1.39453 43.8297,1.39453 16.7217,7.99097 7.09991,31.387 7.09991,31.387 0,48.4297 11.1617,57.9194 11.1617,57.9194 25.1338,69.1133 52.5762,54.0613 52.5762,54.0613 68.3167,43.5315 65.7861,33.6147z M20.8633,42.2878C20.4886,43.9141 18.8701,44.926 17.2435,44.5547 15.6182,44.1824 14.6012,42.5625 14.9762,40.939 15.348,39.3125 16.9622,38.2969 18.5928,38.668 20.2161,39.0405 21.2347,40.6641 20.8633,42.2878z M27.9014,28.7476C27.3961,30.9624 25.1852,32.3516 22.9674,31.8425 20.7535,31.3335 19.3675,29.1265 19.8727,26.905 20.3824,24.6953 22.5895,23.3047 24.8069,23.8098 27.0228,24.323 28.4111,26.53 27.9014,28.7476z M42.5439,18.5156C41.9371,21.176 39.2832,22.8386 36.6253,22.2292 33.96,21.625 32.3008,18.9727 32.9078,16.3086 33.5162,13.6499 36.1689,11.9871 38.8284,12.5952 41.4886,13.2034 43.1523,15.853 42.5439,18.5156z" ),
                    new ButtonLinkPath( "Grid Columns", "/Controls/Settings/GridColumnsSettings.xaml?plugin=" + plugin.FullTypeName, "M17.4635,54.136003L17.470001,63.982808 11.712201,64.001003 11.711001,61.442711 14.908845,61.427015 14.903645,54.137299z M0.05080986,52.440001L2.6092148,52.451701 2.5701561,61.408857 5.7900004,61.442758 5.7784109,64.000002 0,63.955701z M14.82,37.120001L17.379001,37.120001 17.379001,47.357002 14.82,47.357002z M0,36.274002L2.5600004,36.274002 2.5600004,46.510002 0,46.510002z M14.82,16.648003L17.379001,16.648003 17.379001,26.884001 14.82,26.884001z M0,15.802002L2.5600004,15.802002 2.5600004,26.038002 0,26.038002z M11.711,0.0010032654L17.379002,0.014064789 17.379002,11.529003 14.820455,11.529003 14.820455,2.5727425 11.711,2.5610199z M0.044281006,0L5.7850003,0 5.7732601,2.558773 2.5910339,2.5704842 2.5598536,9.8610001 0,9.8493729z" )
                };
            }
        }

        public void OnFragmentNavigation( FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e ) { }

        public void OnNavigatedFrom( FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e ) { }

        public void OnNavigatingFrom( FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e ) { }

        #endregion
    }
}
