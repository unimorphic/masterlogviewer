﻿using System.Windows.Controls;
using MasterLogViewer.ViewModels;

namespace MasterLogViewer.Controls
{
    public partial class AppearanceSettings : UserControl
    {
        public AppearanceSettings()
        {
            InitializeComponent();

            DataContext = new AppearanceViewModel();
        }
    }
}
