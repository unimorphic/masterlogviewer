﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using Common.Plugins;
using FirstFloor.ModernUI.Windows;
using Plugins.Interface;
using MasterLogViewer.Utilities;
using MasterLogViewer.ViewModels;

namespace MasterLogViewer.Controls
{
    public partial class GridColumnsSettings : UserControl, IContent
    {
        public GridColumnsSettings()
        {
            InitializeComponent();

            sortAscending.ItemsSource = Enum.GetValues( typeof( ListSortDirection ) ).Cast<ListSortDirection>();
        }


        #region IContent Members

        public void OnNavigatedTo( FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e )
        {
            var pluginName = NavigationUtility.GetParamater( e, "plugin" );
            if( !string.IsNullOrEmpty( pluginName ) )
            {
                var viewModel = new GridColumnsViewModel( PluginUtility.GetPlugin<ILogViewer>( pluginName ) );
                DataContext = viewModel;
                viewModel.LoadColumns();
            }
        }

        public void OnFragmentNavigation( FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e ) { }

        public void OnNavigatedFrom( FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e ) { }

        public void OnNavigatingFrom( FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e ) { }

        #endregion
    }
}
