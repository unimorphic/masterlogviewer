﻿using System;
using System.Globalization;
using System.Windows.Data;
using FirstFloor.ModernUI.Windows.Controls;
using Plugins.Interface;
using MasterLogViewer.Utilities;
using MasterLogViewer.ViewModels;

namespace MasterLogViewer.Controls
{
    public partial class LogRowViewerDialog : ModernDialog
    {
        public LogRow Row { get; set; }


        public LogRowViewerDialog( LogRow row, ILogViewer plugin )
        {
            InitializeComponent();

            Row = row;
            list.ItemsSource = GridColumnsViewModel.GetColumnSettings( plugin );
        }
    }

    class RowValueConverter : IMultiValueConverter
    {
        public object Convert( object[] values, Type targetType, object parameter, CultureInfo culture )
        {
            var value = ( (LogRow)values[1] ).Fields[values[0].ToString()];
            return value != null ? value.ToString() : string.Empty;
        }

        public object[] ConvertBack( object value, Type[] targetTypes, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }
}
