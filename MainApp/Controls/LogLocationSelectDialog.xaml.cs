﻿using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using FirstFloor.ModernUI.Windows.Controls;
using MasterLogViewer.ViewModels;

namespace MasterLogViewer.Controls
{
    public partial class LogLocationSelectDialog : ModernDialog
    {
        private LogLocation location;


        public LogLocationSelectDialog( LogLocation location )
        {
            InitializeComponent();

            Buttons = new BindingList<Button> { OkButton, CancelButton };
            DataContext = this.location = location;
        }


        private void Dialog_OnClosing( object sender, CancelEventArgs e )
        {
            if( DialogResult.HasValue && DialogResult.Value )
            {
                if( contents.SelectedItem != null && ( (LogLocation.ContentInfo)contents.SelectedItem ).Children.Count <= 0 )
                    location.Path = ((LogLocation.ContentInfo)contents.SelectedItem).FullPath;
            }
        }


        private void Contents_ItemDoubleClicked( object sender, MouseButtonEventArgs e )
        {
            if( contents.SelectedItem != null && ( (LogLocation.ContentInfo)contents.SelectedItem ).Children.Count <= 0 )
            {
                DialogResult = true;
                Close();
            }
        }
    }
}
