﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Plugins.Interface;
using MasterLogViewer.Properties;
using MasterLogViewer.ViewModels;
using System.Text;

namespace MasterLogViewer.Controls
{
    public partial class FilterSelector : UserControl
    {
        private List<Filter> filters = new List<Filter>();
        private ILogViewer plugin;


        public static readonly DependencyProperty FilterProperty =
            DependencyProperty.Register( "Filter", typeof( string ), typeof( FilterSelector ), new PropertyMetadata( string.Empty, FilterPropertyChangedCallback ) );

        public string Filter
        {
            get { return (string)GetValue( FilterProperty ); }
            set { SetValue( FilterProperty, value ); }
        }

        public static readonly DependencyProperty LiveFilterProperty =
            DependencyProperty.Register( "LiveFilter", typeof( string ), typeof( FilterSelector ), new PropertyMetadata( string.Empty, FilterPropertyChangedCallback ) );

        public string LiveFilter
        {
            get { return (string)GetValue( LiveFilterProperty ); }
            set { SetValue( LiveFilterProperty, value ); }
        }


        public static readonly DependencyProperty UpdateOnTextChangedProperty =
               DependencyProperty.Register( "UpdateOnTextChanged", typeof( bool ), typeof( FilterSelector ), new PropertyMetadata( true ) );

        public bool UpdateOnTextChanged
        {
            get { return (bool)GetValue( UpdateOnTextChangedProperty ); }
            set { SetValue( UpdateOnTextChangedProperty, value ); }
        }


        public static readonly DependencyProperty IsCustomFilterEnteredProperty =
               DependencyProperty.Register( "IsCustomFilterEntered", typeof( bool ), typeof( FilterSelector ), new PropertyMetadata( false ) );

        public bool IsCustomFilterEntered
        {
            get { return (bool)GetValue( IsCustomFilterEnteredProperty ); }
            set { SetValue( IsCustomFilterEnteredProperty, value ); }
        }


        public FilterSelector()
        {
            InitializeComponent();
        }


        public void Setup( ILogViewer plugin )
        {
            this.plugin = plugin;
            GetFilters();
        }

        public void UserSettingsOnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if( e.PropertyName == FiltersViewModel.GetFiltersSettingName( plugin ) )
                GetFilters();
        }


        private void ComboBox_DropDownClosed( object sender, EventArgs e )
        {
            SetFilterProp( true );
        }

        private void SearchBox_OnSearch( string obj )
        {
            SetFilterProp( false );
        }

        private void SearchBox_OnTextChanged( object sender, TextChangedEventArgs e )
        {
            LiveFilter = GetFilterValue();
            IsCustomFilterEntered = searchBox.Text.Length > 0;
        }

        private static void FilterPropertyChangedCallback( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            var selector = source as FilterSelector;
            if( selector != null )
            {
                var value = e.NewValue.ToString().Trim();

                var filter = selector.GetFilterFromSelectedFilters();
                if( filter.Length > 0 )
                    value = value.Replace( filter, string.Empty ).Trim();

                if( selector.searchBox.Text.Trim() != value )
                    selector.searchBox.Text = value;
            }
        }


        private void GetFilters()
        {
            filters = FiltersViewModel.GetFilters( plugin );

            var selectedFilters = UserSettings.Default[GetSelectedFiltersSettingName( plugin )] as StringCollection;

            if( selectedFilters != null )
            {
                foreach( var selected in selectedFilters )
                {
                    var filter = filters.FirstOrDefault( f => f.Value == selected );
                    if( filter != null )
                        filter.IsSelected = true;
                }
            }

            combo.ItemsSource = filters;
            SetFilterProp( false );

            Visibility = filters.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        private void SetFilterProp( bool save )
        {
            var selectedFilters = filters.Where( f => f.IsSelected ).ToList();

            Filter = GetFilterValue();

            if( save )
            {
                var savedFilters = new StringCollection();
                foreach( var f in selectedFilters )
                    savedFilters.Add( f.Value );
                UserSettings.Default[GetSelectedFiltersSettingName( plugin )] = savedFilters;
                UserSettings.Default.Save();
            }

            combo.Text = selectedFilters.Count + " filter" + ( selectedFilters.Count != 1 ? "s" : string.Empty );
        }

        private string GetFilterValue()
        {
            var filter = new StringBuilder();
            filter.Append( GetFilterFromSelectedFilters() );
            filter.Append( " " );
            filter.Append( searchBox.Text );
            return ParseFilter( filter.ToString() );
        }

        private string GetFilterFromSelectedFilters()
        {
            var selectedFilters = filters.Where( f => f.IsSelected ).ToList();

            var filter = new StringBuilder();
            foreach( var f in selectedFilters )
            {
                filter.Append( "(" );
                filter.Append( f.Value );
                filter.Append( ") " );
            }
            return ParseFilter( filter.ToString() );
        }

        private string ParseFilter( string filter )
        {
            return filter.Trim().Replace( "[Today]", DateTime.Today.ToShortDateString() );
        }

        public static string GetSelectedFiltersSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-SelectedFilters";
        }
    }
}
