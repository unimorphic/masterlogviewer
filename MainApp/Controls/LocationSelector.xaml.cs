﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Plugins.Interface;
using MasterLogViewer.Properties;
using MasterLogViewer.ViewModels;

namespace MasterLogViewer.Controls
{
    public partial class LocationSelector : UserControl
    {
        class LogFileLocation
        {
            public string Path { get; set; }
            public bool IsSelected { get; set; }
        }

        private ILogViewer plugin;
        private bool fixedLocation;


        public static readonly DependencyProperty LocationsProperty =
            DependencyProperty.Register( "Locations", typeof( List<string> ), typeof( LocationSelector ), new PropertyMetadata( new List<string>() ) );

        public List<string> Locations
        {
            get { return (List<string>)GetValue( LocationsProperty ); }
            set { SetValue( LocationsProperty, value ); }
        }


        public LocationSelector()
        {
            InitializeComponent();
        }


        public void Setup( ILogViewer plugin, string location )
        {
            this.plugin = plugin;

            if( !string.IsNullOrEmpty( location ) )
            {
                fixedLocation = true;
                Locations = new List<string> {location};
                Visibility = Visibility.Collapsed;
            }
            else
                GetLocations();
        }

        public void UserSettingsOnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if( e.PropertyName == FileLocationsViewModel.GetLogFolderPathSettingName( plugin ) )
                GetLocations();
        }


        private void ComboBox_DropDownClosed( object sender, EventArgs e )
        {
            SetLocationsProp( true );
        }


        private void GetLocations()
        {
            if( !fixedLocation )
            {
                var locations = FileLocationsViewModel.GetLogFileLocations( plugin );
                var selectedLocations = GetSelectedLocations( plugin );

                combo.ItemsSource = locations.Select( p => new LogFileLocation { Path = p, IsSelected = selectedLocations.Contains( p ) } ).ToList();
                SetLocationsProp( false );

                Visibility = locations.Count <= 1 ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        private void SetLocationsProp( bool save )
        {
            Locations = ( (List<LogFileLocation>)combo.ItemsSource ).Where( l => l.IsSelected ).Select( l => l.Path ).ToList();

            if( save )
            {
                var selectedLocations = new StringCollection();
                foreach( var location in Locations )
                    selectedLocations.Add( location );
                UserSettings.Default[GetSelectedLogFolderPathsSettingName( plugin )] = selectedLocations;
                UserSettings.Default.Save();
            }

            combo.Text = Locations.Count + " location" + ( Locations.Count != 1 ? "s" : string.Empty );
        }

        public static string GetSelectedLogFolderPathsSettingName( ILogViewer plugin )
        {
            return plugin.FullTypeName + "-SelectedLogFolderPaths";
        }

        public static StringCollection GetSelectedLocations( ILogViewer plugin )
        {
            var locations = FileLocationsViewModel.GetLogFileLocations( plugin );

            var selectedLocations = UserSettings.Default[GetSelectedLogFolderPathsSettingName( plugin )] as StringCollection;
            if( selectedLocations == null || locations.Count <= 1 )
            {
                selectedLocations = new StringCollection();
                foreach( var location in locations )
                    selectedLocations.Add( location );
            }
            return selectedLocations;
        } 
    }
}
