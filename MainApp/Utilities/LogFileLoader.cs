﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Common.Utility;
using MasterLogViewer.ViewModels;
using Plugins.Interface;

namespace MasterLogViewer.Utilities
{
    internal class LogFileLoader
    {
        private readonly BackgroundWorker worker = new BackgroundWorker();
        private readonly ILogViewer plugin;
        private string filePath = string.Empty;
        private string searchText = string.Empty;
        private DateTime? searchDate;
        private bool cancelRequested;


        public bool IsRunning { get { return worker.IsBusy; } }

        public bool CancelRequested
        {
            get { return cancelRequested; }
            set
            {
                cancelRequested = value;
                plugin.CancelReadLogFieldsRequested = value;
            }
        }

        public event Action<string, List<LogRow>> FileLoaded = delegate {};


        public LogFileLoader( ILogViewer plugin )
        {
            this.plugin = plugin;
            worker.DoWork += WorkerOnDoWork;
            worker.RunWorkerCompleted += WorkerOnRunWorkerCompleted;
        }

        public void SearchLogFileAsync( string path, string search, DateTime? date )
        {
            searchText = search != null ? search.ToLower().Trim() : null;
            searchDate = date;
            filePath = path;
            worker.RunWorkerAsync();
        }


        private void WorkerOnDoWork( object sender, DoWorkEventArgs e )
        {
            try
            {
                if( !string.IsNullOrEmpty( filePath ) )
                {
                    var serverName = FileSystemUtility.GetServerName( filePath );
                    var searchTerm = !string.IsNullOrEmpty( searchText ) ? new SearchTerms( searchText, plugin ) : null;
                    var simpleSearchTerm = searchTerm != null ? searchTerm.GetSimpleSearchTerm() : string.Empty;

                    if( CancelRequested )
                        return;
                    
                    var stringRows = plugin.ReadLogFields( filePath, serverName, simpleSearchTerm, searchDate );
                    var rows = stringRows.Select( row => new LogRow( row ) );
                    if( searchTerm != null )
                        rows = rows.Where( row => row.Contains( searchTerm ) );
                    var rowsList = rows.ToList();

                    if( CancelRequested )
                        return;

                    var sortColumn = GridColumnsViewModel.GetDefaultSortColumnBinding( plugin );
                    if( !string.IsNullOrEmpty( sortColumn ) )
                    {
                        var ascending = GridColumnsViewModel.GetDefaultSortAscending( plugin );
                        rowsList.Sort( new LogRowSort( ascending ? ListSortDirection.Ascending : ListSortDirection.Descending, sortColumn, plugin ) );
                    }

                    FileLoaded( filePath, rowsList );
                }
            }
            catch( Exception ex )
            {
                Error.DisplayAndLogError( Error.ErrorType.ShowAndContinue, "Error loading log file", ex );
            }
        }

        private void WorkerOnRunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            FileLoaded( filePath, null );
            searchText = string.Empty;
        }
    }
}
