﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Plugins.Interface;

namespace MasterLogViewer.Utilities
{
    class LogRowSort : IComparer<LogRow>, IComparer
    {
        private readonly ListSortDirection direction;
        private readonly string column;
        private readonly ILogViewer plugin;

        public LogRowSort( ListSortDirection direction, string column, ILogViewer plugin )
        {
            this.direction = direction;
            this.column = column;
            this.plugin = plugin;
        }

        public int Compare( LogRow one, LogRow two )
        {
            return CompareRows( one, two );
        }

        int IComparer.Compare( object one, object two )
        {
            return CompareRows( one, two );
        }

        int CompareRows( object one, object two )
        {
            var fieldName = plugin.GetSortFieldName( column.Replace( " ", string.Empty ) );
            var field1 = (IComparable)( (LogRow)one ).Fields[fieldName];
            var field2 = (IComparable)( (LogRow)two ).Fields[fieldName];
            return direction == ListSortDirection.Ascending ? field1.CompareTo( field2 ) : field2.CompareTo( field1 );
        }
    }
}