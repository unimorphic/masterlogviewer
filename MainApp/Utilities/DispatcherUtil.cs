﻿using System;
using System.Collections.Generic;
using System.Windows.Threading;
using Common.Utility;

namespace MasterLogViewer.Utilities
{
    public static class DispatcherUtil
    {
        public static void Chunk<T>( List<T> items, Dispatcher dispatcher, Action<T> action, Func<bool> cancel )
        {
            if( items != null && items.Count > 0 )
            {
                foreach( var chunk in items.Chunk( 100 ) )
                {
                    if( cancel() )
                        break;

                    var group = chunk;
                    dispatcher.Invoke( DispatcherPriority.Background, new Action( delegate
                    {
                        if( !cancel() )
                        {
                            foreach( var item in group )
                                action( item );
                        }
                    } ) );
                }
            }
        }
    }
}