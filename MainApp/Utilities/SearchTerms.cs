﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugins.Interface;

namespace MasterLogViewer.Utilities
{
    public class SearchTerms : List<SearchTerms>
    {
        public bool Or = false;

        public string Term = string.Empty;

        public string FieldName = string.Empty;


        private SearchTerms() {}

        public SearchTerms( string search, ILogViewer plugin )
        {
            Or = true;

            var sections = search.Split( new[] { "(", ")" }, StringSplitOptions.RemoveEmptyEntries ).ToList();

            RemoveUnneededAndCleanSections( sections, false );

            foreach( var section in sections )
            {
                if( section.StartsWith( "OR" ) )
                    Add( GetSearchTermSectionsContents( section, plugin ) );
                else
                {
                    if( Count <= 0 )
                        Add( new SearchTerms() );

                    this[Count - 1].Add( GetSearchTermSectionsContents( section, plugin ) );
                }

                if( section.EndsWith( "OR" ) && section.Length > 2 )
                    Add( new SearchTerms() );
            }
        }

        public string GetSimpleSearchTerm()
        {
            var simpleSearch = string.Empty;
            if( string.IsNullOrEmpty( FieldName ) )
                simpleSearch = Count == 1 ? this[0].GetSimpleSearchTerm() : ( Count == 0 ? Term : string.Empty );
            return simpleSearch;
        }

        
        private static SearchTerms GetSearchTermSectionsContents( string search, ILogViewer plugin )
        {
            var sections = search.Split( new[] { " " }, StringSplitOptions.RemoveEmptyEntries ).ToList();
            var quoteStartIndex = -1;
            for( int i = 0; i < sections.Count; i++ )
            {
                var searchSection = sections[i];

                if( quoteStartIndex >= 0 )
                {
                    sections[quoteStartIndex] += " " + searchSection;
                    sections[i] = string.Empty;
                }

                if( searchSection.Count( c => c == '\"' ) == 1 )
                    quoteStartIndex = quoteStartIndex < 0 ? i : -1;
            }

            RemoveUnneededAndCleanSections( sections, true );

            var containOrs = new SearchTerms { Or = true };
            foreach( var section in sections )
            {
                var searchSection = section;
                if( plugin.FieldNames.Any( searchSection.Contains ) )
                {
                    foreach( var fieldName in plugin.FieldNames )
                    {
                        if( searchSection.StartsWith( fieldName + ":", true, null ) || searchSection.StartsWith( fieldName + "=", true, null ) )
                        {
                            var fieldSearchTerm = searchSection.Substring( fieldName.Length ).Trim( new[] { ':', '=', '"' } ).ToLower();
                            if( !string.IsNullOrEmpty( fieldSearchTerm ) )
                            {
                                if( containOrs.Count <= 0 )
                                    containOrs.Add( new SearchTerms() );

                                containOrs[containOrs.Count - 1].Add( new SearchTerms { FieldName = fieldName, Term = fieldSearchTerm } );
                            }
                            searchSection = string.Empty;
                        }
                    }
                }

                if( !string.IsNullOrEmpty( searchSection ) )
                {
                    if( containOrs.Count <= 0 )
                        containOrs.Add( new SearchTerms() );

                    if( searchSection == "OR" )
                        containOrs.Add( new SearchTerms() );
                    else
                        containOrs[containOrs.Count - 1].Add( new SearchTerms { Term = searchSection.ToLower() } );
                }
            }

            return containOrs;
        }

        private static void RemoveUnneededAndCleanSections( IList<string> sections, bool removeQuotes )
        {
            var removeChars = new List<char> { ' ', '(', ')' };
            if( removeQuotes )
                removeChars.Add( '\"' );

            for( var i = sections.Count - 1; i >= 0; i-- )
            {
                var section = sections[i].Trim( removeChars.ToArray() );
                if( section.Length <= 0 || section == "AND" || ( ( i == sections.Count - 1 || i == 0 ) && section == "OR" ) )
                    sections.RemoveAt( i );
                else
                    sections[i] = section;
            }
        }
    }
}