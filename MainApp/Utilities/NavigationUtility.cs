﻿using FirstFloor.ModernUI.Windows.Navigation;

namespace MasterLogViewer.Utilities
{
    static class NavigationUtility
    {
        public static string GetParamater( NavigationEventArgs e, string paramName )
        {
            var desiredValue = string.Empty;
            foreach( var item in e.Source.ToString().Split( '&', '?' ) )
            {
                var parts = item.Split( '=' );
                if( parts[0] == paramName )
                {
                    desiredValue = parts[1];
                    break;
                }
            }

            return desiredValue;
        }
    }
}
