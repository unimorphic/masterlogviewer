﻿using System.Collections.Generic;
using Common.Utility;
using Plugins.Interface;

namespace MasterLogViewer.Controls
{
    class ButtonLinkGroup
    {
        public string Title { get; set; }
        public string Image { get; set; }
        public List<ButtonLink> Links { get; set; }

        public ButtonLinkGroup( string title, string image )
        {
            Title = title;
            Image = image;
            Links = new List<ButtonLink>();
        }
    }

    class ButtonLink : BindableBase
    {
        private string miniLinkImage = string.Empty;

        public string Title { get; set; }
        public string Image { get; set; }
        public ILogViewer Plugin { get; set; }

        public string MiniLinkImage
        {
            get { return miniLinkImage; }
            set
            {
                if( miniLinkImage != value )
                {
                    miniLinkImage = value;
                    OnPropertyChanged( "MiniLinkImage" );
                }
            }
        }

        public ButtonLink( string title, ILogViewer plugin, string image )
        {
            Title = title;
            Image = image;
            Plugin = plugin;
        }
    }

	class ButtonLinkPath
	{
		public string Title { get; set; }
		public string Image { get; set; }
        public string PageUrl { get; set; }

		public ButtonLinkPath( string title, string pageUrl, string image )
		{
			Title = title;
			Image = image;
			PageUrl = pageUrl;
		}
	}
}
