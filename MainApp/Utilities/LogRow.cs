﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterLogViewer.Utilities
{
    public class LogRow
    {
        public Dictionary<string, object> Fields { get; private set; }


        public LogRow( Dictionary<string, object> fields )
        {
            Fields = fields;
        }


        public bool Contains( SearchTerms search )
        {
            return TermContains( search, GetAllFieldsLowerCase() );
        }


        private bool TermContains( SearchTerms terms, string allFieldsLowerCase )
        {
            bool contains;

            if( terms.Count > 0 )
            {
                if( terms.Or )
                    contains = terms.Any( t => TermContains( t, allFieldsLowerCase ) );
                else
                    contains = terms.Aggregate( true, ( current, t ) => current & TermContains( t, allFieldsLowerCase ) );
            }
            else
            {
                if( !string.IsNullOrEmpty( terms.FieldName ) )
                {
                    var fieldValue = ( Fields[terms.FieldName] ?? string.Empty ).ToString().ToLower();
                    contains = fieldValue.Contains( terms.Term );
                }
                else
                    contains = allFieldsLowerCase.Contains( terms.Term.ToLower() );
            }

            return contains;
        }

        private string GetAllFieldsLowerCase()
        {
            var fields = new StringBuilder();
            foreach( var field in Fields )
            {
                if( field.Value != null )
                    fields.Append( field.Value.ToString().ToLower() );
            }
            return fields.ToString();
        }
    }
}