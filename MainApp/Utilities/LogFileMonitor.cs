﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using Common.Utility;
using Plugins.Interface;

namespace MasterLogViewer.Utilities
{
    public class LogFileMonitor
    {
        private readonly TimeSpan monitorTimeout = new TimeSpan( 0, 5, 0 );
        private readonly BackgroundWorker worker = new BackgroundWorker { WorkerSupportsCancellation = true, WorkerReportsProgress = true };
        private readonly ILogViewer plugin;
        private readonly string filePath = string.Empty;
        private bool returnInitialFileContents;
        private long lastReadLineNumber;


        public bool IsRunning { get { return worker.IsBusy; } }

        public event Action<List<Dictionary<string, object>>> FileChanged = delegate { };

        public event Action<string> MonitoringFinished = delegate { };


        public LogFileMonitor( string path, ILogViewer plugin )
        {
            this.plugin = plugin;
            filePath = path;

            worker.DoWork += WorkerOnDoWork;
            worker.RunWorkerCompleted += WorkerOnRunWorkerCompleted;
            worker.ProgressChanged += WorkerOnProgressChanged;
        }


        public void StartMonitoring( bool returnInitialContents )
        {
            returnInitialFileContents = returnInitialContents;
            worker.RunWorkerAsync();
        }

        public void StopMonitoring()
        {
            worker.CancelAsync();
        }


        private void WorkerOnDoWork( object sender, DoWorkEventArgs e )
        {
            try
            {
                if( string.IsNullOrEmpty( filePath ) || !File.Exists( filePath ) )
                    return;

                using( var stream = new FileStream( filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite ) )
                {
                    using( var reader = new StreamReader( stream ) )
                    {
                        if( !returnInitialFileContents )
                        {
                            var lastLineNum = lastReadLineNumber > 0 ? lastReadLineNumber : long.MaxValue;
                            lastReadLineNumber = 0;

                            while( !reader.EndOfStream && lastReadLineNumber < lastLineNum )
                            {
                                var line = reader.ReadLine();
                                if( !string.IsNullOrEmpty( line ) )
                                    lastReadLineNumber++;
                            }

                            worker.ReportProgress( 0 );
                        }

                        var serverName = FileSystemUtility.GetServerName( filePath );
                        var timeoutTime = DateTime.Now + monitorTimeout;

                        while( !worker.CancellationPending && DateTime.Now < timeoutTime )
                        {
                            Thread.Sleep( 1000 );

                            var lines = new List<string>();
                            while( !reader.EndOfStream )
                            {
                                var line = reader.ReadLine();
                                if( !string.IsNullOrEmpty( line ) )
                                    lines.Add( line );
                            }

                            if( lines.Count > 0 )
                            {
                                timeoutTime = DateTime.Now + monitorTimeout;
                                lastReadLineNumber += lines.Count;
                                worker.ReportProgress( 0, plugin.ReadLogFields( lines, serverName, null, null ) );
                            }
                        }
                    }
                }
            }
            catch( Exception ex )
            {
                Error.DisplayAndLogError( Error.ErrorType.ShowAndContinue, "Error monitoring log file", ex );
            }
        }

        private void WorkerOnProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            FileChanged( e.UserState as List<Dictionary<string, object>> );
        }

        private void WorkerOnRunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            MonitoringFinished( filePath );
        }
    }
}