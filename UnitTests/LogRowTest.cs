﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MasterLogViewer.Utilities;
using Plugins.SharePoint;

namespace UnitTests
{
    [TestClass]
    public class LogRowTest
    {
        private readonly SPLogViewer plugin = new SPLogViewer();

        private readonly LogRow row = new LogRow( new Dictionary<string, object>
                                                  {
                                                      {"TimeStamp", "05/04/2013 16:00:49.57"},
                                                      {"Process", "wsstracing.exe (0x0BB0)"},
                                                      {"TID", "0x0BC0"},
                                                      {"Area", "SharePoint Foundation "},
                                                      {"Category", "Tracing Controller Service"},
                                                      {"EventID", "5152"},
                                                      {"Level", "Information"},
                                                      {"Message", "Tracing Service started."},
                                                      {"CorrelationID", "1ddb55d6-cd22-447e-8bf1-02eb79a8faf3"},
                                                      {"Server", "Local"}
                                                  } );

        [TestMethod]
        public void Contains_Basic()
        {
            Assert.IsTrue( row.Contains( new SearchTerms( "control", plugin ) ) );
        }

        [TestMethod]
        public void Contains_Multiple()
        {
            Assert.IsTrue( row.Contains( new SearchTerms( "tracing started", plugin ) ) );
        }

        [TestMethod]
        public void Contains_QuotesMultiple()
        {
            Assert.IsFalse( row.Contains( new SearchTerms( "\"tracing started\"", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "\"service started\"", plugin ) ) );
        }

        [TestMethod]
        public void Contains_FieldValue()
        {
            Assert.IsTrue( row.Contains( new SearchTerms( "TimeStamp:00:49", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "TimeStamp:5152", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "Process:ing.exe", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "Process:8bf1", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "TID:0x0BC0", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "TID:16:00", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "Area:ounda", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "Area:Information", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "Category:Controller", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "Category:started", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "EventID:515", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "EventID:0x0BC0", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "Level:infor", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "Level:undat", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "Message:started", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "Message:control", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "CorrelationID:2eb79a8faf", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "CorrelationID:0x0BB0", plugin ) ) );

            Assert.IsTrue( row.Contains( new SearchTerms( "Server:Local", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "Server:Tracing", plugin ) ) );
        }

        [TestMethod]
        public void Contains_FieldValueMultiple()
        {
            Assert.IsTrue( row.Contains( new SearchTerms( "TID:0x0BC0 Server:Local", plugin ) ) );
        }

        [TestMethod]
        public void Contains_FieldValueMultipleOr()
        {
            Assert.IsFalse( row.Contains( new SearchTerms( "TID:asdf", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "TID:asdf Category:Controller", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "TID:asdf OR Category:Controller", plugin ) ) );
        }

        [TestMethod]
        public void Contains_FieldQuotes()
        {
            Assert.IsTrue( row.Contains( new SearchTerms( "Category:Tracing Category:Service", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "Category:\"Tracing Service\"", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "Category:\"Controller Service\"", plugin ) ) );
        }

        [TestMethod]
        public void Contains_MultipleOr()
        {
            Assert.IsFalse( row.Contains( new SearchTerms( "asdf", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "asdf control", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "asdf OR control", plugin ) ) );
        }

        [TestMethod]
        public void Contains_MultipleAnd()
        {
            Assert.IsTrue( row.Contains( new SearchTerms( "tracing control", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "tracing AND control", plugin ) ) );
        }

        [TestMethod]
        public void Contains_Brackets()
        {
            Assert.IsTrue( row.Contains( new SearchTerms( "(Category:Controller OR TID:asdf) control", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "(Category:asdf OR TID:asdf) control", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "(Category:asdf OR TID:asdf) OR control", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "control (Category:asdf OR TID:asdf)", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "control OR (Category:asdf OR TID:asdf)", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "control OR (Category:asdf OR TID:asdf) asdf", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "control (Category:asdf OR TID:asdf) asdf", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "control OR (Category:asdf OR TID:asdf) OR asdf", plugin ) ) );
            Assert.IsFalse( row.Contains( new SearchTerms( "(Category:asdf OR TID:asdf) OR", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "(Category:Controller OR TID:asdf) OR", plugin ) ) );
            Assert.IsTrue( row.Contains( new SearchTerms( "(Category:Controller OR TID:asdf) (control)", plugin ) ) );
        }
    }
}
