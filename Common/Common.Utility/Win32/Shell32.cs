﻿using System;
using System.Runtime.InteropServices;


namespace Common.Utility
{
    /// <summary>
    /// Contains information about a file object
    /// http://msdn.microsoft.com/en-us/library/bb759792%28VS.85%29.aspx
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SHFILEINFO
    {
        /// <summary>
        /// A handle to the icon that represents the file. You are responsible for destroying this handle with
        /// DestroyIcon when you no longer need it. 
        /// </summary>
        public IntPtr hIcon;

        /// <summary>
        /// The index of the icon image within the system image list.
        /// </summary>
        public IntPtr iIcon;

        /// <summary>
        /// An array of values that indicates the attributes of the file object.
        /// </summary>
        public uint dwAttributes;

        /// <summary>
        /// A string that contains the name of the file as it appears in the Microsoft Windows Shell,
        /// or the path and file name of the file that contains the icon representing the file.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string szDisplayName;

        /// <summary>
        /// A string that describes the type of file.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
        public string szTypeName;
    }


    /// <summary>
    /// Static collection of constants and functions from Shell32.dll
    /// </summary>
    static public class Shell32
    {
        /// <summary>Retrieve the handle to the icon that represents the file and the index of the icon within the system image list.
        /// The handle is copied to the hIcon member of the structure specified by psfi, and the index is copied to the iIcon member.</summary>
        public const uint SHGFI_ICON = 0x100;

        /// <summary>Modify SHGFI_ICON, causing the function to retrieve the file's large icon. The SHGFI_ICON flag must also be set.</summary>
        public const uint SHGFI_LARGEICON = 0x0;

        /// <summary>Retrieve the index of a system image list icon. If successful, the index is copied to the iIcon member of psfi.
        /// The return value is a handle to the system image list. Only those images whose indices are successfully copied to
        /// iIcon are valid. Attempting to access other images in the system image list will result in undefined behavior.</summary>
        public const uint SHGFI_SYSICONINDEX = 16384;

        /// <summary>
        /// Retrieves information about an object in the file system, such as a file, folder, directory, or drive root.
        /// http://msdn.microsoft.com/en-us/library/bb762179%28VS.85%29.aspx
        /// </summary>
        /// <param name="path">A pointer to a null-terminated string of maximum length MAX_PATH that contains the path and file name. Both absolute and relative paths are valid</param>
        /// <param name="fileAttributes">A combination of one or more file attribute flags</param>
        /// <param name="info">The address of a SHFILEINFO structure to receive the file information</param>
        /// <param name="sizeFileInfo">The size, in bytes, of the SHFILEINFO structure pointed to by the psfi parameter</param>
        /// <param name="flages">The flags that specify the file information to retrieve</param>
        /// <returns>Returns a value whose meaning depends on the uFlags parameter</returns>
        [DllImport( "SHELL32.DLL", EntryPoint = "SHGetFileInfo", SetLastError = true )]
        public static extern int SHGetFileInfo( string path, uint fileAttributes, ref SHFILEINFO info, uint sizeFileInfo, uint flages  );
    }
}
