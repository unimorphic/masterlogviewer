﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace Common.Utility
{
    static public class Wevtapi
    {
        [DllImport( "wevtapi.dll", CharSet = CharSet.Auto, ExactSpelling = false, SetLastError = true )]
        public static extern bool EvtFormatMessage( EventLogHandle publisherMetadataHandle, EventLogHandle eventHandle, uint messageId, int valueCount, EvtStringVariant[] values, EvtFormatMessageFlags flags, int bufferSize, [Out] StringBuilder buffer, out int bufferUsed );

        [DllImport( "wevtapi.dll", CharSet = CharSet.Auto, EntryPoint = "EvtFormatMessage", ExactSpelling = false, SetLastError = true )]
        public static extern bool EvtFormatMessageBuffer( EventLogHandle publisherMetadataHandle, EventLogHandle eventHandle, uint messageId, int valueCount, IntPtr values, EvtFormatMessageFlags flags, int bufferSize, IntPtr buffer, out int bufferUsed );

        [DllImport( "wevtapi.dll", CharSet = CharSet.Auto, ExactSpelling = false, SetLastError = true )]
        public static extern EventLogHandle EvtOpenPublisherMetadata( EventLogHandle session, string publisherId, string logFilePath, int locale, int flags );

        [DllImport( "wevtapi.dll", CharSet = CharSet.None, ExactSpelling = false )]
        [ReliabilityContract( Consistency.WillNotCorruptState, Cer.Success )]
        public static extern bool EvtClose( IntPtr handle );
    }

    public enum EvtFormatMessageFlags
    {
        EvtFormatMessageEvent = 1,
        EvtFormatMessageLevel = 2,
        EvtFormatMessageTask = 3,
        EvtFormatMessageOpcode = 4,
        EvtFormatMessageKeyword = 5,
        EvtFormatMessageChannel = 6,
        EvtFormatMessageProvider = 7,
        EvtFormatMessageId = 8,
        EvtFormatMessageXml = 9
    }

    [StructLayout( LayoutKind.Explicit )]
    public struct EvtStringVariant
    {
        [FieldOffset( 0 )]
        public string StringVal;

        [FieldOffset( 8 )]
        public uint Count;

        [FieldOffset( 12 )]
        public uint Type;
    }

    [SecurityCritical( SecurityCriticalScope.Everything )]
    [SecurityTreatAsSafe]
    public sealed class EventLogHandle : SafeHandle
    {
        public override bool IsInvalid
        {
            get
            {
                if( base.IsClosed )
                {
                    return true;
                }
                return this.handle == IntPtr.Zero;
            }
        }

        public static EventLogHandle Zero
        {
            get
            {
                return new EventLogHandle();
            }
        }

        private EventLogHandle()
            : base( IntPtr.Zero, true )
        {
        }

        public EventLogHandle( IntPtr handle, bool ownsHandle )
            : base( IntPtr.Zero, ownsHandle )
        {
            base.SetHandle( handle );
        }

        protected override bool ReleaseHandle()
        {
            Wevtapi.EvtClose( this.handle );
            this.handle = IntPtr.Zero;
            return true;
        }
    }
}
