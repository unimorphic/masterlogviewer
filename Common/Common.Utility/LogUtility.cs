using System;
using System.IO;


namespace Common.Utility
{
	static public class LogUtility
	{
        private const int maxLogFileLines = 2000;

		private static string file;

        private static bool logError;


		static string LogFile
		{
			get
			{

                if( string.IsNullOrEmpty( file ) )
                {
                    file = ApplicationInfo.AppDataFolder + "Log.txt";

                    if( !File.Exists( file ) )
                        File.Create( file ).Close();

                    //Clean up the log if it's gotten too long by removing the oldest lines
                    var lines = File.ReadAllLines( LogFile );
                    if( lines.Length > maxLogFileLines )
                    {
                        var newLines = new string[maxLogFileLines];
                        ArrayUtility.Copy( lines, lines.Length - maxLogFileLines, newLines, 0 );
                        File.WriteAllLines( LogFile, newLines );
                    }
                }

			    return file;
			}
		}

		static public void Append( string text, bool ignoreErrors )
		{
			try
			{
                if( !string.IsNullOrEmpty( text ) )
                    File.AppendAllText( LogFile, string.Format( "[{0}] ", DateTime.Now ) + text + Environment.NewLine + Environment.NewLine );
			}
			catch
			{
                if( !ignoreErrors && !logError )
                {
                    System.Windows.MessageBox.Show( "Error creating log file" );
                    logError = true;
                }
			}
		}
	}
}
