﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace Common.Utility
{
    public class RelayCommand : ICommand
    {
        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;


        public Action<object> Execution { get { return _execute; } }

        public event EventHandler CanExecuteChanged = delegate {};


        public RelayCommand( Action<object> execute )
            : this( execute, null )
        {
        }

        public RelayCommand( Action<object> execute, Predicate<object> canExecute )
        {
            if( execute == null )
                throw new ArgumentNullException( "execute" );

            _execute = execute;
            _canExecute = canExecute;
        }


        public void Execute( object parameter )
        {
            _execute( parameter );
        }

        [DebuggerStepThrough]
        public bool CanExecute( object parameter )
        {
            return _canExecute == null || _canExecute( parameter );
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged( this, EventArgs.Empty );
        }
    }
}
