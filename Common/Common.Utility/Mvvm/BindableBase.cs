﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Common.Utility
{
    public abstract class BindableBase : INotifyPropertyChanged
    {
        #region CommandsList

        public class CommandsList : ObservableCollection<RelayCommand>
        {
            public RelayCommand AddIfNeeded( Action<object> execute )
            {
                return AddIfNeeded( execute, null );
            }

            public RelayCommand AddIfNeeded( Action<object> execute, Predicate<object> canExecute )
            {
                var command = this.FirstOrDefault( d => d.Execution == execute );

                if( command == null )
                {
                    command = new RelayCommand( execute, canExecute );
                    Add( command );
                }

                return command;
            }

            public void RaiseCanExecuteChanged()
            {
                foreach( var command in Items )
                    command.RaiseCanExecuteChanged();
            }
        }

        #endregion

        private CommandsList commands;


        protected CommandsList Commands { get { return commands ?? ( commands = new CommandsList() ); } }

        public event PropertyChangedEventHandler PropertyChanged = delegate {};


        public void OnPropertyChanged( params string[] propertyNames )
        {
            var eventHandler = PropertyChanged;
            if( eventHandler != null )
            {
                if( propertyNames != null )
                {
                    foreach( var propertyName in propertyNames )
                        eventHandler( this, new PropertyChangedEventArgs( propertyName ) );
                }
                else
                    eventHandler( this, new PropertyChangedEventArgs( null ) );
            }
        }
    }
}
