﻿using System;
using System.Reflection;

namespace Common.Utility
{
    public static class ReflectionUtility
    {
        public static object GetPropertyValue( object obj, string propertyName )
        {
            if( obj == null )
                throw new ArgumentNullException( "obj" );
            Type objType = obj.GetType();
            PropertyInfo propInfo = GetPropertyInfo( objType, propertyName );
            if( propInfo == null )
                throw new ArgumentOutOfRangeException( "propertyName",
                                                       string.Format( "Couldn't find property {0} in type {1}", propertyName, objType.FullName ) );
            return propInfo.GetValue( obj, null );
        }

        public static void SetPropertyValue( object obj, string propertyName, object val )
        {
            if( obj == null )
                throw new ArgumentNullException( "obj" );
            Type objType = obj.GetType();
            PropertyInfo propInfo = GetPropertyInfo( objType, propertyName );
            if( propInfo == null )
                throw new ArgumentOutOfRangeException( "propertyName",
                                                       string.Format( "Couldn't find property {0} in type {1}", propertyName, objType.FullName ) );
            propInfo.SetValue( obj, val, null );
        }

        public static object GetMemeberValue( object obj, string propertyName )
        {
            if( obj == null )
                throw new ArgumentNullException( "obj" );
            var objType = obj.GetType();

            var memberInfo = GetMemberInfo( objType, propertyName );
            if( memberInfo == null )
                throw new ArgumentOutOfRangeException( "propertyName",
                                                       string.Format( "Couldn't find property {0} in type {1}", propertyName, objType.FullName ) );
            return ((FieldInfo)memberInfo).GetValue( obj );
        }

        public static void CallMethod( object obj, string functionName )
        {
            if( obj == null )
                throw new ArgumentNullException( "obj" );
            var objType = obj.GetType();

            objType.GetMethod( functionName ).Invoke( obj, null );
        }


        private static PropertyInfo GetPropertyInfo( Type type, string propertyName )
        {
            PropertyInfo propInfo;
            do
            {
                propInfo = type.GetProperty( propertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic );
                type = type.BaseType;
            } while( propInfo == null && type != null );
            return propInfo;
        }

        private static MemberInfo GetMemberInfo( Type type, string memeberName )
        {
            MemberInfo[] memberInfos;
            do
            {
                memberInfos = type.GetMember( memeberName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic );
                type = type.BaseType;
            } while( memberInfos == null && memberInfos.Length <= 0 && type != null );
            return memberInfos[0];
        }
    }
}
