using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Interop;

//Notes:
// 1. When deleting files on a usb stick or any remote folder they aren't sent to the trash
// 2. When daylight savings changes, modified file times for files on a usb stick are changed by an hour

namespace Common.Utility
{
	public enum FileDir
	{
		Directory,
		File,
	}

	static public class FileSystemUtility
	{
		/// <summary>
		/// Standardizes the specified file system path by removing any directory separators at the end of the path and
		/// by converting all the characters to lowercase
		/// </summary>
		/// <param name="path">File system path to standardize</param>
		/// <returns>Standardized file system path</returns>
		static public string StandarizePath( string path )
		{
			string stndPath = string.Empty;

			if( path != null )
			{
				stndPath = path.TrimEnd( new [] { ' ', Path.DirectorySeparatorChar } ).ToLower();
				if( IsFile( path ) == FileDir.Directory )
					stndPath += Path.DirectorySeparatorChar;
			}

			return stndPath;
		}

		static public bool PathsEqual( string path1, string path2 )
		{
			return Equals( StandarizePath( path1 ), StandarizePath( path2 ) );
		}


		public static List<string[]> ReadDelimitedFile( string path, int minNumberOfColumns, params string[] delimiters )
		{
			var rows = new List<string[]>();
            
			if( Exists( FileDir.File, path ) )
			{
                using( var stream = new FileStream( path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite ) )
                {
                    using( var reader = new StreamReader( stream ) )
                    {
                        while( !reader.EndOfStream )
                        {
                            var line = reader.ReadLine();
                            if( !string.IsNullOrEmpty( line ) )
                            {
                                var items = line.Split( delimiters, StringSplitOptions.RemoveEmptyEntries );
                                if( items.Length >= minNumberOfColumns )
                                    rows.Add( items );
                            }
                        }
                    }
                }
			}

		    return rows;
		}

        public static List<string> ReadFileRows( string path )
        {
            var rows = new List<string>();

            if( Exists( FileDir.File, path ) )
            {
                using( var stream = new FileStream( path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite ) )
                {
                    using( var reader = new StreamReader( stream ) )
                    {
                        while( !reader.EndOfStream )
                        {
                            var line = reader.ReadLine();
                            if( !string.IsNullOrEmpty( line ) )
                                rows.Add( line );
                        }
                    }
                }
            }

            return rows;
        }

		public static string GetNewestFile( string folder, string searchPattern )
		{
			var fileName = string.Empty;
			var files = new DirectoryInfo( folder ).GetFiles( searchPattern );
			if( files.Length > 0 )
				fileName = files.OrderByDescending( f => f.LastWriteTime ).First().FullName;
			return fileName;
		}

        public static List<string> GetFilesByDate( string folder, string searchPattern, DateTime date )
        {
            return new DirectoryInfo( folder ).GetFiles( searchPattern ).Where( f => f.LastWriteTime.Date == date.Date || f.CreationTime.Date == date.Date )
                                                .OrderByDescending( f => f.LastWriteTime ).Select( f => f.FullName ).ToList();
        }


        #region Info

        static public FileSystemInfo GetFileInfo( FileDir type, string path )
        {
            FileSystemInfo info = null;

            switch( type )
            {
                case FileDir.File:
                    info = new FileInfo( path );
                    break;
                case FileDir.Directory:
                    info = new DirectoryInfo( path );
                    break;
            }

            return info;
        }

        static public FileDir IsFile( string path )
        {
            return Path.HasExtension( path ) ? FileDir.File : FileDir.Directory;
        }

        static public bool Exists( FileDir type, string path )
        {
            if( path == null )
                return false;

            switch( type )
            {
                case FileDir.File:
                    return File.Exists( path );
                case FileDir.Directory:
                    return Directory.Exists( path );
            }

            return false;
        }

        static public string[] Get( FileDir type, string directory )
        {
            var items = new string[0];

            if( string.IsNullOrEmpty( directory ) )
            {
                if( type == FileDir.Directory )
                    items = Directory.GetLogicalDrives();
            }

            //If the specified directory doesn't exist
            else if( !Directory.Exists( directory ) )
            {
                throw ( new Exception( "Directory not found: " + directory ) );
            }
            else
            {
                try
                {
                    switch( type )
                    {
                        case FileDir.File:
                            items = Directory.GetFiles( directory );
                            break;
                        case FileDir.Directory:
                            items = Directory.GetDirectories( directory );
                            for( int i = 0; i < items.Length; i++ )
                                items[i] += Path.DirectorySeparatorChar;
                            break;
                    }
                }
                catch( Exception ) { }
            }

            return items;
        }

        static public string GetFileName( string path, bool extension )
        {
            string name;

            if( extension )
                name = Path.GetFileName( path.TrimEnd( Path.DirectorySeparatorChar ) );
            else
                name = Path.GetFileNameWithoutExtension( path );

            if( name == null || name.Length <= 0 )
                name = path;

            return name;
        }

        static public string GetParentDirectory( string path )
        {
            return StandarizePath( Path.GetDirectoryName( path.Trim( Path.DirectorySeparatorChar ) ) );
        }

        public static string GetFileExtension( string path, bool period )
        {
            string extension = Path.GetExtension( path );
            if( !period && !string.IsNullOrEmpty( extension ) && extension.Length > 1 )
                extension = extension.Substring( 1 );

            return extension;
        }

        static public int GetIconIndex( string path )
        {
            int index = -1;

            switch( path )
            {
                default:
                    var info = new SHFILEINFO();
                    Shell32.SHGetFileInfo( path, 0x80, ref info, (uint)Marshal.SizeOf( info ), Shell32.SHGFI_SYSICONINDEX );

                    if( info.iIcon != IntPtr.Zero )
                        index = info.iIcon.ToInt32();
                    break;
            }

            return index;
        }

        static public ImageSource GetIcon( string path )
        {
            ImageSource icon;

            switch( path )
            {
                default:
                    var info = new SHFILEINFO();
                    Shell32.SHGetFileInfo( path, 0x80, ref info, (uint)Marshal.SizeOf( info ), Shell32.SHGFI_ICON | Shell32.SHGFI_LARGEICON );

                    if( info.hIcon != IntPtr.Zero )
                        icon = Imaging.CreateBitmapSourceFromHIcon( info.hIcon, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions() );
                    else
                        icon = ImageUtility.GetIconResource( "Common.Utility", "document.ico" );
                    break;
            }

            return icon;
        }

        public static string GetServerName( string path )
        {
            var serverName = Path.GetPathRoot( path );
            if( serverName.IndexOf( "\\\\" ) < 0 )
                serverName = "Local";
            else
            {
                serverName = serverName.Replace( "\\\\", string.Empty );
                if( serverName.IndexOf( "\\" ) > 0 )
                    serverName = serverName.Substring( 0, serverName.IndexOf( "\\" ) );
            }

            return serverName;
        }

        #endregion
	}
}
