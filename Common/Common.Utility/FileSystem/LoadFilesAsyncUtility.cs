﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Threading;
using System.IO;

namespace Common.Utility
{
    #region FileObject Struct

    public class FileObject : INotifyPropertyChanged
    {
        readonly string name;
        readonly string path;
        readonly FileDir fileDir;
        readonly string extension;
        readonly FileSystemInfo info;
        ImageSource icon;


        public FileDir FileDir { get { return fileDir; } }

        public string Name { get { return name; } }

        public string Path { get { return path; } }

        public string Extension { get { return extension; } }

        public FileAttributes Attributes
        {
            get
            {
                FileAttributes attributes = info.Attributes;
                if( FileSystemUtility.GetParentDirectory( path ) == null )
                    attributes = FileAttributes.Normal;
                return attributes;
            }
        }

        public DateTime LastModified
        {
            get { return info.LastWriteTime; }
        }

        public DateTime CreatedOn
        {
            get { return info.CreationTime; }
        }

        public ImageSource Icon
        {
            get
            {
                if( icon == null )
                {
                    IconUtility.BeginLoadIconAsync( this );
                    return IconUtility.LoadingImage;
                }
                else
                    return icon;
            }
            set
            {
                icon = (ImageSource)value.GetAsFrozen();

                if( PropertyChanged != null )
                    PropertyChanged( this, new PropertyChangedEventArgs( "Icon" ) );
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;


        public FileObject( FileDir fileDir, string path )
        {
            this.fileDir = fileDir;
            this.path = path;
            name = FileSystemUtility.GetFileName( path, true );
            extension = FileSystemUtility.GetFileExtension( path, false ).ToLower();
            icon = null;
            info = FileSystemUtility.GetFileInfo( fileDir, path );
        }
    }

    #endregion


    #region IconUtility

    static class IconUtility
    {
        static readonly Dictionary<int, ImageSource> icons = new Dictionary<int, ImageSource>();
        static readonly BackgroundWorker worker = new BackgroundWorker();
        static readonly Stack<FileObject> iconFetch = new Stack<FileObject>();


        public static ImageSource LoadingImage
        {
            get { return icons[-1]; }
        }


        static IconUtility()
        {
            if( !icons.ContainsKey( -1 ) )
                icons.Add( -1, ImageUtility.GetIconResource( "Common.Utility", "document.ico" ) );

            worker.WorkerSupportsCancellation = true;
            worker.DoWork += Worker_DoWork;
        }


        public static void BeginLoadIconAsync( FileObject file )
        {
            iconFetch.Push( file );

            if( !worker.IsBusy )
                worker.RunWorkerAsync();
        }

        static public void LoadIcon( FileObject file )
        {
            lock( icons )
            {
                int index = FileSystemUtility.GetIconIndex( file.Path );

                if( icons.ContainsKey( index ) )
                    file.Icon = icons[index];
                else
                {
                    file.Icon = FileSystemUtility.GetIcon( file.Path );
                    icons.Add( index, file.Icon );
                }
            }
        }


        static void Worker_DoWork( object sender, DoWorkEventArgs e )
        {
            lock( icons )
            {
                while( iconFetch.Count > 0 )
                {
                    var file = iconFetch.Pop();

                    if( file != null )
                        LoadIcon( file );

                    //If there's no more icons to load, wait a bit to see if another icon is added before exiting the thread
                    if( iconFetch.Count <= 0 )
                        System.Threading.Thread.Sleep( 1 );
                }
            }
        }
    }

    #endregion


    public class LoadFilesAsyncUtility
    {
        #region UpdateInfo

        struct UpdateInfo
        {
            public readonly object arg;

            public readonly IList<FileObject> files;

            public UpdateInfo( object arg, IList<FileObject> files )
            {
                this.arg = arg;
                this.files = files;
            }
        }

        #endregion

        readonly BackgroundWorker loadWorker = new BackgroundWorker();
        readonly BackgroundWorker updateWorker = new BackgroundWorker();
        Dispatcher dispatcher;


        public bool IsWorking
        {
            get { return loadWorker.IsBusy; }
        }

        public event Action<IList<FileObject>> OnLoadFileStruct = null;


        public LoadFilesAsyncUtility()
        {
            loadWorker.DoWork += LoadWorker_DoWork;
            updateWorker.DoWork += UpdateWorker_DoWork;
        }


        #region Loading

        public void BeginLoadFileStructAsync( string folderPath )
        {
            if( !loadWorker.IsBusy )
            {
                dispatcher = Dispatcher.CurrentDispatcher;
                loadWorker.RunWorkerAsync( folderPath );
            }
        }

        public void BeginLoadFileStructAsync( string[] paths )
        {
            if( !loadWorker.IsBusy )
            {
                dispatcher = Dispatcher.CurrentDispatcher;
                loadWorker.RunWorkerAsync( paths );
            }
        }


        void LoadWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            IList<FileObject> items = GetInformation( e.Argument );

            if( items != null && OnLoadFileStruct != null )
                dispatcher.Invoke( OnLoadFileStruct, (object)items );
        }

        #endregion


        #region Updating

        public void BeginUpdateFileStructAsync( string folderPath, IList<FileObject> contents )
        {
            if( !updateWorker.IsBusy )
            {
                dispatcher = Dispatcher.CurrentDispatcher;
                updateWorker.RunWorkerAsync( new UpdateInfo( folderPath, contents ) );
            }
        }

        public void BeginUpdateFileStructAsync( string[] paths, IList<FileObject> files )
        {
            if( !updateWorker.IsBusy )
            {
                dispatcher = Dispatcher.CurrentDispatcher;
                updateWorker.RunWorkerAsync( new UpdateInfo( paths, files ) );
            }
        }


        void UpdateWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            var info = (UpdateInfo)e.Argument;
            var filesOld = info.files;
            var filesNew = GetInformation( info.arg );
            int index;

            //Update existing
            for( int i = filesOld.Count - 1; i >= 0; i-- )
            {
                index = FindFile( filesOld[i], filesNew );

                if( index < 0 )
                    dispatcher.Invoke( new AddRemoveFile( ( files, file ) => { filesOld.Remove( file ); } ), filesOld, filesOld[i] );
                else
                    IconUtility.BeginLoadIconAsync( filesOld[i] );
            }

            //Add new
            for( int i = 0; i < filesNew.Count; i++ )
            {
                index = FindFile( filesNew[i], filesOld );

                if( index < 0 )
                    dispatcher.Invoke( new AddRemoveFile( ( files, file ) => { files.Add( file ); } ), filesOld, filesNew[i] );
            }
        }

        delegate void AddRemoveFile( IList<FileObject> files, FileObject file );

        #endregion


        static int FindFile( FileObject find, IList<FileObject> search )
        {
            int index = -1;

            for( int i = 0; i < search.Count; i++ )
            {
                if( search[i].Path == find.Path )
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        static IList<FileObject> GetInformation( object arg )
        {
            var items = new List<FileObject>();
            int i;

            // If passed a folder path, load the contents
            if( arg == null || arg is string )
            {
                var folderPath = (string)arg;

                var directories = FileSystemUtility.Get( FileDir.Directory, folderPath );
                var files = FileSystemUtility.Get( FileDir.File, folderPath );

                for( i = 0; i < directories.Length; ++i )
                    items.Add( new FileObject( FileDir.Directory, directories[i] ) );

                for( i = 0; i < files.Length; ++i )
                    items.Add( new FileObject( FileDir.File, files[i] ) );
            }

            //If passed a list of files/folders, load them
            else if( arg is string[] )
            {
                var paths = (string[])arg;

                for( i = 0; i < paths.Length; i++ )
                    items.Add( new FileObject( FileSystemUtility.IsFile( paths[i] ), paths[i] ) );
            }

            return items;
        }
    }
}
