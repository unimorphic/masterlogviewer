using System;
using System.IO;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Windows;


namespace Common.Utility
{
    public static class ApplicationInfo
    {
        #region AssemblyInfo

        public class AssemblyInformation
        {
            public string Product { get; private set; }
            public string Copyright { get; private set; }
            public string Company { get; private set; }
            public string Version { get; private set; }

            public AssemblyInformation( Assembly assembly )
            {
                if( IsAssemblyAttributeDefined<AssemblyProductAttribute>( assembly ) )
                    Product = GetAssemblyAttribute<AssemblyProductAttribute>( assembly ).Product;

                if( IsAssemblyAttributeDefined<AssemblyCopyrightAttribute>( assembly ) )
                    Copyright = GetAssemblyAttribute<AssemblyCopyrightAttribute>( assembly ).Copyright;

                if( IsAssemblyAttributeDefined<AssemblyCompanyAttribute>( assembly ) )
                    Company = GetAssemblyAttribute<AssemblyCompanyAttribute>( assembly ).Company;

                if( IsAssemblyAttributeDefined<AssemblyFileVersionAttribute>( assembly ) )
                    Version = GetAssemblyAttribute<AssemblyFileVersionAttribute>( assembly ).Version;
            }

            bool IsAssemblyAttributeDefined<T>( Assembly assembly )
            {
                return Attribute.IsDefined( assembly, typeof(T) );
            }

            T GetAssemblyAttribute<T>( Assembly assembly ) where T : Attribute
            {
                var result = Attribute.GetCustomAttribute( assembly, typeof( T ) ) as T;
                return result;
            }
        }

        #endregion


        public static AssemblyInformation AssemblyInfo { get; private set; }

        public static string AppDataFolder { get { return Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), AssemblyInfo.Product + Path.DirectorySeparatorChar ); } }

        public static string AppPath = string.Empty;


        public static void Init()
        {
            AssemblyInfo = new AssemblyInformation( Assembly.GetCallingAssembly() );

            string exe = Application.ResourceAssembly.Location;
            AppPath = exe.Substring( 0, exe.LastIndexOf( Path.DirectorySeparatorChar ) + 1 );
        }

        public static string GetFullComputerName()
        {
            var ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            return ipProperties.DomainName.IsNullOrWhiteSpace() ? ipProperties.HostName : string.Format( "{0}.{1}", ipProperties.HostName, ipProperties.DomainName );
        }
    }

}