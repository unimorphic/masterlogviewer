﻿namespace Common.Utility
{
    public static class ClassExtensions
    {
        public static bool IsNullOrWhiteSpace( this string value )
        {
            return value == null || string.IsNullOrEmpty( value.Trim() );
        }
    }
}