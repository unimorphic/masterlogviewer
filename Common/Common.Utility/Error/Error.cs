using System;
using System.Text;
using System.Windows;


namespace Common.Utility
{
	public static class Error
	{
		public enum ErrorType
		{
			ShowAndContinue,
			Fatal
		}

		/// <summary>
		/// True = closing program due to a error (don't show any more errors)
		/// </summary>
		public static bool closingProgram = false;


        public static void DisplayAndLogError( ErrorType type, string userError, Exception exception )
        {
            if( closingProgram )
                return;

            userError = userError ?? "Application Error";
            var details = GenerateErrorDetails( userError, exception );


            Window parentWnd = null;

            try
            {
                parentWnd = Application.Current.MainWindow;
            }
            catch {}

            DisplayErrorWindow( userError, exception, details, parentWnd );
            CloseApplicationIfFatal( type );                                                        

            LogUtility.Append( details, true );
        }


	    static string GenerateErrorDetails( string userError, Exception exception )
        {
            var details = new StringBuilder();
            details.Append( "Error: " + userError + Environment.NewLine );
            details.Append( exception );

            return details.ToString();
        }


        static void DisplayErrorWindow( string userError, Exception exception, string details, Window parentWnd )
        {
            try
            {
                DisplayErrorWindow( userError, exception, parentWnd );
            }
            catch
            {
                //Try displaying the error without parent window
                try
                {
                    DisplayErrorWindow( userError, exception, null );
                }
                catch
                {
                    //Try displaying with dispatcher
                    try
                    {
                        Application.Current.Dispatcher.BeginInvoke( new Action( () =>
                        {
                            try
                            {
                                DisplayErrorWindow( userError, exception, null );
                            }
                            catch( Exception innerEx )
                            {
                                ShowingErrorWindowFailed( innerEx, details );
                            }
                            
                        } ) );
                    }
                    catch( Exception ex )
                    {
                        ShowingErrorWindowFailed( ex, details );
                    }
                }
            }
        }

        static void DisplayErrorWindow( string userError, Exception exception, Window parentWnd )
        {
            var window = new ErrorWindow( userError, exception );

            if( parentWnd != null && parentWnd.IsVisible && parentWnd.IsActive )
            {
                window.Owner = Application.Current.MainWindow;
                window.ShowInTaskbar = false;
            }

            window.ShowDialog();
        }

        static void CloseApplicationIfFatal( ErrorType type )
        {
            if( type == ErrorType.Fatal )
            {
                closingProgram = true;

                if( Application.Current != null && Application.Current.MainWindow != null )
                {
                    Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                    Application.Current.Shutdown();
                }
            }
        }

        static void ShowingErrorWindowFailed( Exception ex, string details )
        {
            string winError = GenerateErrorDetails( "Error displaying message window", ex );
#if DEBUG
            MessageBox.Show( winError, "Error" );
#endif
            LogUtility.Append( winError, true );

            MessageBox.Show( details, "Error" );
        }
	}
}
