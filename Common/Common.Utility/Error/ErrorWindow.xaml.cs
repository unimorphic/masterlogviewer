﻿using System;
using System.Windows;

namespace Common.Utility
{
    public partial class ErrorWindow : Window
    {
        public ErrorWindow( string userError, Exception exception )
        {
            InitializeComponent();
            Title = ApplicationInfo.AssemblyInfo.Product + " Error";
            message.Text = userError;
            source.Text = exception.Source;
            stackTrace.Text = exception.ToString();
        }


        private void WindowOnContentRendered( object sender, EventArgs e )
        {
            var bounds = SystemParameters.WorkArea;

            if( Owner != null )
                bounds = new Rect( Owner.Left, Owner.Top, Owner.ActualWidth, Owner.ActualHeight );

            var point = new Point( bounds.Left + Math.Round( ( bounds.Width - ActualWidth ) / 2.0 ),
                                          bounds.Top + Math.Round( ( bounds.Height - ActualHeight ) / 2.0 ) );
            Left = point.X;
            Top = point.Y;
        }

        private void CopyOnClick( object sender, RoutedEventArgs routedEventArgs )
        {
            Clipboard.SetText( stackTrace.Text );
        }

        private void CloseOnClick( object sender, RoutedEventArgs routedEventArgs )
        {
            Close();
        }
    }
}
