using System;
using System.Windows.Media.Imaging;


namespace Common.Utility
{
	public static class ImageUtility
	{
        static public BitmapImage GetImageResource( string imageSource )
        {
            return new BitmapImage( new Uri( imageSource ) );
        }

        static public BitmapImage GetImageResource( string reference, string resourceName )
        {
            return new BitmapImage( new Uri( GetFullResourceName( reference, resourceName ) ) );
        }

        static public BitmapFrame GetIconResource( string imageSource )
        {
            return BitmapFrame.Create( new Uri( imageSource ) );
        }
        
        static public BitmapFrame GetIconResource( string reference, string resourceName )
        {
            return BitmapFrame.Create( new Uri( GetFullResourceName( reference, resourceName ) ) );
        }

        static public string GetFullResourceName( string reference, string resourceName )
        {
            string name = string.Empty;

            if( reference == null || reference.Length <= 0 )
                name = @"pack://application:,,,/Resources/" + resourceName;
            else
                name = "pack://application:,,,/" + reference + ";component/Resources/" + resourceName;

            return name;
        }
	}
}
