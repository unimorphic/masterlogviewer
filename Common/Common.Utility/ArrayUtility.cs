using System.Collections.Generic;
using System.Linq;

namespace Common.Utility
{
	public static class ArrayUtility
	{
		public static bool Copy<T>( T[] source, int sourceIndex, T[] destination, int destIndex )
		{
			var success = true;

			if( source.Length - sourceIndex == destination.Length - destIndex )
			{
				for( int x = sourceIndex, y = destIndex; x < source.Length; ++x, ++y )
					destination[y] = source[x];
			}
			else
				success = false;

			return success;
		}

        /// <summary>
        /// Break a list of items into chunks of a specific size
        /// </summary>
        public static IEnumerable<IEnumerable<T>> Chunk<T>( this IEnumerable<T> source, int chunksize )
        {
            while( source.Any() )
            {
                yield return source.Take( chunksize );
                source = source.Skip( chunksize );
            }
        }
	}
}
