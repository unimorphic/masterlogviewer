﻿using System;
using System.Configuration;

namespace Common.Utility
{
    public static class SettingsUtility
    {
        public static void AddSettingIfNeeded( ApplicationSettingsBase settings, string settingName, Type type )
        {
            if( settings.Properties[settingName] == null )
            {
                var property = new SettingsProperty( settingName ) {PropertyType = type, Provider = settings.Providers["LocalFileSettingsProvider"]};
                property.DefaultValue = null;
                property.IsReadOnly = false;
                property.SerializeAs = SettingsSerializeAs.Xml;
                property.Attributes.Add( typeof( UserScopedSettingAttribute ), new UserScopedSettingAttribute() );
                settings.Properties.Add( property );
            }
        }
    }
}
