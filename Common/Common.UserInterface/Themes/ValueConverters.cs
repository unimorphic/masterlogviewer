﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;


namespace Common.UserInterface
{
    public class FlagConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return ( (int)value & (int)Enum.Parse( value.GetType(), parameter.ToString() ) ) > 0;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return null;
        }
    }

    public class MathConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            string param = parameter.ToString();
            double result = 0.0;

            if( param.Length > 0 )
            {
                switch( param[0] )
                {
                    case '*':
                        result = (double)value * double.Parse( param.Substring( 1 ) );
                        break;

                    case '/':
                    case '\\':
                        result = (double)value / double.Parse( param.Substring( 1 ) );
                        break;

                    case '-':
                        result = (double)value - double.Parse( param.Substring( 1 ) );
                        break;

                    default:
                        result = (double)value + double.Parse( param );
                        break;
                }
            }

            return result;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return null;
        }
    }

    public class BooleanToVisibilityNegConverter : IValueConverter
    {
        private readonly BooleanToVisibilityConverter _converter = new BooleanToVisibilityConverter();

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var result = _converter.Convert( value, targetType, parameter, culture ) as Visibility?;
            return result == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var result = _converter.ConvertBack( value, targetType, parameter, culture ) as bool?;
            return result != true;
        }
    }

    public class BooleanToVisibilityHiddenConverter : IValueConverter
    {
        private readonly BooleanToVisibilityConverter _converter = new BooleanToVisibilityConverter();

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var result = _converter.Convert( value, targetType, parameter, culture ) as Visibility?;
            return result == Visibility.Collapsed ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var result = _converter.ConvertBack( value, targetType, parameter, culture ) as bool?;
            return result != true;
        }
    }

    public class BooleanToVisibilityHiddenNegConverter : IValueConverter
    {
        private readonly BooleanToVisibilityConverter _converter = new BooleanToVisibilityConverter();

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var result = _converter.Convert( value, targetType, parameter, culture ) as Visibility?;
            return result == Visibility.Collapsed ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var result = _converter.ConvertBack( value, targetType, parameter, culture ) as bool?;
            return result != true;
        }
    }

    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return value == null ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw ( new NotImplementedException() );
        }
    }

    public class ColorToSolidBrushConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return new SolidColorBrush( value == null ? Colors.Black : (Color)value );
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return value != null ? ( (SolidColorBrush)value ).Color : Colors.Black;
        }
    }

    public class BooleanNegConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return !(bool)value;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return !(bool)value;
        }
    }

    public class IntToVisibilityConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var visibility = Visibility.Collapsed;

            if( value is int )
                visibility = (int)value > 0 ? Visibility.Visible : Visibility.Collapsed;
            if( value is string )
                visibility = ( (string)value ).Length > 0 ? Visibility.Visible : Visibility.Collapsed;

            return visibility;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw ( new NotImplementedException() );
        }
    }

    public class StringEmptyToVisibilityConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return ( value is string && !string.IsNullOrEmpty( (string)value ) ) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }
}
