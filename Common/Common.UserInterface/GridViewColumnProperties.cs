﻿using System.ComponentModel;
using System.Windows;

namespace Common.UserInterface
{
    public static class GridViewColumnProperties
    {
        public static readonly DependencyProperty SortDirectionProperty =
           DependencyProperty.RegisterAttached( "SortDirection", typeof( ListSortDirection? ), typeof( GridViewColumnProperties ), new UIPropertyMetadata( null ) );


        public static ListSortDirection? GetSortDirection( DependencyObject obj )
        {
            return (ListSortDirection?)obj.GetValue( SortDirectionProperty );
        }

        public static void SetSortDirection( DependencyObject obj, ListSortDirection? value )
        {
            obj.SetValue( SortDirectionProperty, value );
        }


        public static readonly DependencyProperty BindingPathProperty =
           DependencyProperty.RegisterAttached( "BindingPath", typeof( string ), typeof( GridViewColumnProperties ), new UIPropertyMetadata( string.Empty ) );


        public static string GetBindingPath( DependencyObject obj )
        {
            return (string)obj.GetValue( BindingPathProperty );
        }

        public static void SetBindingPath( DependencyObject obj, string value )
        {
            obj.SetValue( BindingPathProperty, value );
        }
    }
}