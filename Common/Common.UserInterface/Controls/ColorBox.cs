﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Common.UserInterface
{
    public class ColorBox : Button
    {
        WinFormInterop.WinFormInteropLib winForm = new WinFormInterop.WinFormInteropLib();


        public static readonly DependencyProperty SelectedColorProperty =
               DependencyProperty.Register( "SelectedColor", typeof( Color ), typeof( ColorBox ), new PropertyMetadata( Colors.Blue ) );

        public Color SelectedColor
        {
            get { return (Color)GetValue( SelectedColorProperty ); }
            set { SetValue( SelectedColorProperty, value ); }
        }


        public ColorBox()
		{
            SetResourceReference( StyleProperty, typeof( ColorBox ) );
            Click += OnClick;
		}


        private void OnClick( object sender, RoutedEventArgs routedEventArgs )
        {
            var color = winForm.ShowColorDialog( SelectedColor );
            if( color.HasValue )
                SelectedColor = color.Value;
        }
    }
}