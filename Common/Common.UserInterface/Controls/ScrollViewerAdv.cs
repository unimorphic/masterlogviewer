﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Common.Utility;


namespace Common.UserInterface
{
    /// <summary>
    /// ScrollViewer that has the following advanced features:
    /// 1. HorizontalScrollOffset property
    /// 2. If the vertical scroll bar is disabled, the mouse operates as a horizontal scroller
    /// 3. If either scroll bar is enabled, the corresponding measurement will not affect a window with SizeToContent set (if SizeToContent is false)
    /// </summary>
    public class ScrollViewerAdv : ScrollViewer
    {
        bool heightNotSet;
        bool widthNotSet;


        #region Properties

        public static readonly DependencyProperty HorizontalScrollOffsetProperty =
            DependencyProperty.Register( "HorizontalScrollOffset", typeof( double ), typeof( ScrollViewerAdv ),
                                            new PropertyMetadata( HorizontalScrollOffset_Changed ) );

        public double HorizontalScrollOffset
        {
            get { return (double)GetValue( HorizontalScrollOffsetProperty ); }
            set { SetValue( HorizontalScrollOffsetProperty, value ); }
        }


        public static readonly DependencyProperty SizeToContentProperty =
            DependencyProperty.RegisterAttached( "SizeToContent", typeof( bool ), typeof( ScrollViewerAdv ),
            new FrameworkPropertyMetadata( true, FrameworkPropertyMetadataOptions.Inherits | FrameworkPropertyMetadataOptions.AffectsMeasure ) );

        [DefaultValue( true )]
        public bool SizeToContent
        {
            get { return (bool)GetValue( SizeToContentProperty ); }
            set { SetValue( SizeToContentProperty, value ); }
        }


        public static void SetSizeToContent( UIElement element, bool value )
        {
            element.SetValue( SizeToContentProperty, value );
        }

        public static bool GetSizeToContent( UIElement element )
        {
            return (bool)element.GetValue( SizeToContentProperty );
        }

        #endregion


        public ScrollViewerAdv()
        {
            Loaded += ControlOnLoaded;
        }


        void ControlOnLoaded( object sender, RoutedEventArgs e )
        {
            if( Style == null )
                SetResourceReference( StyleProperty, typeof( ScrollViewer ) );

            heightNotSet = Height.Equals( double.NaN );
            widthNotSet = Width.Equals( double.NaN );
        }


        protected override Size MeasureOverride( Size constraint )
        {
            if( IsLoaded && !SizeToContent )
            {
                //3. If either scroll bar is enabled, the corresponding measurement will not affect a window with SizeToContent set (if SizeToContent is false)
                var avalibleRect = System.Windows.Controls.Primitives.LayoutInformation.GetLayoutSlot( this );

                if( VerticalScrollBarVisibility != ScrollBarVisibility.Disabled && VerticalAlignment == VerticalAlignment.Stretch && heightNotSet )
                    Height = avalibleRect.Height;

                if( HorizontalScrollBarVisibility != ScrollBarVisibility.Disabled && HorizontalAlignment == HorizontalAlignment.Stretch && widthNotSet )
                    Width = avalibleRect.Width;
            }

            return base.MeasureOverride( constraint );
        }

        protected override void OnMouseWheel( MouseWheelEventArgs e )
        {
            //2. If the vertical scroll bar is disabled, the mouse operates as a horizontal scroller
            if( VerticalScrollBarVisibility == ScrollBarVisibility.Disabled && HorizontalScrollBarVisibility != ScrollBarVisibility.Disabled )
            {
                e.Handled = true;
                if( e.Delta > 0 )
                    ScrollInfo.MouseWheelLeft();
                else if( e.Delta < 0 )
                    ScrollInfo.MouseWheelRight();
            }
            else
                base.OnMouseWheel( e );
        }

        static void HorizontalScrollOffset_Changed( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            var scrollViewer = source as ScrollViewerAdv;
            if( scrollViewer != null )
                scrollViewer.ScrollToHorizontalOffset( scrollViewer.HorizontalScrollOffset );
        }
    }
}
