﻿using System.Windows.Controls;


namespace Common.UserInterface
{
    public class LoadingAnim : Control
    {
        public LoadingAnim()
        {
            SetResourceReference( StyleProperty, typeof( LoadingAnim ) );
        }
    }
}
