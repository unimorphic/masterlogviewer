﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Common.UserInterface
{
    public class SearchBox : TextBox
    {
        private readonly DispatcherTimer searchTimer;


        public static readonly DependencyProperty ValueProperty =
               DependencyProperty.Register( "Value", typeof( string ), typeof( SearchBox ), new PropertyMetadata( string.Empty, ValuePropertyChangedCallback ) );

        public string Value
        {
            get { return (string)GetValue( ValueProperty ); }
            set { SetValue( ValueProperty, value ); }
        }


        public static readonly DependencyProperty UpdateOnTextChangedProperty =
               DependencyProperty.Register( "UpdateOnTextChanged", typeof( bool ), typeof( SearchBox ), new PropertyMetadata( true ) );

        public bool UpdateOnTextChanged
        {
            get { return (bool)GetValue( UpdateOnTextChangedProperty ); }
            set { SetValue( UpdateOnTextChangedProperty, value ); }
        }


        public event Action<string> Search = delegate {}; 


        public SearchBox()
        {
            SetResourceReference( StyleProperty, typeof(TextBox) );

            // 500 interval is too slow due to the delay between holding a down a key and when the text is entered
            searchTimer = new DispatcherTimer( new TimeSpan( 0, 0, 0, 0, 600 ), DispatcherPriority.Normal, SearchTimer_Tick, Dispatcher );
            searchTimer.Stop();

            MaxLines = 1;
            TextChanged += Search_TextChanged;
        }


        private void Search_TextChanged( object sender, TextChangedEventArgs e )
        {
            if( UpdateOnTextChanged )
            {
                searchTimer.Stop();
                searchTimer.Start();
            }
        }

        private void SearchTimer_Tick( object sender, EventArgs eventArgs )
        {
            searchTimer.Stop();
            Value = Text;
            Search( Text );
        }

        private static void ValuePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            var source = dependencyObject as SearchBox;
            if( source != null )
                source.Text = source.Value;
        }
    }
}