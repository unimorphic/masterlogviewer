using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Common.Utility;


namespace Common.UserInterface
{
	public class DirectoryBox : Control
	{
		WinFormInterop.WinFormInteropLib winForm = new WinFormInterop.WinFormInteropLib();


		public static readonly DependencyProperty DirectoryProperty =
			   DependencyProperty.Register( "Directory", typeof( string ), typeof( DirectoryBox ),
											new FrameworkPropertyMetadata( string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DirectoryProperty_Changed ) );

		[DefaultValue( "" )]
		public string Directory
		{
			get { return (string)GetValue( DirectoryProperty ); }
			set { SetValue( DirectoryProperty, value ); }
		}


        public static readonly DependencyProperty IsReadOnlyProperty =
               DependencyProperty.Register( "IsReadOnly", typeof( bool ), typeof( DirectoryBox ), new PropertyMetadata( false ) );

		[DefaultValue( false )]
		public bool IsReadOnly
		{
            get { return (bool)GetValue( IsReadOnlyProperty ); }
            set { SetValue( IsReadOnlyProperty, value ); }
		}


		public event EventHandler DirectoryChanged = delegate { };


		public DirectoryBox()
		{
			SetResourceReference( StyleProperty, typeof( DirectoryBox ) );
		}


		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			var button = GetTemplateChild( "PART_BrowseButton" ) as ButtonBase;
			if( button != null )
				button.Click += BrowseButton_Click;
		}


		void BrowseButton_Click( object sender, RoutedEventArgs e )
		{
			var dir = winForm.ShowFolderDialog( Directory );
			if( !string.IsNullOrEmpty( dir ) )
				Directory = dir;
		}

		static void DirectoryProperty_Changed( DependencyObject source, DependencyPropertyChangedEventArgs e )
		{
			var box = source as DirectoryBox;
			if( box != null )
			{
			    string dir = System.IO.Directory.Exists( box.Directory ) ? FileSystemUtility.StandarizePath( box.Directory ) : box.Directory;
				if( box.Directory != dir )
				{
					box.Directory = dir;
					if( FileSystemUtility.Exists( FileDir.Directory, box.Directory ) )
						box.DirectoryChanged( box, EventArgs.Empty );
				}
			}
		}
	}
}
