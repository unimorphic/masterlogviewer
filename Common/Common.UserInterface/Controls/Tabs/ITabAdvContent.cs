﻿using System;
using System.Windows;

namespace Common.UserInterface
{
    public class SwitchAwayFromTabEventArgs
    {
        public bool Cancel { get; set; }
    }

    public interface ITabAdvContent
    {
        event Action<string, string, FrameworkElement> AddNewTab;

        void OnSwitchToTab();

        void OnSwitchAwayFromTab( SwitchAwayFromTabEventArgs e );

        void OnClosingTab( SwitchAwayFromTabEventArgs e );

        void OnTabClosed();
    }
}