﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Common.UserInterface
{
    public class TabControlAdv : TabControl
    {
        private TabItem previousTab;
        private readonly List<object> lastSelectedTabs = new List<object>();


        public static readonly DependencyProperty TabsMarginProperty =
            DependencyProperty.Register( "TabsMargin", typeof(Thickness), typeof(TabControlAdv), new PropertyMetadata( new Thickness( 2, 2, 2, 0 ) ) );

        public Thickness TabsMargin
        {
            get { return (Thickness)GetValue( TabsMarginProperty ); }
            set { SetValue( TabsMarginProperty, value ); }
        }


        public static readonly DependencyProperty HasMoreThenOneItemProperty =
            DependencyProperty.Register( "HasMoreThenOneItem", typeof(bool), typeof(TabControlAdv) );

        public bool HasMoreThenOneItem
        {
            get { return (bool)GetValue( HasMoreThenOneItemProperty ); }
            set { SetValue( HasMoreThenOneItemProperty, value ); }
        }


		public static readonly RoutedUICommand CloseAllCmd = new RoutedUICommand( "Close All", "CloseAllCmd", typeof( TabControlAdv ) );
		public static readonly RoutedUICommand CloseAllButThisCmd = new RoutedUICommand( "Close All But This", "CloseAllButThisCmd", typeof( TabControlAdv ) );


        public TabControlAdv()
        {
            SetResourceReference( StyleProperty, typeof( TabControlAdv ) );
            
            PreviewKeyDown += OnPreviewKeyDown;
            KeyUp += OnKeyUp;
            MouseUp += OnMouseUp;
            ((INotifyCollectionChanged)Items).CollectionChanged += ItemsOnCollectionChanged;
            SelectionChanged += OnSelectionChanged;
            CommandBindings.Add( new CommandBinding( ApplicationCommands.Close, CloseCommandOnExecuted ) );
			CommandBindings.Add( new CommandBinding( CloseAllCmd, CloseAllCmdOnExecuted ) );
			CommandBindings.Add( new CommandBinding( CloseAllButThisCmd, CloseAllButThisCmdOnExecuted ) );
        }


        private void OnPreviewKeyDown( object sender, KeyEventArgs e )
        {
            if( e.Key == Key.Tab && ( Keyboard.IsKeyDown( Key.LeftCtrl ) || Keyboard.IsKeyDown( Key.RightCtrl ) ) )
            {
                if( lastSelectedTabs.Count > 0 )
                {
                    var index = lastSelectedTabs.IndexOf( SelectedItem );
                    if( index < 0 )
                    {
                        lastSelectedTabs.Add( SelectedItem );
                        SelectedItem = lastSelectedTabs[lastSelectedTabs.Count - 2];
                    }
                    else if( index == 0 )
                        SelectedItem = lastSelectedTabs[lastSelectedTabs.Count - 1];
                    else
                        SelectedItem = lastSelectedTabs[index - 1];
                }
                e.Handled = true;
            }
        }

        private void OnKeyUp( object sender, KeyEventArgs e )
        {
            if( e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl )
            {
                var index = lastSelectedTabs.IndexOf( SelectedItem );
                if( index >= 0 )
                {
                    if( index >= lastSelectedTabs.Count - 1 )
                    {
                        var tab = lastSelectedTabs[0];
                        lastSelectedTabs.RemoveAt( 0 );
                        lastSelectedTabs.Add( tab );
                    }
                    else
                    {
                        var tabsToInsert = new List<object>();
                        for( int i = lastSelectedTabs.Count - 1; i > index + 1; i-- )
                        {
                            tabsToInsert.Add( lastSelectedTabs[i] );
                            lastSelectedTabs.RemoveAt( i );
                        }
                        foreach( var tab in tabsToInsert )
                            lastSelectedTabs.Insert( 0, tab );
                    }
                }
                if( lastSelectedTabs.Contains( SelectedItem ) )
                    lastSelectedTabs.Remove( SelectedItem );
            }
        }

        private void OnMouseUp( object sender, MouseButtonEventArgs e )
        {
            switch( e.ChangedButton )
            {
                case MouseButton.Middle:
                    AttemptToCloseTab( this.FindItem<TabItem>( e.OriginalSource ) );
                    break;
            }
        }

        private void ItemsOnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            switch( e.Action )
            {
                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Add:
                case NotifyCollectionChangedAction.Remove:
                    if( e.NewItems != null )
                    {
                        foreach( TabItem item in e.NewItems )
                        {
                            var tabContent = item.Content as ITabAdvContent;
                            if( tabContent != null )
                                tabContent.AddNewTab += TabOnAddNewTab;
                        }
                    }
                    if( e.OldItems != null )
                    {
                        foreach( TabItem item in e.OldItems )
                        {
                            var tabContent = item.Content as ITabAdvContent;
                            if( tabContent != null )
                                tabContent.AddNewTab -= TabOnAddNewTab;

                            if( lastSelectedTabs.Contains( item ) )
                                lastSelectedTabs.Remove( item );
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    lastSelectedTabs.Clear();
                    foreach( TabItem item in Items )
                    {
                        var tabContent = item.Content as ITabAdvContent;
                        if( tabContent != null )
                        {
                            tabContent.AddNewTab -= TabOnAddNewTab;
                            tabContent.AddNewTab += TabOnAddNewTab;
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
            }

            HasMoreThenOneItem = Items.Count > 1;
        }

        private void OnSelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if( SelectedItem != null )
            {
                ITabAdvContent tabContent;
                var contin = true;

                if( previousTab != null && !Equals( previousTab, SelectedItem ) )
                {
                    tabContent = previousTab.Content as ITabAdvContent;
                    if( tabContent != null )
                    {
                        var args = new SwitchAwayFromTabEventArgs();
                        tabContent.OnSwitchAwayFromTab( args );
                        contin = !args.Cancel;
                    }
                }

                if( contin )
                {
                    if( !Equals( previousTab, SelectedItem ) )
                    {
                        tabContent = ( (TabItem)SelectedItem ).Content as ITabAdvContent;
                        if( tabContent != null )
                            tabContent.OnSwitchToTab();

                        var tabCntl = ( (TabItem)SelectedItem ).Content as FrameworkElement;
                        if( tabCntl != null )
                            Dispatcher.BeginInvoke( DispatcherPriority.Background, new Action( () => tabCntl.MoveFocus(new TraversalRequest(FocusNavigationDirection.First)) ) );

                        if( previousTab != null && !Keyboard.IsKeyDown( Key.LeftCtrl ) && !Keyboard.IsKeyDown( Key.RightCtrl ) )
                        {
                            if( lastSelectedTabs.Contains( previousTab ) )
                                lastSelectedTabs.Remove( previousTab );
                            lastSelectedTabs.Add( previousTab );
                        }

                        previousTab = (TabItem)SelectedItem;
                    }
                }
                else
                    SelectedItem = previousTab;
            }
        }

        private void CloseCommandOnExecuted( object target, ExecutedRoutedEventArgs e )
        {
            AttemptToCloseTab( this.FindItem<TabItem>( e.OriginalSource ) );
        }

		private void CloseAllCmdOnExecuted( object sender, ExecutedRoutedEventArgs e )
        {
            for( var i = Items.Count - 1; i >= 0; i-- )
                AttemptToCloseTab( (TabItem)Items[i] );
        }

		private void CloseAllButThisCmdOnExecuted( object sender, ExecutedRoutedEventArgs e )
        {
            int index = Items.IndexOf( this.FindItem<TabItem>( e.OriginalSource ) );

            for( var i = Items.Count - 1; i >= 0; i-- )
            {
                if( i != index )
                    AttemptToCloseTab( (TabItem)Items[i] );
            }
        }

        private void TabOnAddNewTab( string title, string iconData, FrameworkElement contentCntl )
        {
            var item = new TabItemAdv {Title = title, IconData = System.Windows.Media.Geometry.Parse(iconData), Content = contentCntl};
            Items.Insert( 0, item );
            Dispatcher.BeginInvoke( DispatcherPriority.Normal, new Action( () =>
            {
                SelectedItem = item;
                contentCntl.Focus();
            } ) );
        }


        private void AttemptToCloseTab( TabItem tab )
        {
            if( tab != null && ( !( tab is TabItemAdv ) || ( (TabItemAdv)tab ).IsClosable ) )
            {
                var close = true;

                var tabContent = tab.Content as ITabAdvContent;
                if( tabContent != null )
                {
                    var args = new SwitchAwayFromTabEventArgs();
                    tabContent.OnClosingTab( args );
                    close = !args.Cancel;
                }

                if( close )
                {
                    var itemIsCurrentItem = Equals( tab, SelectedItem );
                    Items.Remove( tab );

                    if( tabContent != null )
                        tabContent.OnTabClosed();

                    if( itemIsCurrentItem && Items.Count > 0 )
                    {
                        var lastTab = lastSelectedTabs.Count > 0 ? lastSelectedTabs[lastSelectedTabs.Count - 1] : null;
                        lastTab = lastTab ?? Items[0];
                        SelectedItem = lastTab;

                        //Weird graphics issue with z-index
                        Dispatcher.BeginInvoke( DispatcherPriority.SystemIdle, new Action( () =>
                        {
                            SelectedItem = null;
                            SelectedItem = lastTab;
                        } ) );
                    }
                }
            }
        }
    }
}