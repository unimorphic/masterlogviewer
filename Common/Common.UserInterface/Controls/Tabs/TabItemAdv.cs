﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Common.UserInterface
{
    public class TabItemAdv : TabItem
    {
        public static readonly DependencyProperty IconDataProperty =
                DependencyProperty.Register( "IconData", typeof( Geometry ), typeof( TabItemAdv ) );

        public Geometry IconData
        {
            get { return (Geometry)GetValue( IconDataProperty ); }
            set { SetValue( IconDataProperty, value ); }
        }


        public static readonly DependencyProperty TitleProperty =
                DependencyProperty.Register( "Title", typeof( string ), typeof( TabItemAdv ) );

        public string Title
        {
            get { return (string)GetValue( TitleProperty ); }
            set { SetValue( TitleProperty, value ); }
        }


        public static readonly DependencyProperty IsClosableProperty =
                DependencyProperty.Register( "IsClosable", typeof( bool ), typeof( TabItemAdv ), new PropertyMetadata( true ) );

        public bool IsClosable
        {
            get { return (bool)GetValue( IsClosableProperty ); }
            set { SetValue( IsClosableProperty, value ); }
        }


        public TabItemAdv()
        {
            SetResourceReference( StyleProperty, typeof( TabItemAdv ) );
        }
    }
}