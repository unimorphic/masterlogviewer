﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Data;
using Common.Utility;
using Microsoft.Windows.Controls;


namespace Common.UserInterface
{
    public class DirectoryBrowser : DataGrid
    {
        #region Custom Sort

        class MySort : IComparer
        {
            private readonly ListSortDirection direction;
            private readonly DataGridColumn column;

            public MySort( ListSortDirection direction, DataGridColumn column )
            {
                this.direction = direction;
                this.column = column;
            }

            int IComparer.Compare( object one, object two )
            {
                var file1 = (FileObject)one;
                var file2 = (FileObject)two;
                var result = file1.FileDir.CompareTo( file2.FileDir );

                if( result == 0 )
                {
                    switch( (string)column.Header )
                    {
                        case "Name":
                            result = CompareObjects( file1.Name, file2.Name );
                            break;
                        case "Type":
                            result = CompareObjects( file1.Extension, file2.Extension );
                            break;
                        case "Created On":
                            result = CompareObjects( file1.CreatedOn, file2.CreatedOn );
                            break;
                        case "Modified On":
                            result = CompareObjects( file1.LastModified, file2.LastModified );
                            break;
                    }

                    if( result == 0 )
                        result = CompareObjects( file1.Name, file2.Name );
                }

                return result;
            }

            int CompareObjects<T>( T s1, T s2 ) where T : IComparable<T>
            {
                return direction == ListSortDirection.Ascending ? s1.CompareTo( s2 ) : s2.CompareTo( s1 );
            }
        }

        #endregion


        string folderPath = string.Empty;
        string folderPathOriginal = string.Empty;

        readonly LoadFilesAsyncUtility manager = new LoadFilesAsyncUtility();
        ObservableCollection<FileObject> items;


        #region Properties/Events

        /// <summary>Directory</summary>
        public static readonly DependencyProperty DirectoryProperty =
               DependencyProperty.Register( "Directory", typeof( string ), typeof( DirectoryBrowser ),
                                            new FrameworkPropertyMetadata( string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DirectoryProperty_Changed ) );

        /// <summary>
        /// Directory
        /// </summary>
        [DefaultValue( "" )]
        public string Directory
        {
            get { return (string)GetValue( DirectoryProperty ); }
            set { SetValue( DirectoryProperty, value ); }
        }


        /// <summary>On current path changed</summary>
        public event Action<object, string, string> PathChanged = null;

        /// <summary>On refreshed</summary>
        public event Action Refreshed = null;

        public event Action<object> ItemDoubleClicked = delegate {};

        #endregion


        /// <summary>
        /// Constructor
        /// </summary>
        public DirectoryBrowser()
        {
            SetResourceReference( StyleProperty, typeof( DataGrid ) );
            AutoGenerateColumns = false;
            HeadersVisibility = DataGridHeadersVisibility.Column;
            SelectionMode = DataGridSelectionMode.Single;
            RowStyle = new Style( typeof( DataGridRow ), (Style)Application.Current.Resources[typeof( DataGridRow )] );
            RowStyle.Setters.Add( new EventSetter( MouseDoubleClickEvent, new MouseButtonEventHandler( RowDoubleClick ) ) );

            Columns.Add( new DataGridTemplateColumn { Header = "Name", CellTemplate = (DataTemplate)Application.Current.Resources["DirectoryBrowserNameTemplate"], SortMemberPath = "Name"} );
            Columns.Add( new DataGridTextColumn { Header = "Type", Binding = new Binding( "Extension" ) { Mode = BindingMode.OneWay } } );
            Columns.Add( new DataGridTextColumn { Header = "Created On", Binding = new Binding( "CreatedOn" ) { Mode = BindingMode.OneWay } } );
            Columns.Add( new DataGridTextColumn { Header = "Modified On", Binding = new Binding( "LastModified" ) { Mode = BindingMode.OneWay }, SortDirection = ListSortDirection.Descending} );

            manager.OnLoadFileStruct += Manager_OnLoadFileStruct;
            Sorting += Control_Sorting;
        }


        public void Refresh()
        {
            if( ItemsSource != null )   //Wait until contents are loaded before allow refreshing
                manager.BeginUpdateFileStructAsync( folderPathOriginal, items );
        }


        #region Populating

        public void Repopulate()
        {
            if( ItemsSource != null )   //Wait until contents are loaded before allow re-populating
                Populate( folderPath );
        }

        public void Populate( string folderPath )
        {
            if( !manager.IsWorking )
            {
                manager.BeginLoadFileStructAsync( folderPath );
                string stndPath = FileSystemUtility.StandarizePath( folderPath );

                if( this.folderPath != stndPath )
                {
                    string oldPath = this.folderPath;
                    this.folderPath = stndPath;
                    folderPathOriginal = folderPath;

                    if( Directory.ToLower() != folderPath.ToLower() )
                        Directory = folderPath;

                    if( PathChanged != null )
                        PathChanged( this, oldPath, folderPath );
                }
            }
        }

        #endregion


        #region Control Events

        void Manager_OnLoadFileStruct( IList<FileObject> value )
        {
            if( items == null )
            {
                var source = new CollectionViewSource { Source = items = new ObservableCollection<FileObject>( value ) };
                source.SortDescriptions.Add( new SortDescription( "FileDir", ListSortDirection.Ascending ) );
                source.SortDescriptions.Add( new SortDescription( "LastModified", ListSortDirection.Descending ) );
                var binding = new Binding { Source = source };
                BindingOperations.SetBinding( this, ItemsSourceProperty, binding );
                SelectedItem = null;
            }
            else
            {
                items.Clear();
                foreach( var fileObject in value )
                    items.Add( fileObject );
            }

            if( Refreshed != null )
                Refreshed();
        }

        private void Control_Sorting( object sender, DataGridSortingEventArgs e )
        {
            e.Handled = true;

            var direction = ( e.Column.SortDirection != ListSortDirection.Ascending ) ? ListSortDirection.Ascending : ListSortDirection.Descending;
            e.Column.SortDirection = direction;

            var lcv = (ListCollectionView)CollectionViewSource.GetDefaultView( ItemsSource );
            lcv.CustomSort = new MySort( direction, e.Column );
        }

        #endregion


        private void RowDoubleClick( object sender, RoutedEventArgs e )
        {
            if( SelectedItems.Count > 0 )
            {
                var f = (FileObject)SelectedItem;

                switch( f.FileDir )
                {
                    case FileDir.Directory:
                        Populate( f.Path );
                        break;
                    case FileDir.File:
                        ItemDoubleClicked( f );
                        break;
                }
            }
        }

        static void DirectoryProperty_Changed( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            var box = source as DirectoryBrowser;
            if( box != null )
            {
                string dir = FileSystemUtility.StandarizePath( box.Directory );
                box.Directory = dir;
                if( FileSystemUtility.Exists( FileDir.Directory, dir ) )
                    box.Populate( dir );

            }
        }
    }
}
