﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace Common.UserInterface
{
    public static class VisualTreeUtility
    {
        public static T FindVisualChild<T>( DependencyObject data ) where T : class
        {
            object child = null;

            if( data is T )
                return data as T;

            for( int i = 0; i < VisualTreeHelper.GetChildrenCount( data ); i++ )
            {
                child = VisualTreeHelper.GetChild( data, 0 );
                if( child is T )
                    break;
                else
                    child = FindVisualChild<T>( (DependencyObject)child );
            }
            
            return child as T;
        }

        public static T FindVisualParent<T>( DependencyObject data ) where T : class
        {
            object parent = data;

            if( data != null && !( data is T ) )
                parent = FindVisualParent<T>( VisualTreeHelper.GetParent( data ) );

            return parent as T;
        }

        public static T FindItem<T>( this ItemsControl data, object source ) where T : class
        {
            object item = null;
            var listItem = (DependencyObject)source;
            while( listItem != null && !( listItem is T ) )
            {
                if( listItem is FrameworkElement && ( (FrameworkElement)listItem ).Parent is Popup )
                    listItem = ( (Popup)( (FrameworkElement)listItem ).Parent ).PlacementTarget;

                if( listItem is Visual )
                    listItem = VisualTreeHelper.GetParent( listItem );
                else
                    listItem = null;
            }

            if( listItem != null )
                item = data.ItemContainerGenerator.ItemFromContainer( listItem );

            return item as T;
        }
    }
}