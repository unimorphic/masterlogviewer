﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FirstFloor.ModernUI.Windows.Converters
{
    class MathConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            string param = parameter.ToString();
            double result = 0.0;

            if( param.Length > 0 )
            {
                switch( param[0] )
                {
                    case '*':
                        result = (double)value * double.Parse( param.Substring( 1 ) );
                        break;

                    case '/':
                    case '\\':
                        result = (double)value / double.Parse( param.Substring( 1 ) );
                        break;

                    case '-':
                        result = (double)value - double.Parse( param.Substring( 1 ) );
                        break;

                    default:
                        result = (double)value + double.Parse( param );
                        break;
                }
            }

            return result;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            return null;
        }
    }
}