﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace FirstFloor.ModernUI.Presentation
{
    /// <summary>
    /// Identifies the available font size.
    /// </summary>
    public enum FontSize
    {
        /// <summary>
        /// Large fonts.
        /// </summary>
        Large,
        /// <summary>
        /// Small fonts.
        /// </summary>
        Small
    }
}
