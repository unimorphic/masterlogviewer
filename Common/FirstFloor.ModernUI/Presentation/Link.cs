﻿using System;


namespace FirstFloor.ModernUI.Presentation
{
    /// <summary>
    /// Represents a displayable link.
    /// </summary>
    public class Link
        : Displayable
    {
        private Uri source;
        private bool isHeading;

        /// <summary>
        /// Gets or sets the source uri.
        /// </summary>
        /// <value>The source.</value>
        public Uri Source
        {
            get { return source; }
            set
            {
                if( source != value )
                {
                    source = value;
                    OnPropertyChanged( "Source" );
                }
            }
        }

        /// <summary>
        /// Gets or sets if the link is a heading
        /// </summary>
        /// <value>The source.</value>
        public bool IsHeading
        {
            get { return isHeading; }
            set
            {
                if( isHeading != value )
                {
                    isHeading = value;
                    OnPropertyChanged( "IsHeading" );
                }
            }
        }
    }
}
