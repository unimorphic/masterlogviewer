﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;

namespace FirstFloor.ModernUI
{
    //Replacement for the .net 4.0 DependencyObject.SetCurrentValue method, not sure exactly why this replacement works, but as far as I can tell it does work
    static class SetCurrentValueExtension
    {
        #region CoercingProp

        public static readonly DependencyProperty CoercingPropsProperty =
            DependencyProperty.RegisterAttached( "CoercingProps", typeof( List<DependencyProperty> ), typeof( SetCurrentValueExtension ) );

        public static List<DependencyProperty> GetCoercingProps( DependencyObject obj )
        {
            return (List<DependencyProperty>)obj.GetValue( CoercingPropsProperty );
        }

        public static void SetCoercingProps( DependencyObject obj, List<DependencyProperty> value )
        {
            obj.SetValue( CoercingPropsProperty, value );
        }

        public static readonly DependencyProperty CoercingValueProperty =
            DependencyProperty.RegisterAttached( "CoercingValue", typeof( object ), typeof( SetCurrentValueExtension ) );

        public static object GetCoercingValue( DependencyObject obj )
        {
            return obj.GetValue( CoercingValueProperty );
        }

        public static void SetCoercingValue( DependencyObject obj, object value )
        {
            obj.SetValue( CoercingValueProperty, value );
        }

        #endregion


        public static void SetCurrentValue( this DependencyObject source, DependencyProperty prop, object value )
        {
            if( BindingOperations.IsDataBound( source, prop ) )
                source.SetValue( prop, value );
            else
            {
                var props = GetCoercingProps( source ) ?? new List<DependencyProperty>();
                props.Add( prop );

                SetCoercingProps( source, props );
                SetCoercingValue( source, value );
                source.CoerceValue( prop );

                props = GetCoercingProps( source ) ?? new List<DependencyProperty>();
                props.Remove( prop );
                SetCoercingProps( source, props );
                SetCoercingValue( source, null );
            }
        }

        public static object CoerceValueCallback( DependencyObject o, object baseValue, DependencyProperty prop )
        {
            var props = GetCoercingProps( o ) ?? new List<DependencyProperty>();
            return props.Contains( prop ) ? o.GetValue( CoercingValueProperty ) : baseValue;
        }
    }
}