﻿using System;

namespace FirstFloor.ModernUI
{
    static class Debug
    {
        public static void WriteLine( string message, params Object[] param )
        {
            System.Diagnostics.Debug.WriteLine( string.Format( message, param ) );
        }
    }
}