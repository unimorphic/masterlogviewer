﻿using System.Windows.Media;
using System.Windows.Forms;


namespace Common.WinFormInterop
{
    public class WinFormInteropLib
    {
        public Color? ShowColorDialog( Color initColor )
        {
            Color? color = null;

            var dlg = new ColorDialog {Color = System.Drawing.Color.FromArgb( initColor.A, initColor.R, initColor.G, initColor.B )};
            if( dlg.ShowDialog() == DialogResult.OK )
                color = Color.FromArgb( dlg.Color.A, dlg.Color.R, dlg.Color.G, dlg.Color.B );

            return color;
        }

        public string ShowFolderDialog( string startPath )
        {
            string path = string.Empty;
            var dlg = new FolderBrowserDialog();
            if( !string.IsNullOrEmpty( startPath ) )
                dlg.SelectedPath = startPath;
            if( dlg.ShowDialog() == DialogResult.OK )
                path = dlg.SelectedPath;
            return path;
        }
    }
}
