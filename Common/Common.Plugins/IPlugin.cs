namespace Common.Plugins
{
	public interface IPlugin
	{
        string FullTypeName { get; set; }
	}
}
