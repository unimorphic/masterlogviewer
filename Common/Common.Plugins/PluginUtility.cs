using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using Common.Utility;

namespace Common.Plugins
{
    public static class PluginUtility
    {
        private const string pluginFilePatternStart = "Plugins.";
        private const string pluginFilePatternEnd = ".dll";

        public static List<IPlugin> Plugins { get; private set; }


        public static void PopulatePlugins( string folder )
        {
            if( Plugins == null )
                Plugins = new List<IPlugin>();

            var files = Directory.GetFiles( folder, pluginFilePatternStart + "*" + pluginFilePatternEnd );
            foreach( var f in files )
            {
                try
                {
                    AddPluginsIfNeeded( GetPluginsFromAssembly( Assembly.LoadFile( f ) ) );
                }
                catch( Exception ex )
                {
                    LogUtility.Append( "Error loading plugin " + f + "\n" + ex, true );
                }
            }
        }

        public static void PopulateEmbbededPlugins()
        {
            if( Plugins == null )
                Plugins = new List<IPlugin>();

            var callingAssembly = Assembly.GetCallingAssembly();
            var resources = callingAssembly.GetManifestResourceNames().Where( s => s.StartsWith( pluginFilePatternStart ) && s.EndsWith( pluginFilePatternEnd ) ).ToList();
            if( resources.Count > 0 )
            {
                foreach( var resourceName in resources )
                {
                    using( var stream = callingAssembly.GetManifestResourceStream( resourceName ) )
                    {
                        if( stream != null )
                        {
                            var block = new byte[stream.Length];
                            try
                            {
                                stream.Read( block, 0, block.Length );
                                AddPluginsIfNeeded( GetPluginsFromAssembly( Assembly.Load( block ) ) );
                            }
                            catch( Exception ex )
                            {
                                LogUtility.Append( "Error loading plugin type " + resourceName + "\n" + ex, true );
                            }
                        }
                    }
                }
            }
        }


        #region Create

        public static T CreatePlugin<T>() where T : class, IPlugin
        {
            return CreatePlugin<T>( typeof( T ) );
        }

        public static T CreatePlugin<T>( Type type ) where T : class, IPlugin
        {
            return CreatePlugin( type ) as T;
        }

        public static IPlugin CreatePlugin( Type type )
        {
            return Activator.CreateInstance( type ) as IPlugin;
        }

        #endregion


        public static List<T> FilterPlugins<T>()
        {
            return FilterPlugins<T>( Plugins );
        }

        public static List<T> FilterPlugins<T>( List<IPlugin> plugins )
        {
            return plugins.OfType<T>().ToList();
        }

        public static T GetPlugin<T>( string fullName ) where T : class
        {
            return Plugins.FirstOrDefault( p => p.FullTypeName == fullName ) as T;
        }


        private static void AddPluginsIfNeeded( IEnumerable<IPlugin> plugins )
        {
            var pluginNames = Plugins.Select( p => p.FullTypeName );

            foreach( var plugin in plugins )
            {
                if( !pluginNames.Contains( plugin.FullTypeName ) )
                    Plugins.Add( plugin );
            }
        }

        static IEnumerable<IPlugin> GetPluginsFromAssembly( Assembly assembly )
        {
            var plugins = new List<IPlugin>();

            var types = assembly.GetTypes();
            foreach( Type t in types )
            {
                try
                {
                    if( !t.IsAbstract && t.IsPublic )
                    {
                        var type = t.GetInterface( typeof( IPlugin ).ToString(), true );

                        if( type != null )
                        {
                            var plugin = CreatePlugin( t );

                            if( plugin is IPluginCreator )
                            {
                                var pluginInstances = ( (IPluginCreator)plugin ).CreatePlugins();
                                foreach( var instance in pluginInstances )
                                    instance.FullTypeName = instance.GetType().FullName;
                                plugins.AddRange( pluginInstances );
                            }
                            else
                            {
                                plugin.FullTypeName = plugin.GetType().FullName;
                                plugins.Add( plugin );
                            }
                        }
                    }
                }
                catch( Exception ex )
                {
                    LogUtility.Append( "Error loading plugin type " + t.FullName + "\n" + ex, true );
                }
            }

            return plugins;
        }
    }
}
