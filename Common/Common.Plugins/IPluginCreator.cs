namespace Common.Plugins
{
	public interface IPluginCreator
	{
		IPlugin[] CreatePlugins();
	}
}
