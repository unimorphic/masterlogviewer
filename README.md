# Overview #
MasterLogViewer is a WPF C# .NET 3.5 application

**Key Features**

* Search, browse & live monitor SharePoint 2007, 2010 & 2013 logs on single & multiserver farms
* Search, browse & live monitor Windows event logs
* Tabbed modern interface
* Customizable including a dark theme

**[Download the Latest Executable](https://bitbucket.org/unimorphic/masterlogviewer/downloads/MasterLogViewer-1.0.zip)**


# Screen Shot #

![MasterLogViewer.png](https://bitbucket.org/repo/k6qqMn/images/279024899-MasterLogViewer.png)


# Source Code #

Currently a plugin architecture is used for the Windows event logs & SharePoint logs. This provides opportunity for other plugins in the future for viewing other types of log files.

Dependencies

* [Reactive Extensions (Rx)](http://www.microsoft.com/en-us/download/details.aspx?id=24940)
* [WPF Toolkit](http://wpf.codeplex.com/releases/view/40535)
* [Modern UI](https://github.com/firstfloorsoftware/mui) (Converted to .NET 3.5)
* WPF Shell Integration Library
* Icons generated using [Metro Studio](http://www.syncfusion.com/downloads/metrostudio)


# FAQ #
**Q. Why does the application require admin privileges when running?**

A. There are two reasons:

1. When running in SP2013, the current method used to determine the farm structure requires admin rights
1. Some Windows event logs require admin right to view

**Q. Why .NET 3.5? Why not upgrade to .NET X.X?**

A. Not all servers have .NET X.X on them, especially older servers like SharePoint 2010 servers


# License #

Note the included dependencies have their own separate, independent licenses.


**The MIT License (MIT)**

Copyright (c) 2013

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.