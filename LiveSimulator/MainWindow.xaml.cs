﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;

namespace LiveSimulator
{
    public partial class MainWindow : Window
    {
        private const string fileExtention = ".log";
        readonly BackgroundWorker worker = new BackgroundWorker();
        private string fileName = string.Empty;
        private int numberOfLines;


        public MainWindow()
        {
            InitializeComponent();

            worker.WorkerSupportsCancellation = true;
            worker.DoWork += WorkerOnDoWork;
            directory.Directory = Properties.Settings.Default.Directory;
        }

        private void Start_OnClick( object sender, RoutedEventArgs e )
        {
            if( !worker.IsBusy )
            {
                start.Content = "Stop";
                worker.RunWorkerAsync( directory.Directory );
            }
            else
            {
                start.Content = "Start";
                worker.CancelAsync();
            }
        }

        private void Directory_OnDirectoryChanged( object sender, EventArgs e )
        {
            Properties.Settings.Default.Directory = directory.Directory;
            Properties.Settings.Default.Save();
        }


        private void WorkerOnDoWork( object sender, DoWorkEventArgs doWorkEventArgs )
        {
            while( !worker.CancellationPending )
            {
                if( string.IsNullOrEmpty( fileName ) || numberOfLines > 5000 )
                {
                    fileName = Guid.NewGuid().ToString() + fileExtention;
                    numberOfLines = 0;
                }

                var date = DateTime.Now.ToString( "MM/dd/yyyy HH:mm:ss.ff" );

                var text = date + @" 	SPUCHostService.exe (0x0BCC)            	0x077" + ( numberOfLines + 1 ) + "	SharePoint Foundation         	Upgrade                       	fbv7	Medium  	[SPUCHostService] [SPDelegateManager] [DEBUG] [04/05/2013 4:00:59 PM]: Waiting for mutex to initialize type dictionary	 " + Environment.NewLine;
                File.AppendAllText( Path.Combine( doWorkEventArgs.Argument.ToString(), fileName ), text );
                numberOfLines += 1;
                System.Threading.Thread.Sleep( 50 );
            }
        }
    }
}
