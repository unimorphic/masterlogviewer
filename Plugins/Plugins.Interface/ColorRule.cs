﻿using System.Collections.Generic;
using System.Windows.Media;

namespace Plugins.Interface
{
    public enum ColorFilterOperations
    {
        Equals,
        Contains
    }

    public enum ColorFilterCommands
    {
        And,
        Or
    }

    public class ColorRuleInfo
    {
        public List<ColorFilterInfo> Filters { get; private set; }

        public Color BackColor { get; set; }

        public Color ForColor { get; set; }

        public ColorRuleInfo( Color backColor, Color forColor )
        {
            Filters = new List<ColorFilterInfo>();
            BackColor = backColor;
            ForColor = forColor;
        }
    }

    public class ColorFilterInfo
    {
        public string Column { get; set; }

        public ColorFilterOperations Operation { get; set; }

        public string Value { get; set; }

        public ColorFilterCommands Command { get; set; }

        public ColorFilterInfo( string column, ColorFilterOperations operation, string value, ColorFilterCommands command )
        {
            Column = column;
            Operation = operation;
            Value = value;
            Command = command;
        }
    }
}
