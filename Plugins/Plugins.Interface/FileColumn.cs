﻿namespace Plugins.Interface
{
    public class FileColumnInfo
    {
        public string Header { get; set; }

        public string BindingPath { get; set; }

        public bool IsVisible { get; set; }

        public bool ShowToolTip { get; set; }

        public FileColumnInfo( string header, string bindingPath, bool isVisible, bool toolTip )
        {
            Header = header;
            BindingPath = bindingPath;
            IsVisible = isVisible;
            ShowToolTip = toolTip;
        }
    }
}
