﻿using System;
using System.Collections.Generic;

namespace Plugins.Interface
{
    public interface ILogLocationMonitor
    {
        event Action<List<Dictionary<string, object>>> FileChanged;
        event Action Loaded;

        bool IsRunning { get; }

        string Location { get; }

        void StartMonitoring();

        void StopMonitoring();
    }
}