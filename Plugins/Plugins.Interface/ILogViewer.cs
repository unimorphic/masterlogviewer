﻿using System;
using System.Collections.Generic;
using Common.Plugins;

namespace Plugins.Interface
{
    public interface ILogViewer : IPlugin
    {
        string DisplayName { get; }

        string IconData { get; }

        string LogFileExtensions { get; }

        string[] FieldNames { get; }

        string DetailsFieldName { get; }

        bool BrowseLogLocations { get; }

        bool FilterBrowseByDate { get; }


        List<string> GetDefaultLogFileLocations( string currentServerName );

        List<string> GetBrowseLocations();

        string GetLocalLogLocation();

        List<string> GetLocationContents( string location );

        List<string> GetLocationContentHierarchy( string content );

        List<ColorRuleInfo> GetDefaultColorRules();

        List<FileColumnInfo> GetDefaultColumns( bool multipleLogLocations );

        string GetSortFieldName( string fieldName );

        void InitReadLogFields();

        bool CancelReadLogFieldsRequested { get; set; }

        List<Dictionary<string, object>> ReadLogFields( string source, string server, string searchTerm, DateTime? searchDate );

        List<Dictionary<string, object>> ReadLogFields( List<string> rows, string server, string searchTerm, DateTime? searchDate );

        void FinishReadLogFields();

        bool IsTextAutoSearchable( string text );

        ILogLocationMonitor CreateLocationMonitor( string location );

        Dictionary<string, string> GetDefaultFilters();
    }
}
