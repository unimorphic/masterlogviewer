﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Media;
using Common.Utility;
using Plugins.Interface;

namespace Plugins.SharePoint
{
    public class SPLogViewer : ILogViewer
    {
        #region TraceSeverity

        private enum TraceSeverity
        {
            Verbose,
            Information,
            Monitorable,
            Warning,
            Medium,
            High,
            CriticalEvent,
            Exception,
            Unexpected
        }

        #endregion

        public string FullTypeName { get; set; }
        public string DisplayName { get { return "SharePoint"; } }
        public string IconData { get { return "M27.861165,54.489481C24.046058,55.955587 20.16175,57.218489 16.163039,58.135195 19.340227,58.848795 22.568118,59.314997 25.86901,59.418997 26.819535,57.942492 27.276448,56.270689 27.861165,54.489481z M18.528905,51.445173C17.504076,53.715979 16.066636,55.588385 14.123882,57.674192 18.652708,56.851488 23.03563,55.765486 27.014941,53.963282 24.155461,53.413877 21.142378,52.973677 18.528905,51.445173z M17.489776,50.981569C14.106881,52.197775 10.09777,53.570177 6.4101672,54.23678 8.4518242,55.762785 10.730587,56.700487 13.139455,57.30969 15.156511,55.546686 17.061464,53.171679 17.489776,50.981569z M11.446708,46.031054C10.037868,48.853964 8.1224148,51.460772 5.8605517,53.916479 9.4518518,53.044077 13.011951,52.007574 16.483348,50.523268 14.147483,48.989462 12.560039,47.59606 11.446708,46.031054z M30.723344,45.846052C27.121644,47.374758 23.014831,49.041464 19.41313,50.570169 21.673492,52.385176 25.714005,52.653475 28.616487,53.182177 29.486211,50.78377 30.310533,48.476362 30.723344,45.846052z M10.300876,45.663855C7.3345929,46.335653 4.3684103,47.010256 1.4022284,47.682158 1.4503288,50.348768 2.4100559,52.351473 4.7147198,53.367079 7.333293,51.002272 9.2721469,48.455561 10.300876,45.663855z M23.566846,41.325239C22.999129,44.153448 20.379256,48.445162 18.588806,50.294069 21.915699,48.994665 27.464053,46.137754 30.790946,44.83835 28.948396,44.564951 26.020114,43.067545 23.566846,41.325239z M22.949727,41.101439C20.121449,42.262743 15.894832,44.596048 12.636741,45.205553 13.786673,47.257559 15.652625,48.656164 17.811485,49.791468 20.038147,47.455557 22.556417,43.682146 22.949727,41.101439z M9.1145232,38.885397L6.1833968,40.714396 5.5867606,41.117769C3.2775917,42.725164 1.6651788,44.410604 1.4646301,47.038858 4.4556128,46.260256 7.446596,45.481554 10.438879,44.700449 9.3561306,43.067038 8.9736259,41.241407 9.0856135,39.279345z M31.684272,36.908625C29.242803,38.176828 26.468026,39.445131 24.026359,40.713236 26.085114,42.343542 28.587785,43.260244 31.205058,43.507547 31.464166,41.184736 31.790975,39.275831 31.684272,36.908625z M16.254042,36.265423C14.360788,37.059624 12.467636,37.856627 10.576883,38.65093 9.8633635,40.320135 10.410279,42.729044 11.675814,43.692446 13.575767,41.270639 15.208612,38.809629 16.254042,36.265423z M17.855786,35.76022C16.392245,38.421629 13.597867,41.697741 12.133027,44.356649 15.043208,44.056948 19.647636,42.054441 22.435313,40.528536 20.970373,39.689932 19.07712,38.314928 17.855786,35.76022z M23.854653,33.286213C22.282908,33.851414 20.207452,34.736717 18.634408,35.30172 20.245152,38.064928 22.30771,39.374731 23.121533,39.749735 23.488744,37.718428 24.174962,35.088318 23.854653,33.286213z M25.5486,32.460711C25.121589,34.859118 24.190562,37.718428 23.76225,40.117033 26.129416,38.90863 28.998096,37.244625 31.363963,36.036221 30.249331,34.432116 28.128173,33.333113 25.5486,32.460711z M31.363963,31.028505C29.976024,31.367006 28.585185,31.708107 27.197146,32.046608 29.1659,32.59351 30.737544,33.489412 31.731174,34.890318 31.814574,33.603914 31.64527,32.314909 31.363963,31.028505z M16.27745,30.377695C15.450545,31.614595 14.520842,33.044298 12.535232,34.963501 13.957139,33.984499 15.381446,33.007799 16.804752,32.028799 16.628951,31.479296 16.454451,30.927095 16.27745,30.377695z M31.682126,29.667198L31.305467,29.784063C29.135789,30.469946,26.896749,31.229227,24.622174,32.064476L23.732312,32.397495 24.688392,32.057751C26.666139,31.363012,28.673377,30.687258,30.706576,29.998503z M21.291873,27.903694C19.048164,30.247395 16.186349,33.281298 13.942739,35.627701 16.881454,34.065199 19.868568,32.822996 22.390779,32.028799 22.222777,30.596296 22.191478,28.906393 21.291873,27.903694z M17.949358,26.158892C17.707158,27.479294 17.161455,28.645994 16.553351,29.781296 17.696658,29.010494 18.842362,28.239693 19.985668,27.466194 19.405065,26.705793 18.713562,26.309891 17.949358,26.158892z M31.319125,22.950689C29.166812,25.656391 26.7853,28.429693 24.634191,31.135395 28.65511,29.835896 32.678529,28.536493 36.69945,27.236994 35.018342,25.325491 33.366033,23.882889 31.319125,22.950689z M30.35822,22.309889C27.609405,24.028791 24.860692,25.749992 22.113379,27.468693 22.713584,28.679693 23.056085,30.114595 23.168084,31.752495 25.428494,28.697994 28.097808,25.364592 30.35822,22.309889z M25.207194,19.671888C24.069089,22.088589 22.932384,24.502491 21.795679,26.916693 24.30609,25.143291 26.816501,23.372389 29.326913,21.598989 28.175807,20.734488 26.8478,20.046888 25.207194,19.671888z M24.26699,19.260488C22.375081,21.093789 20.483171,22.92709 18.589961,24.760492 19.761867,25.031292 20.722773,25.585892 21.406376,26.526091 22.360681,24.104191 23.313985,21.682389 24.26699,19.260488z M45.42069,17.697987C43.496282,20.807189 41.414273,23.86729 38.758061,26.729294 43.576982,25.427092 48.347905,24.450691 53.043225,24.002491 51.03542,21.427089 48.527608,19.296887 45.42069,17.697987z M18.451861,17.455786C18.671963,19.406387 18.725462,21.317788 18.635462,23.020889 20.222771,21.622388 21.718877,20.223888 23.306085,18.825486 21.687577,18.367288 20.069068,17.911486 18.451861,17.455786z M44.849092,17.265686C40.657666,18.471387 36.53925,19.809887 32.714929,21.690188 34.837342,23.088589 36.53135,25.020891 37.614654,27.031294 40.130368,23.747389 42.699479,20.549488 44.849092,17.265686z M34.433738,13.643283C34.36074,16.010486 33.246232,19.234487 32.074428,21.23179 35.730645,19.260488 39.599065,17.687487 43.681283,16.510486 40.748868,15.281284 37.487056,14.260483 34.433738,13.643283z M33.884234,13.437483C31.194123,14.643284 28.673211,16.236985 26.3504,18.296887 28.174609,18.825486 29.777518,19.632887 31.113423,20.773488 32.609429,18.507787 33.364633,16.010486 33.884234,13.437483z M24.54309,11.809882C24.824391,13.812583 25.106795,15.815185 25.389394,17.817785 27.652507,15.885385 30.10822,14.307184 32.738431,13.046883 30.095117,12.309882 27.368606,11.877682 24.54309,11.809882z M23.832189,11.765682C22.00138,12.911482 20.11597,13.992183 18.910264,15.890585 20.720072,16.471385 22.527482,17.051987 24.337291,17.632885 24.168088,15.677085 24.00139,13.721384 23.832189,11.765682z M22.572488,10.33586L23.536202,10.487909C24.04518,10.567893,24.551114,10.64728,25.053931,10.726672L26.535073,10.962618 25.456302,10.779532C24.715873,10.658205,23.960494,10.542247,23.196013,10.428045z M16.313951,10.27878C17.061254,12.257782 17.428456,13.801983 17.789157,15.341084 19.201864,13.778783 21.065073,12.499982 22.906383,11.424382 20.785274,10.700681 18.496161,10.455781 16.313951,10.27878z M11.938828,9.1459503C13.631437,10.419181 15.342345,12.260482 16.425751,13.838583 16.20715,12.570383 15.670547,10.986982 14.901043,9.7787714 13.819037,9.3516397 12.824333,9.1692103 11.938828,9.1459503z M23.604436,7.6848709C22.780217,8.4061737 21.130382,9.145787 19.758051,9.5649729 19.989901,9.7261093 20.806674,9.9284391 21.111638,10.096809L21.124946,10.105015 22.000198,10.245567 22.069009,10.256424 22.239741,10.248596C23.244687,10.216397 24.568032,10.3683 25.527678,10.309707 25.096669,9.7291017 24.095246,8.5701722 23.604436,7.6848709z M8.3589611,7.3595972L8.4464064,7.3989235C8.8042619,7.5569287,9.5010202,7.8313729,10.493999,8.0791435L10.51851,8.0850314 10.494565,8.078306C9.9986961,7.9328972,9.5157247,7.778538,9.0479586,7.6139518z M8.1702409,7.2675235C8.1724734,7.2685775,8.1776989,7.2712393,8.1859493,7.275616L8.2037108,7.285258 8.1925104,7.279626C8.1732852,7.2697355,8.1657767,7.2654145,8.1702409,7.2675235z M12.774697,5.5753895C11.832077,5.9425762 11.108061,6.7523788 10.714753,7.72912 11.940742,7.9431692 12.30125,8.267123 12.724741,8.5174373L12.90107,8.6132947 12.927039,8.6186825 15.498757,8.4400433C14.26183,7.6458007,13.461013,6.4920712,12.774697,5.5753895z M13.828221,5.1404538C14.800842,6.4685714 16.701884,8.3253645 18.590025,9.2889152 20.326963,8.6769212 22.0432,7.830619 23.168225,6.9737564 20.422066,6.8826876 16.575681,5.9660059 13.828221,5.1404538z M20.879076,1.6795549C18.544324,2.4035382 16.407578,3.3280299 14.561236,4.5441494 17.4285,5.2759522 20.134259,5.5624496 22.756717,5.6222589 21.997701,4.6742778 21.394787,3.2967203 20.879076,1.6795549z M21.336185,0C22.428679,3.6157267,23.962918,7.0756772,26.116067,10.320023L26.554849,10.965777 28.032859,11.207899C38.324231,12.926075 47.178628,15.176821 53.940331,23.52879 54.679936,24.442692 54.618733,24.656391 53.902635,24.703192 50.539262,24.912611 42.517163,26.36133 32.919192,29.283375L32.238,29.494728 32.343595,30.464212C33.356549,40.604044 31.397798,49.514994 26.556529,60.241999 12.300931,59.768099 -11.126821,52.294073 6.0429567,40.801737L6.0437209,40.801233 6.8392086,39.956839C7.6387086,39.110083 8.4424121,38.270178 9.1993148,37.5417 20.882974,26.294292 20.582172,16.164084 10.11202,8.5520194 9.0040774,7.7463765 8.3918118,7.3889689 8.2198095,7.2939986L8.2037108,7.285258 8.2301033,7.298531C8.2609806,7.3137769,8.3034463,7.3341616,8.3572438,7.3588252L8.3580606,7.3591923 9.0457323,6.7894977C12.544024,3.9938207,16.918307,1.9774604,21.336185,0z"; } }
        public string LogFileExtensions { get { return "*.log"; } }
        public string[] FieldNames { get { return new[] { "TimeStamp", "Process", "TID", "Area", "Category", "EventID", "Level", "Message", "CorrelationID", "Server" }; } }
        public string DetailsFieldName { get { return "Message"; } }
        public bool BrowseLogLocations { get { return true; } }
        public bool FilterBrowseByDate { get { return false; } }
        public bool CancelReadLogFieldsRequested { get; set; }


        public List<string> GetDefaultLogFileLocations( string currentServerName )
        {
            var logLocations = new List<string>();

            string localLogPath = GetLocalLogLocation();
            logLocations.Add( localLogPath );

            var remoteLogPath = localLogPath.Replace( ':', '$' );
            foreach( var serverName in SPUtility.GetServerNames() )
            {
                if( serverName.ToLower() != currentServerName.ToLower() && serverName.ToLower() != Environment.MachineName.ToLower() )
                {
                    var path = string.Format( "\\\\{0}\\{1}", serverName, remoteLogPath );
                    if( Directory.Exists( path ) )
                        logLocations.Add( path );
                }
            }

            return logLocations.ToList();
        }

        public List<string> GetBrowseLocations()
        {
            return new List<string>();
        }

        public string GetLocalLogLocation()
        {
            string localLogPath = SPUtility.GetLogsLocation();
            if( !Directory.Exists( localLogPath ) )
                localLogPath = Environment.GetFolderPath( Environment.SpecialFolder.CommonProgramFiles );
            return localLogPath;
        }

        public List<string> GetLocationContents( string location )
        {
            return new List<string>();
        }

        public List<string> GetLocationContentHierarchy( string content )
        {
            return new List<string>();
        }

        public List<ColorRuleInfo> GetDefaultColorRules()
        {
            var defaultRule = new ColorRuleInfo( (Color)ColorConverter.ConvertFromString( "#540000" ), Colors.White );
            defaultRule.Filters.Add( new ColorFilterInfo( "Level", ColorFilterOperations.Equals, "Assert", ColorFilterCommands.Or ) );
            defaultRule.Filters.Add( new ColorFilterInfo( "Level", ColorFilterOperations.Equals, "Critical", ColorFilterCommands.Or ) );
            return new List<ColorRuleInfo> {defaultRule};
        }

        public List<FileColumnInfo> GetDefaultColumns( bool multipleLogLocations )
        {
            return new List<FileColumnInfo>
            {
                new FileColumnInfo( "Time Stamp", "TimeStamp", true, false ),
                new FileColumnInfo( "Server", "Server", multipleLogLocations, false ),
                new FileColumnInfo( "Process", "Process", true, false ),
                new FileColumnInfo( "TID", "TID", true, false ),
                new FileColumnInfo( "Area", "Area", true, false ),
                new FileColumnInfo( "Category", "Category", true, false ),
                new FileColumnInfo( "Event ID", "EventID", true, false ),
                new FileColumnInfo( "Level", "Level", true, false ),
                new FileColumnInfo( "Correlation ID", "CorrelationID", true, false ),
                new FileColumnInfo( "Message", "Message", true, true )
            };
        }

        public string GetSortFieldName( string fieldName )
        {
            return fieldName == "Level" ? "LevelInt" : fieldName;
        }

        public void InitReadLogFields() {}

        public void FinishReadLogFields() {}

        public List<Dictionary<string, object>> ReadLogFields( string source, string server, string searchTerm, DateTime? searchDate )
        {
            var rows = FileSystemUtility.ReadFileRows( source );
            if( rows.Count > 0 )
                rows.RemoveAt( 0 );

            return ReadLogFields( rows, server, searchTerm, searchDate );
        }

        public List<Dictionary<string, object>> ReadLogFields( List<string> rows, string server, string searchTerm, DateTime? searchDate )
        {
            var logFields = new List<Dictionary<string, object>>();
            foreach( var r in rows )
            {
                if( CancelReadLogFieldsRequested )
                    break;

                var items = r.Split( new[] {"\t"}, StringSplitOptions.RemoveEmptyEntries );
                if( items.Length >= 8 && ( string.IsNullOrEmpty( searchTerm ) || r.ToLower().Contains( searchTerm ) ) )
                {
                    var fields = ParseFields( items, server );

                    if( fields.ContainsKey( "MessageAppend" ) )
                    {
                        if( logFields.Count > 0 )
                            logFields[logFields.Count - 1]["Message"] += fields["MessageAppend"].ToString();
                    }
                    else
                    {
                        var dateFieldValue = fields["TimeStamp"] as DateTime?;
                        if( !searchDate.HasValue || !dateFieldValue.HasValue || dateFieldValue.Value.Date == searchDate.Value.Date )
                            logFields.Add( fields );
                        else if( dateFieldValue.Value.Date < searchDate.Value.Date )
                            break;
                    }
                }
            }

            return logFields;
        }

        public bool IsTextAutoSearchable( string text )
        {
            var guid = Guid.Empty;
            try
            {
                guid = new Guid( text );
            }
            catch( Exception ) {}

            return guid != Guid.Empty;
        }

        public ILogLocationMonitor CreateLocationMonitor( string location )
        {
            return null;
        }

        public Dictionary<string, string> GetDefaultFilters()
        {
            return new Dictionary<string, string> { { "Errors", "Level:Assert OR Level:Critical" } };
        }


        static Dictionary<string, object> ParseFields( IList<string> fields, string server )
        {
            var result = new Dictionary<string, object>();

            if( fields[0].Trim().EndsWith( "*" ) )
                result.Add( "MessageAppend", fields[7].Trim( ' ', '.' ) );
            else
            {
                DateTime time;
                if( DateTime.TryParseExact( fields[0].Trim().Replace( "/", "-" ), "MM-dd-yyyy HH:mm:ss.ff", null, DateTimeStyles.AssumeLocal, out time ) )
                    result.Add( "TimeStamp", time );
                else
                    result.Add( "TimeStamp", null );

                result.Add( "Process", fields[1].Trim() );
                result.Add( "TID", fields[2].Trim() );
                result.Add( "Area", fields[3].Trim() );
                result.Add( "Category", fields[4].Trim() );
                result.Add( "EventID", fields[5].Trim() );
                var level = fields[6].Trim();
                result.Add( "Level", level );
                result.Add( "Message", fields[7].Trim().TrimEnd( '.' ) );

                var correlationId = string.Empty;
                if( fields.Count >= 9 )
                    correlationId = fields[8].Trim();
                result.Add( "CorrelationID", correlationId );

                result.Add( "Server", server );

                var levelInt = -1;
                if( !string.IsNullOrEmpty( level ) && Enum.IsDefined( typeof( TraceSeverity ), level ) )
                    levelInt = (int)(TraceSeverity)Enum.Parse( typeof( TraceSeverity ), level, true );
                result.Add( "LevelInt", levelInt );
            }

            return result;
        }
    }
}
