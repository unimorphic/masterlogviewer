﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.Win32;
using System.Security;
using Common.Utility;
using System.Diagnostics;
using System.Threading;

/*
http://sharepointlogviewer.codeplex.com/
The MIT License (MIT)
Copyright (c) 2010 Overroot Inc.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Plugins.SharePoint
{
    #region SPVersion

    enum SPVersion
    {
        Unknown,
        SP2007,
        SP2010,
        SP2013
    }

    #endregion

    class SPUtility
    {
        public static SPVersion SPVersion
        {
            get
            {
                try
                {
                    var key = Registry.LocalMachine.OpenSubKey( @"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\12.0" );
                    if( key != null )
                        return SPVersion.SP2007;

                    // Check for SP2010
                    key = Registry.LocalMachine.OpenSubKey( @"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\14.0\WSS" ); // Needed because SP2013 has 14.0 Key too
                    if( key != null )
                        return SPVersion.SP2010;

                    // Check for SP2013
                    key = Registry.LocalMachine.OpenSubKey( @"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\15.0\WSS" );
                    if( key != null )
                        return SPVersion.SP2013;
                }
                catch( SecurityException ) { }
                return SPVersion.Unknown;
            }
        }

        public static bool IsWSSInstalled
        {
            get
            {
                try
                {
                    return GetWSSRegistryKey() != null;
                }
                catch( SecurityException ) { }
                return false;
            }
        }

        public static string WSSInstallPath
        {
            get
            {
                string installPath = String.Empty;
                try
                {
                    using( var key = GetWSSRegistryKey() )
                    {
                        if( key != null )
                            installPath = key.GetValue( "Location" ).ToString();
                    }
                }
                catch( SecurityException ) { }
                return installPath;
            }
        }



        public static string GetLogsLocation()
        {
            string logLocation = String.Empty;
            if( IsWSSInstalled )
            {
                logLocation = GetSPDiagnosticsLogLocation();
                if( logLocation == String.Empty )
                    logLocation = GetStandardLogLocation();
            }

            logLocation = Environment.ExpandEnvironmentVariables( logLocation );

            return logLocation;
        }

        public static IEnumerable<string> GetServerNames()
        {
            var serverNames = new List<string>();

            if( !IsWSSInstalled )
                return serverNames;

            try
            {
                if( SPVersion == SPVersion.SP2007 || SPVersion == SPVersion.SP2010 )
                {
                    var servers = (IEnumerable)GetSharePointProperty( "Microsoft.SharePoint.Administration.SPFarm", "Local", "Servers" );
                    foreach( var server in servers )
                    {
                        var propServerName = server.GetType().GetProperty( "Name", BindingFlags.Public | BindingFlags.Instance );
                        serverNames.Add( (string)propServerName.GetValue( server, null ) );
                    }
                }
                else
                {
                    var results = ExecutePowerShellCommand( "Get-SPServer |? {$_.Role -eq 'Application'} |% {$_.Address}" );

                    if( results != null && results.Length > 0 )
                        serverNames.AddRange( results );
                }
            }
            catch( Exception ex )
            {
                Error.DisplayAndLogError( Error.ErrorType.ShowAndContinue, "Failed to retrieve the SharePoint servers in the farm", ex );
            }

            return serverNames;
        }


        private static RegistryKey GetWSSRegistryKey()
        {
            return GetWSSRegistryKey( "15.0" ) ?? GetWSSRegistryKey( "14.0" ) ?? GetWSSRegistryKey( "12.0" );
        }

        private static RegistryKey GetWSSRegistryKey( string version )
        {
            var key = Registry.LocalMachine.OpenSubKey( @"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\" + version );

            if( key != null )
            {
                var val = key.GetValue( "SharePoint" );
                if( val == null || !val.Equals( "Installed" ) )
                    key = null;
            }
            return key;
        }

        private static string GetStandardLogLocation()
        {
            string logLocation = WSSInstallPath;
            if( logLocation != String.Empty )
                logLocation = Path.Combine( logLocation, "logs" );

            return logLocation;
        }

        private static string GetSPDiagnosticsLogLocation()
        {
            var logLocation = string.Empty;

            try
            {
                if( SPVersion == SPVersion.SP2007 || SPVersion == SPVersion.SP2010 )
                    logLocation = (string)GetSharePointProperty( "Microsoft.SharePoint.Administration.SPDiagnosticsService", "Local", "LogLocation" );
                else
                {
                    var results = ExecutePowerShellCommand( "[Microsoft.SharePoint.Administration.SPDiagnosticsService]::Local.LogLocation" );

                    if( results != null && results.Length > 0 )
                        logLocation = results[0];
                }
            }
            catch( Exception ex )
            {
                Error.DisplayAndLogError( Error.ErrorType.ShowAndContinue, "Failed to retrieve the SharePoint log location", ex );
            }

            return logLocation;
        }


        private static string[] ExecutePowerShellCommand( string command )
        {
            var psi = new ProcessStartInfo
            {
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                FileName = "powershell.exe",
                CreateNoWindow = true
            };
            psi.Arguments = "-Command \"& Add-PSSnapin Microsoft.SharePoint.PowerShell; " + command + "\"";

            var process = new Process { StartInfo = psi };
            process.Start();

            while( !process.HasExited )
            {
                Thread.Sleep( 500 );
            }

            var error = process.StandardError.ReadToEnd();
            if( !string.IsNullOrEmpty( error ) )
                throw ( new Exception( error ) );

            return process.StandardOutput.ReadToEnd().Split( new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries );
        }

        private static object GetSharePointProperty( string className, string classPropName, string childPropName )
        {
            object result = null;
            var assemblyVersion = SPVersion == SPVersion.SP2007 ? "12.0.0.0" : "14.0.0.0";
            var type = Type.GetType( className + ", Microsoft.SharePoint, Version=" + assemblyVersion + ", Culture=neutral, PublicKeyToken=71e9bce111e9429c" );

            if( type != null )
            {
                var classProp = type.GetProperty( classPropName, BindingFlags.Public | BindingFlags.Static ).GetValue( null, null );
                if( classProp != null )
                    result = classProp.GetType().GetProperty( childPropName, BindingFlags.Public | BindingFlags.Instance ).GetValue( classProp, null );
            }
            return result;
        }
    }
}
