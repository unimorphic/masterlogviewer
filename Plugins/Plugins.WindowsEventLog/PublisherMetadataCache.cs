﻿using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.InteropServices;
using Common.Utility;

namespace Plugins.WindowsEventLog
{
    static class PublisherMetadataCache
    {
        private static readonly Dictionary<string, EventLogHandle> cache = new Dictionary<string, EventLogHandle>();

        public static EventLogHandle GetHandle( EventRecord record )
        {
            EventLogHandle handle;

            if( cache.ContainsKey( record.ProviderName ) )
                handle = cache[record.ProviderName];
            else
            {
                var session = ReflectionUtility.GetMemeberValue( record, "session" );
                var sessionHandle = new EventLogHandle( ( (SafeHandle)ReflectionUtility.GetPropertyValue( session, "Handle" ) ).DangerousGetHandle(), false );
                handle = Wevtapi.EvtOpenPublisherMetadata( sessionHandle, record.ProviderName, null, 0, 0 );
                cache.Add( record.ProviderName, handle );
            }

            return handle;
        }

        public static void ClearCache()
        {
            cache.Clear();
        }
    }
}
