﻿using System.Diagnostics.Eventing.Reader;
using Common.Utility;

namespace Plugins.WindowsEventLog
{
    class LevelFieldCache : FieldCache
    {
        protected override string GetValue( EventRecord record )
        {
            return EvtFormatMessageRenderName( record, EvtFormatMessageFlags.EvtFormatMessageLevel );
        }

        protected override bool IsMatch( EventRecord a, EventRecord b )
        {
            return a.ProviderName == b.ProviderName && a.Level.Value == b.Level.Value;
        }
    }
}
