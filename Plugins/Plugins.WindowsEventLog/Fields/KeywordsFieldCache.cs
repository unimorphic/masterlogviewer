﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.InteropServices;
using Common.Utility;
using System.Linq;

namespace Plugins.WindowsEventLog
{
    class KeywordsFieldCache : FieldCache
    {
        protected override string GetValue( EventRecord record )
        {
            var keywords = EvtFormatMessageRenderKeywords( PublisherMetadataCache.GetHandle( record ), GetEventHandle( record ), EvtFormatMessageFlags.EvtFormatMessageKeyword );
            return keywords.Any() ? keywords.Aggregate( ( a, b ) => a + "," + b ) : string.Empty;
        }

        protected override bool IsMatch( EventRecord a, EventRecord b )
        {
            return a.ProviderName == b.ProviderName && a.Keywords.Value == b.Keywords.Value;
        }


        private static IEnumerable<string> EvtFormatMessageRenderKeywords( EventLogHandle pmHandle, EventLogHandle eventHandle, EvtFormatMessageFlags flag )
        {
            int num;
            int lastWin32Error;
            IEnumerable<string> strs;
            IntPtr zero = IntPtr.Zero;
            try
            {
                var strs1 = new List<string>();
                bool flag1 = Wevtapi.EvtFormatMessageBuffer( pmHandle, eventHandle, 0, 0, IntPtr.Zero, flag, 0, IntPtr.Zero, out num );
                lastWin32Error = Marshal.GetLastWin32Error();
                if( !flag1 )
                {
                    int num1 = lastWin32Error;
                    if( num1 <= 15028 )
                    {
                        if( num1 != 1815 )
                        {
                            switch( num1 )
                            {
                                case 15027:
                                case 15028:
                                    {
                                        break;
                                    }
                                default:
                                    {
                                        goto Label1;
                                    }
                            }
                        }
                    }
                    else if( num1 != 15033 && num1 != 15100 )
                    {
                        goto Label1;
                    }
                    strs = strs1.AsReadOnly();
                    return strs;
                }
                Label3:
                zero = Marshal.AllocHGlobal( num * 2 );
                flag1 = Wevtapi.EvtFormatMessageBuffer( pmHandle, eventHandle, 0, 0, IntPtr.Zero, flag, num, zero, out num );
                lastWin32Error = Marshal.GetLastWin32Error();
                if( !flag1 )
                {
                    int num2 = lastWin32Error;
                    if( num2 <= 15028 )
                    {
                        if( num2 != 1815 )
                        {
                            switch( num2 )
                            {
                                case 15027:
                                case 15028:
                                    {
                                        break;
                                    }
                                default:
                                    {
                                        goto Label2;
                                    }
                            }
                        }
                    }
                    else if( num2 != 15033 && num2 != 15100 )
                    {
                        goto Label2;
                    }
                    strs = strs1;
                    return strs;
                }
                Label4:
                IntPtr intPtr = zero;
                while( true )
                {
                    string stringAuto = Marshal.PtrToStringAuto( intPtr );
                    if( string.IsNullOrEmpty( stringAuto ) )
                    {
                        break;
                    }
                    strs1.Add( stringAuto );
                    intPtr = new IntPtr( (long)intPtr + (long)( stringAuto.Length * 2 ) + (long)2 );
                }
                strs = strs1.AsReadOnly();
                return strs;
                Label1:
                if( lastWin32Error != 122 )
                {
                    //EventLogException.Throw( lastWin32Error );
                    goto Label3;
                }
                else
                {
                    goto Label3;
                }
                Label2:
                //EventLogException.Throw( lastWin32Error );
                goto Label4;
            }
            finally
            {
                if( zero != IntPtr.Zero )
                {
                    Marshal.FreeHGlobal( zero );
                }
            }
        }
    }
}
