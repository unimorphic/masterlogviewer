﻿using System.Diagnostics.Eventing.Reader;
using Common.Utility;

namespace Plugins.WindowsEventLog
{
    static class MessageFieldUtility
    {
        public static string GetValue( EventRecord record )
        {
            var value = FieldCache.EvtFormatMessageRenderName( record, EvtFormatMessageFlags.EvtFormatMessageEvent );
            return !string.IsNullOrEmpty( value ) ? value : ( record.Properties.Count == 1 ? record.Properties[0].Value.ToString() : string.Empty );
        }
    }
}
