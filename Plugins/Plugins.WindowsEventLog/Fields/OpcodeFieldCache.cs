﻿using System.Diagnostics.Eventing.Reader;
using Common.Utility;

namespace Plugins.WindowsEventLog
{
    class OpcodeFieldCache : FieldCache
    {
        protected override string GetValue( EventRecord record )
        {
            return EvtFormatMessageRenderName( record, EvtFormatMessageFlags.EvtFormatMessageOpcode );
        }

        protected override bool IsMatch( EventRecord a, EventRecord b )
        {
            return a.ProviderName == b.ProviderName && a.Opcode.Value == b.Opcode.Value;
        }
    }
}
