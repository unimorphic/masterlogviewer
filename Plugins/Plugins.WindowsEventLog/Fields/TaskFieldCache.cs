﻿using System.Diagnostics.Eventing.Reader;
using Common.Utility;

namespace Plugins.WindowsEventLog
{
    class TaskFieldCache : FieldCache
    {
        protected override string GetValue( EventRecord record )
        {
            var value = EvtFormatMessageRenderName( record, EvtFormatMessageFlags.EvtFormatMessageTask );
            return !string.IsNullOrEmpty( value ) ? value : "None";
        }

        protected override bool IsMatch( EventRecord a, EventRecord b )
        {
            return a.ProviderName == b.ProviderName && a.Task.Value == b.Task.Value;
        }
    }
}
