﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using Common.Utility;
using Plugins.Interface;

namespace Plugins.WindowsEventLog
{
    public class WindowsEventLogViewer : ILogViewer
    {
        #region FieldCaches

        enum FieldCaches
        {
            Task,
            Keywords,
            Level,
            Opcode
        }

        #endregion

        #region LocationMonitor

        class LocationMonitor : ILogLocationMonitor
        {
            private readonly EventLogWatcher watcher;
            private readonly string path;
            private bool isLoaded;


            public bool IsRunning { get { return watcher.Enabled; } }
            public string Location { get { return path; } }

            public event Action<List<Dictionary<string, object>>> FileChanged = delegate { };
            public event Action Loaded = delegate { };

            internal event Func<EventRecord, Dictionary<string, object>> ParseRecord = delegate { return new Dictionary<string, object>(); };
            internal event Action ClearAllCaches = delegate {};


            public LocationMonitor( string path )
            {
                this.path = path;

                watcher = new EventLogWatcher( path );
                watcher.EventRecordWritten += WatcherOnEventRecordWritten;
            }


            public void StartMonitoring()
            {
                watcher.Enabled = true;

                if( !isLoaded )
                {
                    isLoaded = true;
                    Loaded();
                }
            }

            public void StopMonitoring()
            {
                watcher.Enabled = false;
                ClearAllCaches();
            }


            private void WatcherOnEventRecordWritten( object sender, EventRecordWrittenEventArgs e )
            {
                if( e.EventRecord != null )
                    FileChanged( new List<Dictionary<string, object>> { ParseRecord( e.EventRecord ) } );
            }
        }

        #endregion

        private readonly Dictionary<FieldCaches, FieldCache> fieldCaches = new Dictionary<FieldCaches, FieldCache>();

        public string FullTypeName { get; set; }
        public string DisplayName { get { return "Windows Events"; } }
        public string IconData { get { return "F1M181.003,-1898.78L207.077,-1902.33 207.089,-1877.18 181.027,-1877.03 181.003,-1898.78z M207.065,-1874.28L207.085,-1849.1 181.023,-1852.69 181.022,-1874.45 207.065,-1874.28z M210.226,-1902.79L244.798,-1907.84 244.798,-1877.5 210.226,-1877.22 210.226,-1902.79z M244.807,-1874.04L244.798,-1843.84 210.226,-1848.72 210.177,-1874.1 244.807,-1874.04z"; } }
        public string LogFileExtensions { get { return "*.evtx"; } }
        public string[] FieldNames { get { return new[] { "Keywords", "User", "OperationalCode", "Log", "Computer", "ProcessID", "ThreadID", "DateAndTime",
                                                           "Source", "EventID", "TaskCategory", "Level", "LevelByte", "Message", "Xml" }; } }
        public string DetailsFieldName { get { return "Message"; } }
        public string DateFieldName { get { return "DateAndTime"; } }
        public bool BrowseLogLocations { get { return false; } }
        public bool FilterBrowseByDate { get { return true; } }
        public bool CancelReadLogFieldsRequested { get; set; }


        public List<string> GetDefaultLogFileLocations( string currentServerName )
        {
            return new List<string> { "Application", "Security", "Setup", "System" };
        } 

        public string GetLocalLogLocation()
        {
            return Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments );
        }

        public List<string> GetBrowseLocations()
        {
            return new List<string> {"Standard Logs"};
        }

        public List<string> GetLocationContents( string location )
        {
            var contents = EventLogSession.GlobalSession.GetLogNames().ToList();
            contents.Sort();
            return contents;
        }

        public List<string> GetLocationContentHierarchy( string content )
        {
            var hierarchy = content.Split( new[] { '-' }, 3 ).ToList();
            var split = hierarchy[hierarchy.Count - 1].Split( '/' );
            if( split.Length > 1 )
            {
                hierarchy.RemoveAt( hierarchy.Count - 1 );
                hierarchy.AddRange( split );
            }
            return hierarchy;
        }

        public List<ColorRuleInfo> GetDefaultColorRules()
        {
            return new List<ColorRuleInfo>();
        }

        public List<FileColumnInfo> GetDefaultColumns( bool multipleLogLocations )
        {
            return new List<FileColumnInfo>
            {
                new FileColumnInfo( "Level", "Level", true, false ),
                new FileColumnInfo( "Date And Time", "DateAndTime", true, false ),
                new FileColumnInfo( "Source", "Source", true, false ),
                new FileColumnInfo( "Event ID", "EventID", true, false ),
                new FileColumnInfo( "Task Category", "TaskCategory", true, false ),
                new FileColumnInfo( "Message", "Message", false, false ),
                new FileColumnInfo( "Keywords", "Keywords", false, false ),
                new FileColumnInfo( "User", "User", false, false ),
                new FileColumnInfo( "Operational Code", "OperationalCode", false, false ),
                new FileColumnInfo( "Log", "Log", false, false ),
                new FileColumnInfo( "Computer", "Computer", false, false ),
                new FileColumnInfo( "Process ID", "ProcessID", false, false ),
                new FileColumnInfo( "Thread ID", "ThreadID", false, false ),
                new FileColumnInfo( "Xml", "Xml", false, false )
            };
        }

        public string GetSortFieldName( string fieldName )
        {
            return fieldName == "Level" ? "LevelByte" : fieldName;
        }

        public void InitReadLogFields()
        {
            if( fieldCaches.Count <= 0 )
            {
                fieldCaches.Add( FieldCaches.Level, new LevelFieldCache() );
                fieldCaches.Add( FieldCaches.Keywords, new KeywordsFieldCache() );
                fieldCaches.Add( FieldCaches.Opcode, new OpcodeFieldCache() );
                fieldCaches.Add( FieldCaches.Task, new TaskFieldCache() );
            }
        }

        public void FinishReadLogFields()
        {
            ClearAllCaches();
        }

        public List<Dictionary<string, object>> ReadLogFields( string source, string server, string searchTerm, DateTime? searchDate )
        {
            var rows = new List<Dictionary<string, object>>();
            var query = new EventLogQuery( source, File.Exists( source ) ? PathType.FilePath : PathType.LogName ) {ReverseDirection = true};
            var reader = new EventLogReader( query );
            var eventInstance = reader.ReadEvent();
            while( eventInstance != null )
            {
                try
                {
                    if( CancelReadLogFieldsRequested )
                        break;

                    if( !searchDate.HasValue || !eventInstance.TimeCreated.HasValue || eventInstance.TimeCreated.Value.Date == searchDate.Value.Date )
                        rows.Add( ParseRecord( eventInstance ) );
                    else if( eventInstance.TimeCreated.Value.Date < searchDate.Value.Date )
                        break;

                    eventInstance = reader.ReadEvent();
                }
                catch( Exception ex )
                {
                    LogUtility.Append( "Error loading windows event log \n" + ex, true );
                }
            }
            
            return rows;
        }

        public List<Dictionary<string, object>> ReadLogFields( List<string> rows, string server, string searchTerm, DateTime? searchDate )
        {
            return new List<Dictionary<string, object>>();
        }

        public bool IsTextAutoSearchable( string text )
        {
            return false;
        }

        public ILogLocationMonitor CreateLocationMonitor( string location )
        {
            var monitor = new LocationMonitor( location );
            monitor.ClearAllCaches += ClearAllCaches;
            monitor.ParseRecord += ParseRecord;
            return monitor;
        }

        public Dictionary<string, string> GetDefaultFilters()
        {
            return new Dictionary<string, string> { { "Errors", "Level:Critical OR Level:Error" } };
        }


        private Dictionary<string, object> ParseRecord( EventRecord record )
        {
            var fields = new Dictionary<string, object>();
            fields.Add( "Keywords", record.Keywords.HasValue ? fieldCaches[FieldCaches.Keywords].GetCachedValue( record ) : string.Empty );
            fields.Add( "User", record.UserId != null ? record.UserId.Value : string.Empty );
            fields.Add( "OperationalCode", record.Opcode.HasValue ? fieldCaches[FieldCaches.Opcode].GetCachedValue( record ) : string.Empty );
            fields.Add( "Log", record.LogName );
            fields.Add( "Computer", record.MachineName );
            fields.Add( "ProcessID", record.ProcessId );
            fields.Add( "ThreadID", record.ThreadId );
            fields.Add( "DateAndTime", record.TimeCreated );
            fields.Add( "Source", record.ProviderName );
            fields.Add( "EventID", record.Id );
            fields.Add( "TaskCategory", record.Task.HasValue ? fieldCaches[FieldCaches.Task].GetCachedValue( record ) : string.Empty );
            fields.Add( "Level", record.Level.HasValue ? fieldCaches[FieldCaches.Level].GetCachedValue( record ) : string.Empty );
            fields.Add( "LevelByte", record.Level.HasValue ? record.Level.Value : -1 );
            fields.Add( "Message", MessageFieldUtility.GetValue( record ) );
            fields.Add( "Xml", record.ToXml() );
            return fields;
        }

        private void ClearAllCaches()
        {
            PublisherMetadataCache.ClearCache();
            foreach( var cache in fieldCaches )
                cache.Value.ClearCache();
        }
    }
}
