﻿using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Common.Utility;

namespace Plugins.WindowsEventLog
{
    abstract class FieldCache
    {
        readonly Dictionary<EventRecord, string> cache = new Dictionary<EventRecord, string>();

        public string GetCachedValue( EventRecord record )
        {
            string value;
            var key = cache.Keys.FirstOrDefault( k => IsMatch( k, record ) );
            if( key != null )
                value = cache[key];
            else
            {
                value = GetValue( record );
                cache.Add( record, value );
            }

            return value;
        }

        public void ClearCache()
        {
            cache.Clear();
        }

        protected abstract string GetValue( EventRecord record );

        protected abstract bool IsMatch( EventRecord a, EventRecord b );


        protected static EventLogHandle GetEventHandle( EventRecord record )
        {
            return new EventLogHandle( ( (SafeHandle)ReflectionUtility.GetPropertyValue( record, "Handle" ) ).DangerousGetHandle(), false );
        }

        public static string EvtFormatMessageRenderName( EventRecord record, EvtFormatMessageFlags flag )
        {
            return EvtFormatMessageRenderName( PublisherMetadataCache.GetHandle( record ), GetEventHandle( record ), flag );
        }

        static string EvtFormatMessageRenderName( EventLogHandle pmHandle, EventLogHandle eventHandle, EvtFormatMessageFlags flag )
        {
            int num;
            var stringBuilder = new StringBuilder( null );
            bool flag1 = Wevtapi.EvtFormatMessage( pmHandle, eventHandle, 0, 0, null, flag, 0, stringBuilder, out num );
            int lastWin32Error = Marshal.GetLastWin32Error();
            if( !flag1 && lastWin32Error != 15029 )
            {
                int num1 = lastWin32Error;
                if( num1 <= 15028 )
                {
                    if( num1 != 1815 )
                    {
                        switch( num1 )
                        {
                            case 15027:
                            case 15028:
                                {
                                    break;
                                }
                            default:
                                {
                                    goto Label0;
                                }
                        }
                    }
                }
                else if( num1 != 15033 && num1 != 15100 )
                {
                    goto Label0;
                }
                return string.Empty;
            }
        Label2:
            stringBuilder.EnsureCapacity( num );
            flag1 = Wevtapi.EvtFormatMessage( pmHandle, eventHandle, 0, 0, null, flag, num, stringBuilder, out num );
            lastWin32Error = Marshal.GetLastWin32Error();
            if( !flag1 && lastWin32Error != 15029 )
            {
                int num2 = lastWin32Error;
                if( num2 <= 15028 )
                {
                    if( num2 != 1815 )
                    {
                        switch( num2 )
                        {
                            case 15027:
                            case 15028:
                                {
                                    break;
                                }
                            default:
                                {
                                    //EventLogException.Throw( lastWin32Error );
                                    return stringBuilder.ToString();
                                }
                        }
                    }
                }
                else if( num2 != 15033 && num2 != 15100 )
                {
                    //EventLogException.Throw( lastWin32Error );
                    return stringBuilder.ToString();
                }
                return string.Empty;
            }
            return stringBuilder.ToString();
        Label0:
            if( lastWin32Error != 122 )
            {
                //EventLogException.Throw( lastWin32Error );
                goto Label2;
            }
            else
            {
                goto Label2;
            }
        }
    }
}
